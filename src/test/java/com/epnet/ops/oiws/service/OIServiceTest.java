package com.epnet.ops.oiws.service;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epnet.ops.oiws.beans.BaseOrder;
import com.epnet.ops.oiws.beans.BaseOrderItem;
import com.epnet.ops.oiws.beans.GenerateOrderDocumentRequest;
import com.epnet.ops.oiws.beans.GetNextOrderDocumentToGenerateResponse;
import com.epnet.ops.oiws.beans.ObjectFactory;
import com.epnet.ops.oiws.beans.OrderInstallments;
import com.epnet.ops.oiws.beans.OrderOptions;
import com.epnet.ops.oiws.beans.SetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.SetOrderStateRequest;
import com.epnet.ops.oiws.dao.opx.type.ContainerSource;
import com.epnet.ops.oiws.dao.opx.type.ContainerStatus;
import com.epnet.ops.oiws.dao.opx.type.ContainerType;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestStatus;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestType;
import com.epnet.ops.oiws.dao.opx.type.ItemState;
import com.epnet.ops.oiws.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:oiws-test-ha-context.xml" })
public class OIServiceTest {
	private static final String TEST_CUSTID = "abcdef";
	private static final String TEST_PO_NUMBER = "xxyy12";
	
	private ObjectFactory factory = new ObjectFactory();
	
	@Resource
	private IOIService oiService;
	
	@Test
	public void runOrchestratedTest() {
		// Save a new order
		int id = oiService.saveOrder(createSampleOrder());
		Assert.assertTrue(id > 0);

		// Retrieve the newly created order
		BaseOrder order = oiService.getOrder(id);
		Assert.assertTrue(order != null);
		
		oiService.saveOrder(order);
		
		// Submit the newly created order
		oiService.submitOrder(id);
		
		// Fetch the newly submitted order id
		Integer submittedId = oiService.getNextSubmittedOrderToProcess();

		// Reset the status of the submitted order to WIP
		SetOrderStateRequest req = factory.createSetOrderStateRequest();
		req.setOrderIdentifier(id);
		req.setOrderStateCode(ContainerStatus.WIP.name());
		req.setItemStateCode(ItemState.WIP.name());
		oiService.setOrderState(req);
		
		// Fetch the updated order and check modified status flag 
		order = oiService.getOrder(id);
		Assert.assertTrue(order.getStateCode().equals(ContainerStatus.WIP.name()));
		
		// Re-fetch the first single order item
		BaseOrderItem orderItem = order.getOrderItem().get(0);
		Integer orderItemId = orderItem.getOrderItemIdentifier();
		BaseOrderItem fetchedItem = oiService.getOrderItem(orderItemId, null);
		Assert.assertTrue(fetchedItem != null);

		// Update the single order item
		fetchedItem.setComplimentary(Boolean.TRUE);
		oiService.saveOrderItem(fetchedItem);

		oiService.removeOrderItem(orderItem.getOrderIdentifier(), orderItem.getItemIdentifier());
		
		// Fetch a list of orders for the test customer
		List<BaseOrder> orders = oiService.listOrders(TEST_CUSTID, ContainerType.ORDER.toString(), null);
		Assert.assertTrue(orders.size() > 0);
		
		oiService.deleteOrder(order.getOrderIdentifier());
	}
	
	@Test
	@Ignore
	public void runDocRequestTest() {

		GenerateOrderDocumentRequest request = factory.createGenerateOrderDocumentRequest();
		request.setOrderIdentifier(10);
		request.setDocTypeCode(DocumentRequestType.WNSR_POF.name());
		request.setBaseFileName("MyFileName");
		request.setLanguageCode("ENG");
		
		oiService.generateOrderDocument(request);

		GetNextOrderDocumentToGenerateResponse response = oiService.getNextOrderDocumentToGenerate();
		
		if (response == null) {
			System.out.println("Null response");
		}
		
		SetOrderDocumentStatusRequest statusRequest = factory.createSetOrderDocumentStatusRequest();
		statusRequest.setRequestId(1);
		statusRequest.setFileLocation("/this/is/a/test/file.pdf");
		statusRequest.setStatusCode(DocumentRequestStatus.COMPLETED.name());
		
		oiService.setOrderDocumentStatus(statusRequest);
	}
	
	private BaseOrder createSampleOrder() {
		BaseOrder order = factory.createBaseOrder();
		
		order.setCustomerIdentifier(TEST_CUSTID);
		order.setCategoryCode(ContainerType.ORDER.toString());
		order.setStateCode(ContainerStatus.WIP.toString());
		order.setSourceCode(ContainerSource.ECM.toString());
		order.setCategoryCode(ContainerType.ORDER.toString());
		order.setOpportunityIdentifier(12345678);
		order.setSalesRepIdentifier(12345);
		order.setCustomerContactIdentifier(1000);
		order.setCurrencyCode("USD");
		// order.setLanguageCode("ENG");
		order.setPurchaseOrderNumber(TEST_PO_NUMBER);
		order.setBillingAddressIdentifier(2000);
		order.setBillingContactIdentifier(3000);
		
		order.getOrderItem().add(createSampleItem());
		
		OrderOptions options = factory.createOrderOptions();
		options.setOrderDescription("This is a test order");
		order.setOptions(options);

		OrderInstallments installments = factory.createOrderInstallments();
		installments.setInstallmentCount(1);
		installments.setAmount1(100.00);
		installments.setInvoiceDate1(DateUtil.createXMLGregorianCalendar());
		order.setInstallments(installments);
		
		return order;
	}
	
	private BaseOrderItem createSampleItem() {
		BaseOrderItem item = factory.createBaseOrderItem();
		
		item.setProductIdentifier(100);
		item.setGrossPrice(100.0);
		item.setPremiumPricingDiscount(5.0);
		item.setPrimaryAgencyDiscount(10.0);
		item.setStartDate(DateUtil.createXMLGregorianCalendar());
		item.setSequence(1);
		item.setTermInMonths(12);
		item.setProbability(.9);	
		item.setRenewal(true);
		item.setRenewalSubscriptionIdentifier(111);
		item.setPurchaseOrderNumber("zzz123");
		item.setExpectedCloseDate(DateUtil.createXMLGregorianCalendar());
		item.setTidIdentifier(2222);
		item.setMultiYearPayments(1);
		item.setAmountYear1(100.0);
		
		return item;
	}
}