package com.epnet.ops.oiws;

/**
 * General application constants.
 * 
 * @author jsaparo
 *
 */
public class AppConstants {
	public static final String STATUS_OK = "OK";
	public static final String STATUS_FAIL = "FAIL";
}
