package com.epnet.ops.oiws.util;

import java.util.HashMap;
import java.util.Map;

public class CurrencyUtil {

	/*
	 * This is a bit of a hack. Because we don't have a reliable way of forming the necessary
	 * Java locale based on language/country lookups from OPs, this map is used to derive currency symbols
	 * for those ISO currency codes currently supported in OPs.
	 */
	private static final Map<String, String> CURRENCY_SYMBOLS = new HashMap<String, String>();
	static {
		CURRENCY_SYMBOLS.put("USD", "$");
		CURRENCY_SYMBOLS.put("INR", "Rs");
		CURRENCY_SYMBOLS.put("AUD", "$");
		CURRENCY_SYMBOLS.put("CAD", "$");
		CURRENCY_SYMBOLS.put("NZD", "$");
		CURRENCY_SYMBOLS.put("GBP", "£");
		CURRENCY_SYMBOLS.put("EUR", "€");
		CURRENCY_SYMBOLS.put("JPY", "¥");
		CURRENCY_SYMBOLS.put("ZAR", "R");
	}
	
	/**
	 * Return the symbol for a given iso code.
	 * 
	 * @param isoCurrencyCode The iso code
	 * @return The symbol, or null if not found
	 */
	public static String getSymbolForCode(String isoCurrencyCode) {
		return CURRENCY_SYMBOLS.get(isoCurrencyCode);
	}

}
