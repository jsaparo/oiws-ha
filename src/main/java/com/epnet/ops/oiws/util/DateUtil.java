package com.epnet.ops.oiws.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

/**
 * Miscellaneous date utilities.
 *  
 * @author jsaparo
 *
 */
public class DateUtil {
	
	public static Logger log = Logger.getLogger(DateUtil.class);
	
	// Stock set of formats
	public static final String SHORT_US_DATE = "MM/dd/yyyy";
	public static final String SHORT_US_DATETIME = "MM/dd/yyyy HH:mm:ss";
	public static final String UNIVERSAL_DATE = "yyyy-MM-dd";
	public static final String UNIVERSAL_DATETIME = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	public static final String MEDIUM_US_DATE = "MMMMM d, yyyy";
	public static final String COMPRESSED_DATE = "yyyyMMdd";
	public static final String COMPRESSED_DATETIME = "yyyyMMddHH:mm:ss";
	public static final String XML_DATE = "yyyy-MM-dd HH:mm:ss";

	/**
	 * Format a stock short us date from today's date.
	 * @return A formatted date string
	 */
	public static String formatShortUsDate() {
		return format(SHORT_US_DATE);
	}
	
	/**
	 * Format a stock short us date from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatShortUsDate(Date dt) {
		return format(dt, SHORT_US_DATE);
	}
	
	/**
	 * Format a stock short us date/time from today's date.
	 * @return A formatted date string
	 */
	public static String formatShortUsDateTime() {
		return format(SHORT_US_DATETIME);
	}
	
	/**
	 * Format a stock short us date/time from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatShortUsDateTime(Date dt) {
		return format(dt, SHORT_US_DATETIME);
	}
	
	/**
	 * Format a stock universal date from today's date.
	 * @return A formatted date string
	 */
	public static String formatUniversalDateTime() {
		return format(UNIVERSAL_DATETIME);
	}
	
	/**
	 * Format a stock universal date from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatUniversalDateTime(Date dt) {
		return format(dt, UNIVERSAL_DATETIME);
	}
	
	/**
	 * Format a stock universal date from today's date.
	 * @return A formatted date string
	 */
	public static String formatUniversalDate() {
		return format(UNIVERSAL_DATE);
	}
	
	/**
	 * Format a stock universal date from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatUniversalDate(Date dt) {
		return format(dt, UNIVERSAL_DATE);
	}
	
	/**
	 * Format a stock medium us date from today's date.
	 * @return A formatted date string
	 */
	public static String formatMediumUsDate() {
		return format(MEDIUM_US_DATE);
	}
	
	/**
	 * Format a stock medium us date from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatMediumUsDate(Date dt) {
		return format(dt, MEDIUM_US_DATE);
	}
	
	/**
	 * Format a stock compressed us date from today's date.
	 * @return A formatted date string
	 */
	public static String formatCompressedDate() {
		return format(COMPRESSED_DATE);
	}
	
	/**
	 * Format a stock compressed date from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatCompressedDate(Date dt) {
		return format(dt, COMPRESSED_DATE);
	}
	
	/**
	 * Format a stock compressed date/time from today's date.
	 * @return A date string
	 */
	public static String formatCompressedDateTime() {
		return format(COMPRESSED_DATETIME);
	}
	
	/**
	 * Format a stock compressed date/time from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatCompressedDateTime(Date dt) {
		return format(dt, COMPRESSED_DATETIME);
	}
	
	/**
	 * Format a stock XML/Gregorian date from today's date.
	 * @return A date string
	 */
	public static String formatXmlDateTime() {
		return format(XML_DATE);
	}
	
	/**
	 * Format a stock XML/Gregorian date from a given date.
	 * @param dt A date to format
	 * @return A formatted date string
	 */
	public static String formatXmlDateTime(Date dt) {
		return format(dt, XML_DATE);
	}

	/**
	 * Format today's date with a given date format.
	 * 
	 * @param format A date format
	 * @return A formatted date string
	 */
	public static String format(String format) {
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		
		return fmt.format(new Date());
	}
	
	/**
	 * Format a date as a string given a format.
	 * 
	 * @param dt The date to format.
	 * @param format A data format string
	 * @return A formatted date string
	 */
	public static String format(Date dt, String format) {
		if (dt == null) {
			return null;
		}
		
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		
		return fmt.format(dt);
	}
	
	/**
	 * Parse a given date string into a date object.
	 * 
	 * @param dtStr A string representation of a date
	 * @param format The format to use for interpreting the string
	 * @return A date object, or null if the date could not be parsed
	 */
	public static Date toDate(String dtStr, String format) {
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		
		Date dt = null;
		
		try {
			dt = fmt.parse(dtStr);
		} catch (ParseException e) {
			log.warn("Unable to parse date string: " + dtStr + " using format: " + format);
		}

		return dt; 
	}
	
	/**
	 * Utility method for instantiating an XMLGregorianCalendar object
	 * initialized to a specified date.
	 * 
	 * @param date The date to set the calendar to
	 * @return An XMLGregorianCalendar object
	 */
	public static XMLGregorianCalendar createXMLGregorianCalendar(Date date) 
    {
		if (date == null) {
			return null;
		}
		
        DatatypeFactory datatypeFactory = null;

		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException ex) {
			log.warn("Error creating xml calendar object", ex);
			return null;
		}
        
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        
        return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
    }
	
	/**
	 * Utility method for instantiating an XMLGregorianCalendar object.
	 * 
	 * @return An XMLGregorianCalendar object
	 */
	public static XMLGregorianCalendar createXMLGregorianCalendar() 
    {
		return createXMLGregorianCalendar(new Date());
    }
	
	/**
	 * Calculate the number of days between two dates.
	 * 
	 * @param d1 The first date
	 * @param d2 The second date
	 * 
	 * @return Positive number of days between the two given dates.
	 */
	public static int getDaysBetween(Date d1, Date d2) {
		if (d1 == null || d2 == null) {
			return 0;
		}
		
		return Math.abs((int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24)));
	}
	
	/**
	 * Convert an XMLGregorianCalendar value to a java Date object
	 * 
	 * @param xmlCal The XML calendar value to convert
	 * 
	 * @return A converted Date object
	 */
	public static Date createDate(XMLGregorianCalendar xmlCal) {
		if (xmlCal == null) {
			return null;
		}
		
		return xmlCal.toGregorianCalendar().getTime();
	}
}
