package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class GetContainerByStatusAndTypeProc extends StoredProcedure {
	public static final String GET_CONTAINER_BY_STATUS_TYPE_PROC_NAME = "prx_get_Container_by_Status_Type";
	
	@Autowired(required=false)
	public GetContainerByStatusAndTypeProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, GET_CONTAINER_BY_STATUS_TYPE_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_STATUS_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_TYPE_ID_PARAM, Types.INTEGER));
		
		compile();
	}
}
