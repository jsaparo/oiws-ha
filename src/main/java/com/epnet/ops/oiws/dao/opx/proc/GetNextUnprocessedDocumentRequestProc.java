package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class GetNextUnprocessedDocumentRequestProc extends StoredProcedure  {
	public static final String GET_NEXT_UNPROCESSED_DOC_REQUEST_PROC_NAME = "dbo.prx_get_next_unprocessed_DocumentRequest";
	
	@Autowired(required=false)
	public GetNextUnprocessedDocumentRequestProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, GET_NEXT_UNPROCESSED_DOC_REQUEST_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.DELAY_PARAM, Types.TIMESTAMP));
		
		compile();
	}
}
