package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author jsaparo
 *
 */
public enum ContainerType {
	QUOTE(1),
	TRIAL(2),
	ORDER(3),
	CUSTOMER(4),
	LOST(5),
	PACKAGE(6);

    private static final Map<Integer, ContainerType> ENUMS_BY_ID = new HashMap<Integer, ContainerType>();
	static {
		ENUMS_BY_ID.put(ORDER.getId(), ORDER);
		ENUMS_BY_ID.put(QUOTE.getId(), QUOTE);
		ENUMS_BY_ID.put(CUSTOMER.getId(), CUSTOMER);
		ENUMS_BY_ID.put(TRIAL.getId(), TRIAL);
		ENUMS_BY_ID.put(LOST.getId(), LOST);
		ENUMS_BY_ID.put(PACKAGE.getId(), PACKAGE);
	}
	
    private int id;   

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    private ContainerType(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static ContainerType getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }
}