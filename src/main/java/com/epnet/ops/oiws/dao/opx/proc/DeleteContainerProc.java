package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class DeleteContainerProc extends StoredProcedure {
	public static final String DELETE_CONTAINER_PROC_NAME = "prx_delete_Container_by_pk";
	
	@Autowired(required=false)
	public DeleteContainerProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, DELETE_CONTAINER_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.CASCADE_DELETE_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));
		
		compile();
	}
}
