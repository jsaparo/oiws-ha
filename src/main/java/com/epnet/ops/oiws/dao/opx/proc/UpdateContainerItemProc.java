package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class UpdateContainerItemProc extends StoredProcedure {
	public static final String UPDATE_CONTAINER_ITEM_PROC_NAME = "prx_update_ContainerItem_by_pk";
	
	@Autowired(required=false)
	public UpdateContainerItemProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, UPDATE_CONTAINER_ITEM_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ITEM_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ITEM_SEQUENCE_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_TYPE_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_PRICE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_AMOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.GROSS_PRICE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.START_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.END_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.GROUP_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.TRIAL_INTERFACES_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.ACTIVE_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.TERM_IN_MONTHS_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));
		
		compile();
	}}
