package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class UpdateItemAccessingSiteProc extends StoredProcedure {
	
	public static final String UPDATE_ITEM_ACCESSING_SITE_PROC_NAME = "prx_update_ContainerItemAccessingSite_by_pk";

	@Autowired(required=false)
	public UpdateItemAccessingSiteProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, UPDATE_ITEM_ACCESSING_SITE_PROC_NAME);

		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ITEM_ACCESSING_SITE_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.INTERFACE_IDENTIFIERS_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));
		
		compile();
	}	
}
