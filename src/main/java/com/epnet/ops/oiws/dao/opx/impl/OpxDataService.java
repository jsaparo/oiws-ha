package com.epnet.ops.oiws.dao.opx.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.epnet.ops.oiws.beans.ObjectFactory;
import com.epnet.ops.oiws.dao.opx.IOpxDataService;
import com.epnet.ops.oiws.dao.opx.OpxConstants;
import com.epnet.ops.oiws.dao.opx.OpxException;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerInstallments;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerItemAccessingSite;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerAssociation;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerOptions;
import com.epnet.ops.oiws.dao.opx.dto.OpxDocumentRequest;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainer;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerItem;
import com.epnet.ops.oiws.dao.opx.dto.OpxItem;
import com.epnet.ops.oiws.dao.opx.proc.CreateContainerAssociationProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateContainerInstallmentsProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateContainerItemAccessingSiteProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateContainerItemProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateContainerOptionsProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateContainerProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateDocumentRequestProc;
import com.epnet.ops.oiws.dao.opx.proc.CreateItemProc;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerAssociationProc;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerInstallmentsProc;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerItemAccessingSiteProc;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerItemByContainerAndItem;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerItemProc;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerOptionsProc;
import com.epnet.ops.oiws.dao.opx.proc.DeleteContainerProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerAssociationProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerAssociationsProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerByStatusAndTypeProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerInstallmentsProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerItemAccessingSiteProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerItemAccessingSitesProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerItemProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerItemsProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerOptionsProc;
import com.epnet.ops.oiws.dao.opx.proc.GetContainerProc;
import com.epnet.ops.oiws.dao.opx.proc.GetDocumentRequestProc;
import com.epnet.ops.oiws.dao.opx.proc.GetItemProc;
import com.epnet.ops.oiws.dao.opx.proc.GetNextUnprocessedDocumentRequestProc;
import com.epnet.ops.oiws.dao.opx.proc.ListContainersProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateItemStateProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateContainerInstallmentsProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateContainerItemAccessingSiteProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateContainerItemProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateContainerOptionsProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateContainerProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateContainerStatusProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateDocumentRequestStatusProc;
import com.epnet.ops.oiws.dao.opx.proc.UpdateItemProc;
import com.epnet.ops.oiws.dao.opx.type.ContainerStatus;
import com.epnet.ops.oiws.dao.opx.type.ContainerType;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestStatus;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestType;
import com.epnet.ops.oiws.dao.opx.type.ItemState;
import com.epnet.ops.oiws.service.impl.OIServiceImpl;

/**
 * General OPX data access implementation.
 * 
 * @author jsaparo
 *
 */
@Component
@Transactional
public class OpxDataService implements IOpxDataService {
	private static final int DEFAULT_ITEM_LIST_SIZE = 20;
	
	private Logger log = Logger.getLogger(OIServiceImpl.class);

	ObjectFactory factory = new ObjectFactory();
	
	@Resource(name="createContainerItemProc")
	private CreateContainerItemProc createContainerItemProc;

	@Resource(name="createContainerProc")
	private CreateContainerProc createContainerProc;

	@Resource(name="createItemProc")
	private CreateItemProc createItemProc;

	@Resource(name="deleteContainerItemProc")
	private DeleteContainerItemProc deleteContainerItemProc;

	@Resource(name="deleteContainerProc")
	private DeleteContainerProc deleteContainerProc;

	@Resource(name="getContainerByStatusAndTypeProc")
	private GetContainerByStatusAndTypeProc getContainerByStatusAndTypeProc;

	@Resource(name="getContainerProc")
	private GetContainerProc getContainerProc;
	
	@Resource(name="getContainerItemsProc")
	private GetContainerItemsProc getContainerItemsProc;
	
	@Resource(name="getContainerItemProc")
	private GetContainerItemProc getContainerItemProc;
	
	@Resource(name="getItemProc")
	private GetItemProc getItemProc;
	
	@Resource(name="updateContainerProc")
	private UpdateContainerProc updateContainerProc;
	
	@Resource(name="updateContainerStatusProc")
	private UpdateContainerStatusProc updateContainerStatusProc;
	
	@Resource(name="updateContainerItemProc")
	private UpdateContainerItemProc updateContainerItemProc;
	
	@Resource(name="updateItemProc")
	private UpdateItemProc updateItemProc;
	
	@Resource(name="listContainersProc")
	private ListContainersProc listContainersProc;
	
	@Resource(name="createDocumentRequestProc")
	private CreateDocumentRequestProc createDocumentRequestProc;
	
	@Resource(name="updateDocumentRequestStatusProc")
	private UpdateDocumentRequestStatusProc updateDocumentRequestStatusProc;
	
	@Resource(name="getNextUnprocessedDocumentRequestProc")
	private GetNextUnprocessedDocumentRequestProc getNextUnprocessedDocumentRequestProc;
	
	@Resource(name="getDocumentRequestProc")
	private GetDocumentRequestProc getDocumentRequestProc;
	
	@Resource(name="createContainerAssociationProc")
	private CreateContainerAssociationProc createContainerAssociationProc;

	@Resource(name="deleteContainerAssociationProc")
	private DeleteContainerAssociationProc deleteContainerAssociationProc;

	@Resource(name="getContainerAssociationProc")
	private GetContainerAssociationProc getContainerAssociationProc;

	@Resource(name="getContainerAssociationsProc")
	private GetContainerAssociationsProc getContainerAssociationsProc;

	@Resource(name="createContainerItemAccessingSiteProc")
	private CreateContainerItemAccessingSiteProc createContainerItemAccessingSiteProc;
	
	@Resource(name="deleteContainerItemAccessingSiteProc")
	private DeleteContainerItemAccessingSiteProc deleteContainerItemAccessingSiteProc;
	
	@Resource(name="getContainerItemAccessingSiteProc")
	private GetContainerItemAccessingSiteProc getContainerItemAccessingSiteProc;
	
	@Resource(name="getContainerItemAccessingSitesProc")
	private GetContainerItemAccessingSitesProc getContainerItemAccessingSitesProc;
	
	@Resource(name="updateContainerItemAccessingSiteProc")
	private UpdateContainerItemAccessingSiteProc updateContainerItemAccessingSiteProc;

	@Resource(name="deleteContainerItemByContainerAndItem")
	private DeleteContainerItemByContainerAndItem deleteContainerItemByContainerAndItem;
	
	@Resource(name="createContainerOptionsProc")
	private CreateContainerOptionsProc createContainerOptionsProc;
	
	@Resource(name="updateContainerOptionsProc")
	private UpdateContainerOptionsProc updateContainerOptionsProc;
	
	@Resource(name="deleteContainerOptionsProc")
	private DeleteContainerOptionsProc deleteContainerOptionsProc;
	
	@Resource(name="getContainerOptionsProc")
	private GetContainerOptionsProc getContainerOptionsProc;

	@Resource(name="createContainerInstallmentsProc")
	private CreateContainerInstallmentsProc createContainerInstallmentsProc;
	
	@Resource(name="updateContainerInstallmentsProc")
	private UpdateContainerInstallmentsProc updateContainerInstallmentsProc;
	
	@Resource(name="deleteContainerInstallmentsProc")
	private DeleteContainerInstallmentsProc deleteContainerInstallmentsProc;
	
	@Resource(name="getContainerInstallmentsProc")
	private GetContainerInstallmentsProc getContainerInstallmentsProc;
	
	@Resource(name="updateItemStateProc")
	private UpdateItemStateProc updateItemStateProc;	

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createContainer(com.epnet.ops.oiws.dao.opx.dto.OpxContainer)
	 */
	@Override
	public int createContainer(OpxContainer container) throws OpxException {
		log.debug("Create a new container of type: " + container.getContainerTypeInd() + 
			" for customer: " + container.getCustId());
		
		// Set up parameters
		Map<String, Object> params = createNewContainerParams(container);

		// Create a new container in the database
		try {
			// Insert record
			Map<String, Object> results = createContainerProc.execute(params);
			
			// Extract id
			Object id = extractSingleRowValue(results, OpxConstants.CONTAINER_ID_FIELD);
			if (id == null) {
				throw new OpxException(
					"Internal error creating container for customer - " +
					"No id returned from database for customer: " + container.getCustId());
			}
			
			container.setContainerId((Integer) id);

			// Save order options
			if (container.getOptions() != null) {
				container.getOptions().setContainerId(container.getContainerId());
				createContainerOptions(container.getOptions());
			}
			
			// Save order installments
			if (container.getInstallments() != null) {
				container.getInstallments().setContainerId(container.getContainerId());
				createContainerInstallments(container.getInstallments());
			}

		} catch (Exception ex) {
			throw new OpxException(
				"Error creating container for customer: " + container.getCustId() + " ->" + ex.getMessage(), ex);
		}
		
		return container.getContainerId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#updateContainer(com.epnet.ops.oiws.dao.opx.dto.OpxContainer)
	 */
	@Override
	public void updateContainer(OpxContainer container) throws OpxException {
		log.debug("Update existing container with id: " + container.getContainerId());
		
		// Set up parameters
		Map<String, Object> params = createUpdatableContainerParams(container);

		// Create a new container in the database
		try {
			updateContainerProc.execute(params);
			
			// Save order options
			if (container.getOptions() != null) {
				OpxContainerOptions existingOptions = getContainerOptions(container.getContainerId());
				if (existingOptions == null) {
					createContainerOptions(container.getOptions());
				} else {
					updateContainerOptions(container.getOptions());
				}
			}
			
			// Save order installments
			if (container.getInstallments() != null) {
				OpxContainerInstallments existingInstallments = getContainerInstallments(container.getContainerId());
				if (existingInstallments == null) {
					createContainerInstallments(container.getInstallments());
				} else {
					updateContainerInstallments(container.getInstallments());
				}
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error updating container for id: " + container.getContainerId() + " ->" + ex.getMessage(), ex);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainer(int)
	 */
	@Override
	public OpxContainer getContainer(int containerId, boolean headerOnly) throws OpxException {
		log.debug("Getting container for id: " + containerId);
				
		OpxContainer container = null;
		
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, Integer.valueOf(containerId));
		
		// Create a new container in the database
		try {
			// Populate container object
			Map<String, Object> results = getContainerProc.execute(params);
			Map<String, Object> row = extractSingleRow(results);
			
			if (row == null) {
				throw new OpxException("Container with id: " + containerId + " does not exist");
			}
			
			container = constructContainerFromRow(row);
			
			if (!headerOnly) {
				// Populate container items
				container.setContainerItems(getContainerItems(containerId));
				
				// Populate container associations (packages)
				container.setContainerAssociations(getContainerAssociations(containerId));
			}

			// Fetch options
			container.setOptions(getContainerOptions(containerId));
			
			// Fetch installments
			container.setInstallments(getContainerInstallments(containerId));
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting container for id: " + containerId + " ->" + ex.getMessage(), ex);
		}
		
		return container;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#listContainers(java.lang.String, com.epnet.ops.oiws.dao.opx.type.ContainerType, java.lang.Integer)
	 */
	@Override
	public List<OpxContainer> listContainers(
		String custId, ContainerType containerType, Integer salesRepId) throws OpxException {
		
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CUST_ID_PARAM, custId);
		params.put(OpxConstants.SALES_REP_ID_PARAM, salesRepId);
		params.put(OpxConstants.CONTAINER_TYPE_ID_PARAM, containerType.getId());

		List<OpxContainer> containers = null;
		
		try {
			// Fetch container rows
			Map<String, Object> results = listContainersProc.execute(params);
			List<Map<String, Object>> rows = extractRows(results);
			
			// Populate container objects
			if (!CollectionUtils.isEmpty(rows)) {
				containers = new ArrayList<OpxContainer>(rows.size());
				for (Map<String, Object> row : rows) {
					containers.add(constructContainerFromRow(row));
				}
			}
			
		} catch (Exception ex) {
			throw new OpxException(
				"Error listing containers for customer: " + custId + " type: " + containerType + 
				" salesRep: " + salesRepId  + " -> " + ex.getMessage(), ex);
		}
		
		return containers;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerItems(int)
	 */
	@Override
	public List<OpxContainerItem> getContainerItems(int containerId) throws OpxException {

		List<OpxContainerItem> containerItems = new ArrayList<OpxContainerItem>(DEFAULT_ITEM_LIST_SIZE);
		
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, Integer.valueOf(containerId));
		
		// Create a new container in the database
		try {
			
			List<Map<String, Object>> rows = extractRows(getContainerItemsProc.execute(params));
			
			if (!CollectionUtils.isEmpty(rows)) {
				// Add item to container
				for (Map<String, Object> row : rows) {
					OpxContainerItem containerItem = constructContainerItemFromRow(row);
					containerItem.setItem(getItem(containerItem.getItemId()));
					containerItems.add(containerItem);
					
					// Fetch accessing site collections for item from db
					List<OpxContainerItemAccessingSite> opxAccessingSites = 
						getContainerItemAccessingSites(containerItem.getContainerId(), containerItem.getItemId());
					
					// Construct accessing site collections within item
					if (!CollectionUtils.isEmpty(opxAccessingSites)) {
						for (OpxContainerItemAccessingSite opxAs : opxAccessingSites) {
							containerItem.getAccessingSites().add(opxAs);
						}
					}
				}
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting container items for id: " + containerId + " ->" + ex.getMessage(), ex);
		}
		
		return containerItems;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerItem(int, int)
	 */
	@Override
	public OpxContainerItem getContainerItem(int containerItemId) throws OpxException {
		OpxContainerItem containerItem = null;
		
		try {
			// Set up parameters
			Map<String, Object> params = createParams();
			params.put(OpxConstants.CONTAINER_ITEM_ID_PARAM, Integer.valueOf(containerItemId));

			// Fetch container item row
			Map<String, Object> results = getContainerItemProc.execute(params);
			Map<String, Object> row = extractSingleRow(results);
			
			// Init container item object from row
			containerItem = constructContainerItemFromRow(row);
			containerItem.setItem(getItem(containerItem.getItemId()));
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting container item for id: " + containerItemId + " ->" + ex.getMessage(), ex);
		}
		
		return containerItem;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createContainerItem(com.epnet.ops.oiws.dao.opx.dto.OpxContainerItem)
	 */
	@Override
	public int createContainerItem(OpxContainerItem containerItem) throws OpxException {
		log.debug("Create new container item for product: " + containerItem.getItem().getProductId());
		
		// If there is no item currently associated with the container item being created,
		// create the item record and associate it with the new container item. Otherwise
		// we are associating a new container item with an existing item.
		if (containerItem.getItemId() == null) {
			containerItem.setItemId(createItem(containerItem.getItem()));
		}
		
		// Insert new container item record
		Map<String, Object> params = createContainerItemParams(containerItem);
		
		// Insert record
		Object id = null;
		
		try {
			Map<String, Object> results = createContainerItemProc.execute(params);
			id = extractSingleRowValue(results, OpxConstants.CONTAINER_ITEM_ID_FIELD);
		} catch (Exception ex) {
			throw new OpxException("Error creating item ->" + ex.getMessage(), ex);
		}
		
		// Extract id
		if (id == null) {
			throw new OpxException(
				"Internal error creating container item for customer - " +
				"No id returned from database for container: " + containerItem.getContainerId() +
				" and item " + containerItem.getItemId());
		}
		
		containerItem.setContainerItemId((Integer) id);

		return containerItem.getContainerId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createItem(com.epnet.ops.oiws.dao.opx.dto.OpxItem)
	 */
	@Override
	public int createItem(OpxItem item) throws OpxException {
		Map<String, Object> params = createItemParams(item, false);

		try {
			// Insert record
			Map<String, Object> results = createItemProc.execute(params);
	
			// Extract id
			Object id = extractSingleRowValue(results, OpxConstants.ITEM_ID_FIELD);
			if (id == null) {
				throw new OpxException(
					"Internal error creating item for product. No item id returned for product:  " + item.getProductId());
			}
			
			item.setItemId((Integer) id);
		} catch (Exception ex) {
			throw new OpxException("Error creating item ->" + ex.getMessage(), ex);
		}
		
		return item.getItemId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#updateContainerItem(com.epnet.ops.oiws.dao.opx.dto.OpxItem)
	 */
	@Override
	public void updateContainerItem(OpxContainerItem containerItem) throws OpxException {
		try { 
			Map<String, Object> params = createContainerItemParams(containerItem);
			updateContainerItemProc.execute(params);
			updateItem(containerItem.getItem());
		} catch (Exception ex) {
			throw new OpxException(
				"Error updating container item: " + containerItem.getContainerItemId() + " ->" + ex.getMessage(), ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#updateItem(com.epnet.ops.oiws.dao.opx.dto.OpxItem)
	 */
	public void updateItem(OpxItem item) throws OpxException {
		try { 
			Map<String, Object> params = createItemParams(item, true);
			updateItemProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException(
				"Error updating item: " + item.getItemId() + " ->" + ex.getMessage(), ex);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerItemMap(int)
	 */
	@Override
	public Map<Integer, OpxContainerItem> getContainerItemMap(int containerId) throws OpxException {
		
		// Fetch existing items
		List<OpxContainerItem> existingItems = getContainerItems(containerId);
		
		// Map by id
		Map<Integer, OpxContainerItem> mappedItems = new LinkedHashMap<Integer, OpxContainerItem> (existingItems.size());
		for (OpxContainerItem containerItem : existingItems) {
			mappedItems.put(containerItem.getItemId(), containerItem);
		}
		
		return mappedItems;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#removeItemFromContainer(int, int)
	 */
	@Override
	public void removeItemFromContainer(int containerId, int itemId) throws OpxException {
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, containerId);
		params.put(OpxConstants.ITEM_ID_PARAM, itemId);
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);
		
		try {
			deleteContainerItemByContainerAndItem.execute(params);
		} catch (Exception ex) {
			throw new OpxException(
				"Error deleting container item for container: " + containerId + 
				" and item: " + itemId + " ->" + ex.getMessage(), ex);
		}		
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#setContainerStatus(int, com.epnet.ops.oiws.dao.opx.type.OrderStatus)
	 */
	@Override
	public void updateContainerStatus(int containerId, ContainerStatus status) throws OpxException {
		// delegate for now until this method is removed
		setContainerState(containerId, status, null);
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainersByStatusAndType(com.epnet.ops.oiws.dao.opx.type.OrderStatus, com.epnet.ops.oiws.dao.opx.type.OrderCategory)
	 */
	@Override
	public OpxContainer getFirstContainersByStatusAndType(ContainerStatus status, ContainerType type) throws OpxException {
		OpxContainer container = null;
		
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_STATUS_ID_PARAM, (short) status.getId());
		params.put(OpxConstants.CONTAINER_TYPE_ID_PARAM, (short) type.getId());
		
		try {
			Map<String, Object> row = extractSingleRow(getContainerByStatusAndTypeProc.execute(params));
			
			if (!CollectionUtils.isEmpty(row)) {
				container = constructContainerFromRow(row);
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting first container for status: " + status + " ->" + ex.getMessage(), ex);
		}
		
		return container;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createDocumentGenerationRequest(int, com.epnet.ops.oiws.dao.opx.type.DocumentRequestType, java.lang.String, java.lang.String)
	 */
	@Override
	public Integer createDocumentGenerationRequest (
		int containerId, DocumentRequestType docType, String baseFileName, String languageCode) throws OpxException {
		
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, containerId);
		params.put(OpxConstants.DOCUMENT_REQUEST_TYPE_CODE_PARAM, docType.getId());
		params.put(OpxConstants.BASE_FILENAME_PARAM, baseFileName);
		params.put(OpxConstants.LANGUAGE_CODE_PARAM, languageCode);
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

		Integer docRequestId = null;
		
		try {
			Map<String, Object> row = extractSingleRow(createDocumentRequestProc.execute(params));
			
			if (!CollectionUtils.isEmpty(row)) {
				docRequestId = (Integer) row.get(OpxConstants.DOCUMENT_REQUEST_ID_FIELD);
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error creating document generation request ->" + ex.getMessage(), ex);
		}
		
		return docRequestId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getNextUnprocessedDocumentRequestId()
	 */
	@Override
	public Integer getNextUnprocessedDocumentRequestId() throws OpxException {
		Map<String, Object> params = createParams();
		params.put(OpxConstants.DELAY_PARAM, null);

		Integer docRequestId = null;
		
		try {
			Map<String, Object> row = extractSingleRow(getNextUnprocessedDocumentRequestProc.execute(params));
			
			if (!CollectionUtils.isEmpty(row)) {
				docRequestId = (Integer) row.get(OpxConstants.DOCUMENT_REQUEST_ID_FIELD);
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting next unprocessed document request ->" + ex.getMessage(), ex);
		}
			
		return docRequestId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#setDocumentGenerationRequestStatus(int, com.epnet.ops.oiws.dao.opx.type.DocumentRequestStatus, java.lang.String)
	 */
	@Override
	public void setDocumentGenerationRequestStatus(
		int docRequestId, DocumentRequestStatus status, String fileLocation) throws OpxException {
		
		try {
			Map<String, Object> params = createParams();
			params.put(OpxConstants.DOCUMENT_REQUEST_ID_PARAM, docRequestId);
			params.put(OpxConstants.DOCUMENT_REQUEST_STATUS_ID_PARAM, status.getId());
			params.put(OpxConstants.FILE_LOCATION_PARAM, fileLocation);
			params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);
			
			this.updateDocumentRequestStatusProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException(
				"Error setting document request status for id: " + docRequestId + " ->" + ex.getMessage(), ex);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getDocumentGenerationRequest(int)
	 */
	@Override
	public OpxDocumentRequest getDocumentGenerationRequest(int docRequestId) throws OpxException {		
		OpxDocumentRequest docRequest = null;
		
		Map<String, Object> params = createParams();
		params.put(OpxConstants.DOCUMENT_REQUEST_ID_PARAM, docRequestId);
		
		// Create a new container in the database
		try {
			// Populate container object
			Map<String, Object> results = getDocumentRequestProc.execute(params);
			Map<String, Object> row = extractSingleRow(results);
			
			if (row == null) {
				throw new OpxException("Document request with id: " + docRequestId + " does not exist");
			}
			
			docRequest = constructDocumentRequestFromRow(row);
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting document request for id: " + docRequestId + " ->" + ex.getMessage(), ex);
		}
		
		return docRequest;
	}	

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getItem(int)
	 */
	@Override
	public OpxItem getItem(int itemId) throws OpxException {
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.ITEM_ID_PARAM, Integer.valueOf(itemId));

		Map<String, Object> row = null;
		
		try {
			row = extractSingleRow(getItemProc.execute(params));
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting item for id: " + itemId + " ->" + ex.getMessage(), ex);
		}
	
		if (row == null) {
			throw new OpxException("Order item not found for id: " + itemId);
		}
		
		return constructItemFromRow(row);
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#deleteContainer(int)
	 */
	@Override
	public void deleteContainer(int containerId) throws OpxException {
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, Integer.valueOf(containerId));
		params.put(OpxConstants.CASCADE_DELETE_PARAM, Boolean.TRUE);
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);
		
		try {
			deleteContainerProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException("Error deleting container: " + containerId);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createContainerAssociation(int, int)
	 */
	@Override
	public Integer createContainerAssociation(int parentContainerId, int childContainerId) throws OpxException {
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.PARENT_CONTAINER_ID_PARAM, Integer.valueOf(parentContainerId));
		params.put(OpxConstants.CHILD_CONTAINER_ID_PARAM, Integer.valueOf(childContainerId));
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);
		
		Integer associationId;
		
		try {
			Map<String, Object> results = createContainerAssociationProc.execute(params);
	
			// Extract id
			associationId = (Integer) extractSingleRowValue(results, OpxConstants.CONTAINER_ASSOCIATION_ID_FIELD);
			if (associationId == null) {
				throw new OpxException("Error obtaining id for newly created container association");
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error creating container association between parent container: " + 
					parentContainerId + " and child id: " + childContainerId);
		}
		
		return associationId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#deleteContainerAssociation(int)
	 */
	@Override
	public void deleteContainerAssociation(int containerAssociationId) throws OpxException {
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ASSOCIATION_ID_PARAM, Integer.valueOf(containerAssociationId));
		
		try {
			deleteContainerAssociationProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException(
				"Error deleting container association for id: " + containerAssociationId);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerAssociation(int)
	 */
	@Override
	public OpxContainerAssociation getContainerAssociation(int containerAssociationId) throws OpxException {
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ASSOCIATION_ID_PARAM, Integer.valueOf(containerAssociationId));

		Map<String, Object> row = null;
		
		try {
			row = extractSingleRow(getItemProc.execute(params));
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting container association for id: " + containerAssociationId + " ->" + ex.getMessage(), ex);
		}
	
		if (row == null) {
			throw new OpxException("Container association not found for id: " + containerAssociationId);
		}
		
		return constructContainerAssociationFromRow(row);
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerAssociations(int)
	 */
	@Override
	public List<OpxContainerAssociation> getContainerAssociations(int containerId) throws OpxException {
			
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.PARENT_CONTAINER_ID_PARAM, containerId);

		List<OpxContainerAssociation> associations = null;
		
		try {
			// Fetch container rows
			Map<String, Object> results = getContainerAssociationsProc.execute(params);
			List<Map<String, Object>> rows = extractRows(results);
			
			// Populate container objects
			if (!CollectionUtils.isEmpty(rows)) {
				associations = new ArrayList<OpxContainerAssociation>(rows.size());
				for (Map<String, Object> row : rows) {
					associations.add(constructContainerAssociationFromRow(row));
				}
			}
			
		} catch (Exception ex) {
			throw new OpxException(
				"Error container associations for container: " + containerId + " -> " + ex.getMessage(), ex);
		}
		
		return associations;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createContainerItemAccessingSite(com.epnet.ops.oiws.dao.opx.dto.OpxAccessingSite)
	 */
	@Override
	public Integer createContainerItemAccessingSite(OpxContainerItemAccessingSite opxAccessingSite) throws OpxException {
			
		try {
			Map<String, Object> params = createParams();
			params.put(OpxConstants.CONTAINER_ID_PARAM, opxAccessingSite.getContainerId());
			params.put(OpxConstants.ITEM_ID_PARAM, opxAccessingSite.getItemId());
			params.put(OpxConstants.CUST_ID_PARAM, opxAccessingSite.getCustId());
			params.put(OpxConstants.INTERFACE_IDENTIFIERS_PARAM, opxAccessingSite.getInterfaceIds());
			params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

			createContainerItemAccessingSiteProc.execute(params);
			
			// Insert record
			Map<String, Object> results = createItemProc.execute(params);
	
			// Extract id
			Object id = extractSingleRowValue(results, OpxConstants.ITEM_ID_FIELD);
			if (id == null) {
				throw new OpxException(
					"Internal error item accessing site. container id: " + 
					opxAccessingSite.getContainerId() +	" and item id: " + opxAccessingSite.getItemId());
			}
			
			opxAccessingSite.setContainerItemAccessingSiteId((Integer) id);
			
		} catch (Exception ex) {
			throw new OpxException(
				"Error creating item accessing container id: " + opxAccessingSite.getContainerId() + 
				" and item id: " + opxAccessingSite.getItemId() + " ->" + ex.getMessage(), ex);
		}
		
		return opxAccessingSite.getContainerItemAccessingSiteId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#deleteContainerItemAccessingSite(int)
	 */
	@Override
	public void deleteContainerItemAccessingSite(int containerItemAccessingSiteId) throws OpxException {
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ITEM_ACCESSING_SITE_ID_PARAM, Integer.valueOf(containerItemAccessingSiteId));
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);
		
		try {
			deleteContainerItemAccessingSiteProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException("Error deleting container item accessing site for id:: " + containerItemAccessingSiteId);
		}	
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#updateContainerItemAccessingSite(int, java.lang.String)
	 */
	@Override
	public void updateContainerItemAccessingSite(int containerItemAccessingSiteId, String interfaceIds)
		throws OpxException {
		
		try {
			Map<String, Object> params = createParams();
			params.put(OpxConstants.CONTAINER_ITEM_ACCESSING_SITE_ID_PARAM, containerItemAccessingSiteId);
			params.put(OpxConstants.INTERFACE_IDENTIFIERS_PARAM, interfaceIds);
			params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);
			
			updateContainerItemAccessingSiteProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException(
				"Error setting item accessing sites for id: " + containerItemAccessingSiteId + " ->" + ex.getMessage(), ex);
		}		
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerItemAccessingSites(int, int)
	 */
	@Override
	public List<OpxContainerItemAccessingSite> getContainerItemAccessingSites(
		int containerId, int itemId) throws OpxException {

		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, containerId);
		params.put(OpxConstants.ITEM_ID_PARAM, itemId);
		params.put(OpxConstants.CUST_ID_PARAM, null);

		List<OpxContainerItemAccessingSite> accessingSites = null;
		
		try {
			// Fetch container rows
			Map<String, Object> results = getContainerItemAccessingSitesProc.execute(params);
			List<Map<String, Object>> rows = extractRows(results);
			
			// Populate container objects
			if (!CollectionUtils.isEmpty(rows)) {
				accessingSites = new ArrayList<OpxContainerItemAccessingSite>(rows.size());
				for (Map<String, Object> row : rows) {
					accessingSites.add(constructContainerItemAccessingSiteFromRow(row));
				}
			}
			
		} catch (Exception ex) {
			throw new OpxException(
				"Error container associations for container: " + containerId + " -> " + ex.getMessage(), ex);
		}
		
		return accessingSites;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createContainerOptions(com.epnet.ops.oiws.dao.opx.dto.OpxContainerOptions)
	 */
	@Override
	public Integer createContainerOptions(OpxContainerOptions opxContainerOptions) throws OpxException {
		Map<String, Object> params = createContainerOptionsParams(opxContainerOptions);

		try {
			createContainerOptionsProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException("Error creating item ->" + ex.getMessage(), ex);
		}

		return opxContainerOptions.getContainerId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerOptions(int)
	 */
	@Override
	public OpxContainerOptions getContainerOptions(int containerId) throws OpxException {
		log.debug("Getting container for id: " + containerId);
		
		OpxContainerOptions containerOptions = null;
		
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, Integer.valueOf(containerId));
		
		try {
			// Populate container object
			Map<String, Object> results = getContainerOptionsProc.execute(params);
			Map<String, Object> row = extractSingleRow(results);
			
			if (row != null) {
				containerOptions = constructContainerOptionsFromRow(row);
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting container options for id: " + containerId + " ->" + ex.getMessage(), ex);
		}
		
		return containerOptions;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#deleteContainerOptinos(int)
	 */
	@Override
	public void deleteContainerOptions(int containerId) throws OpxException {
		throw new OpxException("Delete container options not implemented");
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#updateContainerOptions(com.epnet.ops.oiws.dao.opx.dto.OpxContainerOptions)
	 */
	@Override
	public void updateContainerOptions(OpxContainerOptions opxContainerOptions) throws OpxException {
		Map<String, Object> params = createContainerOptionsParams(opxContainerOptions);

		try {
			updateContainerOptionsProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException("Error updating container options for container: " + 
				opxContainerOptions.getContainerId() + " ->" + ex.getMessage(), ex);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#createContainerInstallments(com.epnet.ops.oiws.dao.opx.dto.OpxContainerInstallments)
	 */
	@Override
	public Integer createContainerInstallments(OpxContainerInstallments opxContainerInstallments) throws OpxException {
		Map<String, Object> params = createContainerInstallmentsParams(opxContainerInstallments);

		try {
			createContainerInstallmentsProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException("Error creating installments ->" + ex.getMessage(), ex);
		}

		return opxContainerInstallments.getContainerId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#getContainerInstallments(int)
	 */
	@Override
	public OpxContainerInstallments getContainerInstallments(int containerId) throws OpxException {
		log.debug("Getting container for id: " + containerId);
		
		OpxContainerInstallments containerOptions = null;
		
		// Set up parameters
		Map<String, Object> params = createParams();
		params.put(OpxConstants.CONTAINER_ID_PARAM, Integer.valueOf(containerId));
		
		try {
			// Populate container object
			Map<String, Object> results = getContainerInstallmentsProc.execute(params);
			Map<String, Object> row = extractSingleRow(results);
			
			if (row != null) {
				containerOptions = constructContainerInstallmentsFromRow(row);
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error getting container installments for id: " + containerId + " ->" + ex.getMessage(), ex);
		}
		
		return containerOptions;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#deleteContainerInstallments(int)
	 */
	@Override
	public void deleteContainerInstallments(int containerId) throws OpxException {
		throw new OpxException("Delete container installments not implemented");
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#updateContainerInstallments(com.epnet.ops.oiws.dao.opx.dto.OpxContainerInstallments)
	 */
	@Override
	public void updateContainerInstallments(OpxContainerInstallments opxContainerInstallments) throws OpxException {
		Map<String, Object> params = createContainerInstallmentsParams(opxContainerInstallments);

		try {
			updateContainerInstallmentsProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException("Error updating container installments for container: " + 
				opxContainerInstallments.getContainerId() + " ->" + ex.getMessage(), ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.dao.opx.IOpxDataService#setItemState(int, com.epnet.ops.oiws.dao.opx.type.ItemState)
	 */
	@Override
	public void setItemState(int itemId, ItemState itemState) throws OpxException {
		try {
			Map<String, Object> params = createParams();
			params.put(OpxConstants.ITEM_ID_PARAM, itemId);
			params.put(OpxConstants.ITEM_STATE_ID_PARAM, (short) itemState.getId());
			params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

			updateItemStateProc.execute(params);
		} catch (Exception ex) {
			throw new OpxException(
				"Error updating state for item: " + itemId + " ->" + ex.getMessage(), ex);
		}
	}

	@Override
	public void setContainerState(int containerId, ContainerStatus orderStatus, ItemState itemState) throws OpxException {
		try {
			Map<String, Object> params = createParams();
			params.put(OpxConstants.CONTAINER_ID_PARAM, containerId);
			params.put(OpxConstants.CONTAINER_STATUS_ID_PARAM, (short) orderStatus.getId());
			params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

			updateContainerStatusProc.execute(params);
			
			// Set item states if given
			if (itemState != null) {
				List<OpxContainerItem> items = getContainerItems(containerId);
				if (!CollectionUtils.isEmpty(items)) {
					for (OpxContainerItem item : items) {
						setItemState(item.getItemId(), itemState);
					}
				}
			}
		} catch (Exception ex) {
			throw new OpxException(
				"Error updating status for container: " + containerId + " ->" + ex.getMessage(), ex);
		}
	}

	/**
	 * Extract the first single row of data from a returned sproc result set
	 * 
	 * LinkedHashMap / First entry value - List of Maps / First Entry -> Map of record values
	 * @param resultSets
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> extractRows(Map<String, Object> resultSets) {
		if (CollectionUtils.isEmpty(resultSets)) {
			return null;
		}
		
		return (List<Map<String, Object>>) resultSets.values().iterator().next();
	}
	
	/**
	 * Extract the first single row of data from a returned sproc result set
	 * 
	 * LinkedHashMap / First entry value - List of Maps / First Entry -> Map of record values
	 * @param resultSets
	 * @return
	 */
	private Map<String, Object> extractSingleRow(Map<String, Object> resultSets) {
		if (CollectionUtils.isEmpty(resultSets)) {
			return null;
		}
		
		List<Map<String, Object>> valueMaps = extractRows(resultSets);
		
		return !CollectionUtils.isEmpty(valueMaps) ? valueMaps.get(0) : null;
	}
	
	/**
	 * Extract a single value from the first single row of data from a returned sproc result set
	 * 
	 * @param resultSets
	 * @param key
	 * @return
	 */
	private Object extractSingleRowValue(Map<String, Object> resultSets, String key) {
		Map<String, Object> dataRow = extractSingleRow(resultSets);
		
		if (CollectionUtils.isEmpty(dataRow)) {
			return null;
		}
		
		return dataRow.get(key);
	}
	
	/**
	 * Convert BigDecimal to double value;
	 * 
	 * @param bd Object to convert
	 * @return Converted value, or null
	 */
	private Double getDoubleValue(BigDecimal bd) {
		return bd != null ? Double.valueOf(bd.doubleValue()) : null;
	}
	
	/**
	 * Compare the ids of items specified in a given container object to update with
	 * the existing ids for the container. Existing ids not specified in the given
	 * container should be removed.
	 * 
	 * @param container The container object to save
	 * @return A list of existing ids not specified in the given container
	 * @throws OpxException
	 */
	private Set<Integer> resolveItemsToDelete(OpxContainer container) throws OpxException {
		Set<Integer> itemsToDelete = new HashSet<Integer>(DEFAULT_ITEM_LIST_SIZE);
		
		// Record ids specified in the given container object
		Set<Integer> givenIds = new HashSet<Integer>(DEFAULT_ITEM_LIST_SIZE);
		for (OpxContainerItem containerItem : container.getContainerItems()) {
			if (containerItem.getItem().getItemId() != null) {
				givenIds.add(containerItem.getItem().getItemId());
			}
		}
		
		// Fetch existing items from the database
		List<OpxContainerItem> existingItems = getContainerItems(container.getContainerId());
		for (OpxContainerItem containerItem : existingItems) {
			if (!givenIds.contains(containerItem.getItemId())) {
				itemsToDelete.add(containerItem.getItemId());
			}
		}
		
		return itemsToDelete;
	}
	
	/**
	 * Utility method for constructing a default parameter map
	 * @return An object map
	 */
	private Map<String, Object> createParams() {
		return new LinkedHashMap<String, Object>();
	}
	
	/**
	 * Construct a parameter map for a given container for parameters that are used
	 * for updating a container.
	 * 
	 * @param container The container to create a map for
	 * 
	 * @return A map passed into a db call
	 */
	private Map<String, Object> createNewContainerParams(OpxContainer container) {
		Map<String, Object> params = createCommonContainerParams(container);

		// Params specifically for container creation
		params.put(OpxConstants.CUST_ID_PARAM, container.getCustId());
		params.put(OpxConstants.CONTAINER_TYPE_ID_PARAM, container.getContainerTypeInd());
		params.put(OpxConstants.CONTAINER_STATUS_ID_PARAM, container.getContainerStatusInd());
		params.put(OpxConstants.CONTAINER_SOURCE_ID_ID_PARAM, container.getContainerSourceInd());

		return params;
	}
	
	/**
	 * Construct a parameter map for a given container for parameters that are used
	 * for creating a new container.
	 * 
	 * @param container The container to create a map for
	 * 
	 * @return A map passed into a db call
	 */
	private Map<String, Object> createUpdatableContainerParams(OpxContainer container) {
		Map<String, Object> params = createCommonContainerParams(container);

		// Params specifically for container modification
		params.put(OpxConstants.CONTAINER_ID_PARAM, container.getContainerId());

		return params;
	}
	
	/**
	 * Construct a parameter map for a given container for parameters that are common
	 * to both creating and updating a container.
	 * 
	 * @param container The container to create a map for
	 * 
	 * @return A map passed into a db call
	 */
	private Map<String, Object> createCommonContainerParams(OpxContainer container) {
		Map<String, Object> params = createParams();

		params.put(OpxConstants.OPPORTUNITY_ID_PARAM, container.getOpportunityId());
		params.put(OpxConstants.SALES_REP_ID_PARAM, container.getSalesRepId());
		params.put(OpxConstants.CURRENCY_CODE_PARAM, container.getCurrencyCode());
		params.put(OpxConstants.LANGUAGE_CODE_PARAM, container.getLanguageCode());
		params.put(OpxConstants.CUSTOMER_CONTACT_ID_PARAM, container.getCustomerContactId());
		params.put(OpxConstants.PO_NUMBER_PARAM, container.getPurchaseOrderNumber());
		params.put(OpxConstants.BILLING_ADDRESS_ID_PARAM, container.getBillingAddressId());
		params.put(OpxConstants.BILLING_CONTACT_ID_PARAM, container.getBillingContactId());
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

		return params;
	}
	
	/**
	 * Construct a parameter map for a given item.
	 * 
	 * @param opxItem The item to create a map for
	 * 
	 * @return A map passed into a db call
	 */
	private Map<String, Object> createContainerItemParams(OpxContainerItem containerItem) {
		Map<String, Object> params = createParams();

		params.put(OpxConstants.CONTAINER_ID_PARAM, containerItem.getContainerId());
		params.put(OpxConstants.CONTAINER_ITEM_ID_PARAM, containerItem.getContainerItemId());
		params.put(OpxConstants.ITEM_ID_PARAM, containerItem.getItemId());
		params.put(OpxConstants.CONTAINER_ITEM_SEQUENCE_PARAM, Integer.valueOf(containerItem.getSequence()));
		params.put(OpxConstants.DISCOUNT_TYPE_ID_PARAM, containerItem.getDiscountTypeInd());
		params.put(OpxConstants.DISCOUNT_PRICE_PARAM, containerItem.getDiscountPrice());
		params.put(OpxConstants.DISCOUNT_AMOUNT_PARAM, containerItem.getDiscountAmount());
		params.put(OpxConstants.GROSS_PRICE_PARAM, containerItem.getGrossPrice());
		params.put(OpxConstants.START_DATE_PARAM, containerItem.getStartDate());
		params.put(OpxConstants.END_DATE_PARAM, containerItem.getEndDate());
		params.put(OpxConstants.GROUP_CODE_PARAM, containerItem.getGroupCode());
		params.put(OpxConstants.TRIAL_INTERFACES_PARAM, containerItem.getTrialInterfaces());
		params.put(OpxConstants.ACTIVE_PARAM, containerItem.getActive());
		params.put(OpxConstants.TERM_IN_MONTHS_PARAM, containerItem.getTermInMonths());

		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

		return params;
	}
	
	/**
	 * Construct a parameter map for a given item.
	 * 
	 * @param opxItem The item to create a map for
	 * @param update If true, an update, otherwise a create
	 * @return A map passed into a db call
	 */
	private Map<String, Object> createItemParams(OpxItem opxItem, boolean update) {
		Map<String, Object> params = createParams();

		if (update) {
			params.put(OpxConstants.ITEM_ID_PARAM, opxItem.getItemId());
		}
		params.put(OpxConstants.PRODUCT_ID_PARAM, opxItem.getProductId());
		params.put(OpxConstants.START_DATE_PARAM, opxItem.getStartDate());
		params.put(OpxConstants.END_DATE_PARAM, opxItem.getEndDate());
		params.put(OpxConstants.TERM_IN_MONTHS_PARAM, opxItem.getTermInMonths());
		params.put(OpxConstants.GROSS_PRICE_PARAM, opxItem.getGrossPrice());
		params.put(OpxConstants.PREMIUM_PRICING_DICSOUNT_PARAM, opxItem.getPremiumPricingDiscount());
		params.put(OpxConstants.PRIMARY_AGENCY_DICSOUNT_PARAM, opxItem.getPrimaryAgencyDiscount());
		params.put(OpxConstants.COMPLIMENTARY_PARAM, opxItem.getComplimentary());
		params.put(OpxConstants.QUANTITY_PARAM, opxItem.getQuantity());
		params.put(OpxConstants.BUSINESS_MODEL_ID_PARAM, opxItem.getBusinessModelId());
		params.put(OpxConstants.ROYALTY_AMOUNT_PARAM, opxItem.getRoyaltyAmount());
		params.put(OpxConstants.ROYALTY_COMMENT_PARAM, opxItem.getRoyaltyComment());
		params.put(OpxConstants.RENEWAL_PARAM, opxItem.getRenewal());
		params.put(OpxConstants.RENEWAL_SUBSCRIPTION_ID_PARAM, opxItem.getRenewalSubscriptionId());
		params.put(OpxConstants.RENEWAL_NOTE_PARAM, opxItem.getRenewalNote());
		params.put(OpxConstants.CONTRACT_NUMBER_PARAM, opxItem.getContractNumber());
		if (!update) {
			params.put(OpxConstants.ITEM_STATE_ID_PARAM, ItemState.WIP.getId());
		}
		params.put(OpxConstants.PROBABILITY_PARAM, opxItem.getProbability());
		params.put(OpxConstants.EXPECTED_CLOSE_DATE_PARAM, opxItem.getExpectedCloseDate());
		params.put(OpxConstants.TID_IDENTIFIER_PARAM, opxItem.getTidIdentifier());
		params.put(OpxConstants.LIST_RATE_PARAM, opxItem.getListRate());
		params.put(OpxConstants.MAX_SIMULTANEOUS_USERS_PARAM, opxItem.getMaxSimultaneousUsers());
		params.put(OpxConstants.PRICED_THROUGH_CONSORTIUM_ID_PARAM, opxItem.getPricedThroughConsortiumId());
		params.put(OpxConstants.PURCHASE_ORDER_NUMBER_PARAM, opxItem.getPurchaseOrderNumber());
		params.put(OpxConstants.INVOICE_START_DATE_PARAM, opxItem.getInvoiceStartDate());
		params.put(OpxConstants.MULTI_YEAR_PAYMENTS_PARAM, opxItem.getMultiYearPayments());
		params.put(OpxConstants.AMOUNT_YEAR1_PARAM, opxItem.getAmountYear1());
		params.put(OpxConstants.AMOUNT_YEAR2_PARAM, opxItem.getAmountYear2());
		params.put(OpxConstants.AMOUNT_YEAR3_PARAM, opxItem.getAmountYear3());
		params.put(OpxConstants.AMOUNT_YEAR4_PARAM, opxItem.getAmountYear4());
		params.put(OpxConstants.AMOUNT_YEAR5_PARAM, opxItem.getAmountYear5());
		params.put(OpxConstants.AMOUNT_YEAR6_PARAM, opxItem.getAmountYear6());
		params.put(OpxConstants.AMOUNT_YEAR7_PARAM, opxItem.getAmountYear7());
		params.put(OpxConstants.AMOUNT_YEAR8_PARAM, opxItem.getAmountYear8());
		params.put(OpxConstants.AMOUNT_YEAR9_PARAM, opxItem.getAmountYear9());
		params.put(OpxConstants.AMOUNT_YEAR10_PARAM, opxItem.getAmountYear10());
		params.put(OpxConstants.UPGRADE_DOWNGRADE_FLAG_PARAM, opxItem.getUpgradeDowngradeFlag());
		params.put(OpxConstants.EHIS_PURCHASE_COUNT_PARAM, opxItem.getEhisPurchaseCount());
		params.put(OpxConstants.EHIS_FREE_COUNT_PARAM, opxItem.getEhisFreeCount());
		params.put(OpxConstants.BOOKS_CONSORTIA_PARAM, opxItem.getBooksConsortia());
		params.put(OpxConstants.BOOKS_SHARED_PARAM, opxItem.getBooksShared());
		params.put(OpxConstants.PREV_LIST_RATE_PARAM, opxItem.getPrevListRate());
		params.put(OpxConstants.PREV_TERM_PARAM, opxItem.getPrevTerm());
		params.put(OpxConstants.PREV_ROYALTY_AMOUNT_PARAM, opxItem.getPrevRoyaltyAmount());
		params.put(OpxConstants.PREV_ROYALTY_COMMENT_PARAM, opxItem.getPrevRoyaltyComment());
		params.put(OpxConstants.STATUS_TAG_PARAM, opxItem.getStatusTag());
		params.put(OpxConstants.ITEM_FTE_PARAM, opxItem.getItemFTE());
		params.put(OpxConstants.ITEM_NOTE_PARAM, opxItem.getItemNote());
		params.put(OpxConstants.CONTROLLING_CUSTOMER_INDICATOR_PARAM, opxItem.getControllingCustomerIndicator());
		params.put(OpxConstants.DISCOUNT_TYPE_ID_PARAM, opxItem.getDiscountTypeInd());
		params.put(OpxConstants.DISCOUNT_PRICE_PARAM, opxItem.getDiscountPrice());
		params.put(OpxConstants.DISCOUNT_AMOUNT_PARAM, opxItem.getDiscountAmount());
		params.put(OpxConstants.LOST_REASON_PARAM, opxItem.getLostReason());
		params.put(OpxConstants.LOST_NOTES_PARAM, opxItem.getLostNotes());
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

		return params;
	}
	
	/**
	 * Construct a container object from a row of data retrieved from the database.
	 * 
	 * @param row The row of data
	 * @return A new container object
	 */
	private OpxContainer constructContainerFromRow(Map<String, Object> row) {
		OpxContainer container = new OpxContainer();
		
		container.setContainerId((Integer) row.get(OpxConstants.CONTAINER_ID_FIELD));
		container.setCustId((String) row.get(OpxConstants.CUSTOMER_ID_FIELD));
		container.setContainerTypeInd((Short) row.get(OpxConstants.CONTAINER_TYPE_ID_FIELD));
		container.setContainerStatusInd((Short) row.get(OpxConstants.CONTAINER_STATUS_ID_FIELD));
		container.setContainerSourceInd((Short) row.get(OpxConstants.CONTAINER_SOURCE_ID_FIELD));
		container.setOpportunityId((Integer) row.get(OpxConstants.OPPORTUNITY_ID_FIELD));
		container.setSalesRepId((Integer) row.get(OpxConstants.SALES_REP_ID_FIELD));
		container.setCurrencyCode((String) row.get(OpxConstants.CURRENCY_CODE_FIELD));
		container.setLanguageCode((String) row.get(OpxConstants.LANGUAGE_CODE_FIELD));
		container.setCustomerContactId((Integer) row.get(OpxConstants.CUSTOMER_CONTACT_ID_FIELD));
		container.setPurchaseOrderNumber((String) row.get(OpxConstants.PO_NUMBER_FIELD));
		container.setBillingAddressId((Integer) row.get(OpxConstants.BILLING_ADDRESS_ID_FIELD));
		container.setBillingContactId((Integer) row.get(OpxConstants.BILLING_CONTACT_ID_FIELD));
		Object createDate = row.get(OpxConstants.CREATED_DATE_FIELD);
		Object modifyDate = row.get(OpxConstants.MODIFIED_DATE_FIELD);
		container.setModifiedDate((Date) (modifyDate != null ? modifyDate : createDate));

		return container;
	}

	/**
	 * Construct a container item object from a row of data retrieved from the database.
	 * 
	 * @param row The row of data
	 * @return A new container item object
	 */
	private OpxItem constructItemFromRow(Map<String, Object> row) {
		// Populate associated item
		OpxItem opxItem = new OpxItem();
		opxItem.setItemId((Integer) row.get(OpxConstants.ITEM_ID_FIELD));
		opxItem.setProductId((Integer) row.get(OpxConstants.PRODUCT_ID_FIELD));
		opxItem.setStartDate((Date) row.get(OpxConstants.START_DATE_FIELD));
		opxItem.setEndDate((Date) row.get(OpxConstants.END_DATE_FIELD));
		opxItem.setBusinessModelId((Integer) row.get(OpxConstants.BUSINESS_MODEL_ID_FIELD));
		opxItem.setTermInMonths((Integer) row.get(OpxConstants.TERM_IN_MONTHS_FIELD));
		opxItem.setGrossPrice(getDoubleValue((BigDecimal) row.get(OpxConstants.GROSS_PRICE_FIELD)));
		opxItem.setDiscountTypeInd((Short) row.get(OpxConstants.DISCOUNT_TYPE_ID_FIELD));
		opxItem.setDiscountAmount(getDoubleValue((BigDecimal) row.get(OpxConstants.DISCOUNT_AMOUNT_FIELD)));
		opxItem.setDiscountPrice(getDoubleValue((BigDecimal) row.get(OpxConstants.DISCOUNT_PRICE_FIELD)));
		opxItem.setPremiumPricingDiscount(getDoubleValue((BigDecimal) row.get(OpxConstants.PREMIUM_PRICING_DICSOUNT_FIELD)));
		opxItem.setPrimaryAgencyDiscount(getDoubleValue((BigDecimal) row.get(OpxConstants.PRIMARY_AGENCY_DICSOUNT_FIELD)));
		opxItem.setComplimentary((Boolean) row.get(OpxConstants.COMPLIMENTARY_FIELD));
		opxItem.setQuantity((Integer) row.get(OpxConstants.QUANTITY_FIELD));
		opxItem.setRoyaltyAmount(getDoubleValue((BigDecimal) row.get(OpxConstants.ROYALTY_AMOUNT_FIELD)));
		opxItem.setRoyaltyComment((String) row.get(OpxConstants.ROYALTY_COMMENT_FIELD));
		opxItem.setRenewal((Boolean) row.get(OpxConstants.RENEWAL_FIELD));
		opxItem.setRenewalSubscriptionId((Integer) row.get(OpxConstants.RENEWAL_SUBSCRIPTION_ID_FIELD));
		opxItem.setRenewalNote((String) row.get(OpxConstants.RENEWAL_NOTE_FIELD));
		opxItem.setContractNumber((String) row.get(OpxConstants.CONTRACT_NUMBER_FIELD));
		opxItem.setItemStateInd((Short) row.get(OpxConstants.ITEM_STATE_ID_FIELD));
		opxItem.setProbability(getDoubleValue((BigDecimal) row.get(OpxConstants.PROBABILITY_FIELD)));
		Object createDate = row.get(OpxConstants.CREATED_DATE_FIELD);
		Object modifyDate = row.get(OpxConstants.MODIFIED_DATE_FIELD);
		opxItem.setModifiedDate((Date) (modifyDate != null ? modifyDate : createDate));		
		opxItem.setExpectedCloseDate((Date) row.get(OpxConstants.EXPECTED_CLOSE_DATE_FIELD));
		opxItem.setTidIdentifier((Integer) row.get(OpxConstants.TID_IDENTIFIER_FIELD));
		opxItem.setListRate((Double) row.get(OpxConstants.LIST_RATE_FIELD));
		opxItem.setMaxSimultaneousUsers((Integer) row.get(OpxConstants.MAX_SIMULTANEOUS_USERS_FIELD));
		opxItem.setPurchaseOrderNumber((String) row.get(OpxConstants.PURCHASE_ORDER_NUMBER_FIELD));
		opxItem.setInvoiceStartDate((Date) row.get(OpxConstants.INVOICE_START_DATE_FIELD));
		opxItem.setMultiYearPayments((Integer) row.get(OpxConstants.MULTI_YEAR_PAYMENTS_FIELD));
		opxItem.setAmountYear1(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR1_FIELD)));
		opxItem.setAmountYear2(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR2_FIELD)));
		opxItem.setAmountYear3(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR3_FIELD)));
		opxItem.setAmountYear4(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR4_FIELD)));
		opxItem.setAmountYear5(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR5_FIELD)));
		opxItem.setAmountYear6(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR6_FIELD)));
		opxItem.setAmountYear7(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR7_FIELD)));
		opxItem.setAmountYear8(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR8_FIELD)));
		opxItem.setAmountYear9(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR9_FIELD)));
		opxItem.setAmountYear10(getDoubleFromBD((BigDecimal) row.get(OpxConstants.AMOUNT_YEAR10_FIELD)));
		opxItem.setUpgradeDowngradeFlag((String) row.get(OpxConstants.UPGRADE_DOWNGRADE_FLAG_FIELD));
		opxItem.setEhisPurchaseCount((Integer) row.get(OpxConstants.EHIS_PURCHASE_COUNT_FIELD));
		opxItem.setEhisFreeCount((Integer) row.get(OpxConstants.EHIS_FREE_COUNT_FIELD));
		opxItem.setBooksConsortia((Boolean) row.get(OpxConstants.BOOKS_CONSORTIA_FIELD));
		opxItem.setBooksShared((Boolean) row.get(OpxConstants.BOOKS_SHARED_FIELD));
		opxItem.setPrevListRate((Double) row.get(OpxConstants.PREV_LIST_RATE_FIELD));
		opxItem.setPrevTerm((Integer) row.get(OpxConstants.PREV_TERM_FIELD));
		opxItem.setPrevRoyaltyAmount((Double) row.get(OpxConstants.PREV_ROYALTY_AMOUNT_FIELD));
		opxItem.setPrevRoyaltyComment((String) row.get(OpxConstants.PREV_ROYALTY_COMMENT_FIELD));
		opxItem.setStatusTag((String) row.get(OpxConstants.STATUS_TAG_FIELD));
		opxItem.setItemFTE((Integer) row.get(OpxConstants.ITEM_FTE_FIELD));
		opxItem.setItemNote((String) row.get(OpxConstants.ITEM_NOTE_FIELD));
		opxItem.setPricedThroughConsortiumId((Integer) row.get(OpxConstants.PRICED_THROUGH_CONSORTIUM_ID_FIELD));
		opxItem.setLostReason((String) row.get(OpxConstants.LOST_REASON_FIELD));
		opxItem.setLostNotes((String) row.get(OpxConstants.LOST_NOTES_FIELD));
		
		return opxItem;
	}
	
	/**
	 * Construct an item object from a row of data retrieved from the database.
	 * 
	 * @param row The row of data
	 * @return A new container item object
	 */
	private OpxContainerItem constructContainerItemFromRow(Map<String, Object> row) {
		// Populate container item
		OpxContainerItem containerItem = new OpxContainerItem();
		containerItem.setContainerId((Integer) row.get(OpxConstants.CONTAINER_ID_FIELD));
		containerItem.setContainerItemId((Integer) row.get(OpxConstants.CONTAINER_ITEM_ID_FIELD));
		containerItem.setSequence((Integer) row.get(OpxConstants.CONTAINER_ITEM_SEQUENCE_FIELD));
		containerItem.setItemId((Integer) row.get(OpxConstants.ITEM_ID_FIELD));
		containerItem.setGrossPrice(getDoubleValue((BigDecimal) row.get(OpxConstants.GROSS_PRICE_FIELD)));
		containerItem.setDiscountTypeInd((Short) row.get(OpxConstants.DISCOUNT_TYPE_ID_FIELD));
		containerItem.setDiscountAmount(getDoubleValue((BigDecimal) row.get(OpxConstants.DISCOUNT_AMOUNT_FIELD)));
		containerItem.setDiscountPrice(getDoubleValue((BigDecimal) row.get(OpxConstants.DISCOUNT_PRICE_FIELD)));
		containerItem.setStartDate((Date) row.get(OpxConstants.START_DATE_FIELD));
		containerItem.setEndDate((Date) row.get(OpxConstants.END_DATE_FIELD));
		containerItem.setGroupCode((String) row.get(OpxConstants.GROUP_CODE_FIELD));
		containerItem.setTrialInterfaces((String) row.get(OpxConstants.TRIAL_INTERFACES_FIELD));
		containerItem.setActive((Boolean) row.get(OpxConstants.ACTIVE_FIELD));
		
		return containerItem;
	}
	
	private OpxDocumentRequest constructDocumentRequestFromRow(Map<String, Object> row) {
		OpxDocumentRequest docRequest = new OpxDocumentRequest();
		docRequest.setDocumentRequestId((Integer) row.get(OpxConstants.DOCUMENT_REQUEST_ID_FIELD));
		docRequest.setContainerId((Integer) row.get(OpxConstants.CONTAINER_ID_FIELD));
		docRequest.setBaseFileName((String) row.get(OpxConstants.BASE_FILENAME_FIELD));
		docRequest.setLanguageCode((String) row.get(OpxConstants.LANGUAGE_CODE_FIELD));
		docRequest.setDocRequestTypeCode((String) row.get(OpxConstants.DOCUMENT_REQUEST_TYPE_CODE_FIELD));
		docRequest.setStatusId((Short) row.get(OpxConstants.DOCUMENT_REQUEST_STATUS_ID_PARAM));
		
		return docRequest;
	}
	
	private OpxContainerAssociation constructContainerAssociationFromRow(Map<String, Object> row) {
		OpxContainerAssociation association = new OpxContainerAssociation();
		association.setContainerAssociationId((Integer) row.get(OpxConstants.CONTAINER_ASSOCIATION_ID_FIELD));
		association.setParentContainerId((Integer) row.get(OpxConstants.PARENT_CONTAINER_ID_FIELD));
		association.setChildContainerId((Integer) row.get(OpxConstants.CHILD_CONTAINER_ID_FIELD));
		
		return association;
	}
	
	private OpxContainerItemAccessingSite constructContainerItemAccessingSiteFromRow(Map<String, Object> row) {
		OpxContainerItemAccessingSite accessingSite = new OpxContainerItemAccessingSite();

		accessingSite.setContainerItemAccessingSiteId((Integer) row.get(OpxConstants.CONTAINER_ITEM_ACCESSING_SITE_ID_FIELD));
		accessingSite.setContainerId((Integer) row.get(OpxConstants.CONTAINER_ID_FIELD));
		accessingSite.setItemId((Integer) row.get(OpxConstants.ITEM_ID_FIELD));
		accessingSite.setCustId((String) row.get(OpxConstants.CUSTOMER_ID_FIELD));
		accessingSite.setInterfaceIds((String) row.get(OpxConstants.INTERFACE_IDENTIFIERS_FIELD));
		
		return accessingSite;
	}
	
	private Map<String, Object> createContainerOptionsParams(OpxContainerOptions containerOptions) {
		Map<String, Object> params = createParams();

		params.put(OpxConstants.CONTAINER_ID_PARAM, containerOptions.getContainerId());
		params.put(OpxConstants.ORDER_DESCRIPTION_PARAM, containerOptions.getOrderDescription());
		params.put(OpxConstants.CUSTOMER_NOTE_PARAM, containerOptions.getCustomerNote());
		params.put(OpxConstants.TERMS_AND_CONDITIONS_PARAM, containerOptions.getTermsAndConditions());
		params.put(OpxConstants.INVOICE_DISPLAY_DATE_PARAM, containerOptions.getInvoiceDisplayDate());
		params.put(OpxConstants.DISPLAY_SUBSCRIPTION_ID_PARAM, containerOptions.getDisplaySubscriptionId());
		params.put(OpxConstants.DISPLAY_PACKAGED_ITEM_PRICES_PARAM, containerOptions.getDisplayPackagedItemPrices());
		params.put(OpxConstants.DISPLAY_ITEM_INTERFACES_PARAM, containerOptions.getDisplayItemInterfaces());
		params.put(OpxConstants.DISPLAY_SIMULTANEOUS_USERS_PARAM, containerOptions.getDisplaySimultaneousUsers());
		params.put(OpxConstants.QUOTE_EXPIRE_DAYS_PARAM, containerOptions.getQuoteExpireDays());
		params.put(OpxConstants.SHOW_MARKETING_ON_QUOTE_PARAM, containerOptions.getShowMarketingOnQuote());
		params.put(OpxConstants.DISPLAY_ITEM_DISCOUNT_PARAM, containerOptions.getDisplayItemDiscount());
		params.put(OpxConstants.DISPLAY_ACCESSING_SITE_COUNT_PARAM, containerOptions.getDisplayAccessingSiteCount());
		params.put(OpxConstants.PAY_BY_CREDIT_CART_PARAM, containerOptions.getPayByCreditCard());
		params.put(OpxConstants.DISPLAY_PACKAGE_NAMES_PARAM, containerOptions.getDisplayPackageNames());
		params.put(OpxConstants.DISPLAY_TAX_ID_PARAM, containerOptions.getDisplayTaxId());
		params.put(OpxConstants.TAX_LANGUAGE_PARAM, containerOptions.getTaxLanguage());
		params.put(OpxConstants.TAX_LABEL_PARAM, containerOptions.getTaxLabel());
		params.put(OpxConstants.TAX_PERCENT_PARAM, containerOptions.getTaxPercent());
		params.put(OpxConstants.INVOICE_ASAP_PARAM, containerOptions.getInvoiceAsap());
		params.put(OpxConstants.RUSH_ORDER_PARAM, containerOptions.getRushOrder());
		params.put(OpxConstants.QUOTED_CURRENCY_PARAM, containerOptions.getQuotedCurrency());
		params.put(OpxConstants.INVOICE_NOTE_PARAM, containerOptions.getInvoiceNote());
		params.put(OpxConstants.ORDER_PROCESSING_NOTE_PARAM, containerOptions.getOrderProcessingNote());
		params.put(OpxConstants.PURCHASE_ORDER_FORM_NOTE_PARAM, containerOptions.getPurchaseOrderFormNote());
		params.put(OpxConstants.COMMUNICATION_MODE_CODE_PARAM, containerOptions.getCommunicationModeCode());
		params.put(OpxConstants.GROUP_CODE_PARAM, containerOptions.getGroupCode());
		params.put(OpxConstants.QUOTE_ACCESSING_SITE_NOTE_PARAM, containerOptions.getQuoteAccessingSiteNote());
		params.put(OpxConstants.DISCOUNT_TYPE_ID_PARAM, containerOptions.getDiscountTypeInd());
		params.put(OpxConstants.DISCOUNT_PRICE_PARAM, containerOptions.getDiscountPrice());
		params.put(OpxConstants.DISCOUNT_AMOUNT_PARAM, containerOptions.getDiscountAmount());
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

		return params;
	}
	
	private OpxContainerOptions constructContainerOptionsFromRow(Map<String, Object> row) {
		OpxContainerOptions containerOptions = new OpxContainerOptions();

		containerOptions.setContainerId((Integer) row.get(OpxConstants.CONTAINER_ID_FIELD));
		containerOptions.setOrderDescription((String) row.get(OpxConstants.ORDER_DESCRIPTION_FIELD));
		containerOptions.setCustomerNote((String) row.get(OpxConstants.CUSTOMER_NOTE_FIELD));
		containerOptions.setTermsAndConditions((String) row.get(OpxConstants.TERMS_AND_CONDITIONS_FIELD));
		containerOptions.setInvoiceDisplayDate((Date) row.get(OpxConstants.INVOICE_DISPLAY_DATE_FIELD));
		containerOptions.setDisplaySubscriptionId((Boolean) row.get(OpxConstants.DISPLAY_SUBSCRIPTION_ID_FIELD));
		containerOptions.setDisplayPackagedItemPrices((Boolean) row.get(OpxConstants.DISPLAY_PACKAGED_ITEM_PRICES_FIELD));
		containerOptions.setDisplayItemInterfaces((Boolean) row.get(OpxConstants.DISPLAY_ITEM_INTERFACES_FIELD));
		containerOptions.setDisplaySimultaneousUsers((Boolean) row.get(OpxConstants.DISPLAY_SIMULTANEOUS_USERS_FIELD));
		containerOptions.setQuoteExpireDays((Integer) row.get(OpxConstants.QUOTE_EXPIRE_DAYS_FIELD));
		containerOptions.setShowMarketingOnQuote((Boolean) row.get(OpxConstants.SHOW_MARKETING_ON_QUOTE_FIELD));
		containerOptions.setDisplayItemDiscount((Boolean) row.get(OpxConstants.DISPLAY_ITEM_DISCOUNT_FIELD));
		containerOptions.setDisplayAccessingSiteCount((Boolean) row.get(OpxConstants.DISPLAY_ACCESSING_SITE_COUNT_FIELD));
		containerOptions.setPayByCreditCard((Boolean) row.get(OpxConstants.PAY_BY_CREDIT_CART_FIELD));
		containerOptions.setDisplayPackageNames((Boolean) row.get(OpxConstants.DISPLAY_PACKAGE_NAMES_FIELD));
		containerOptions.setDisplayTaxId((Boolean) row.get(OpxConstants.DISPLAY_TAX_ID_FIELD));
		containerOptions.setTaxLanguage((String) row.get(OpxConstants.TAX_LANGUAGE_FIELD));
		containerOptions.setTaxLabel((String) row.get(OpxConstants.TAX_LABEL_FIELD));
		BigDecimal pct = (BigDecimal) row.get(OpxConstants.TAX_PERCENT_FIELD);
		if (pct != null) {
			containerOptions.setTaxPercent(pct.doubleValue());
		}
		containerOptions.setInvoiceAsap((Boolean) row.get(OpxConstants.INVOICE_ASAP_FIELD));
		containerOptions.setRushOrder((Boolean) row.get(OpxConstants.RUSH_ORDER_FIELD));
		containerOptions.setQuotedCurrency((String) row.get(OpxConstants.QUOTED_CURRENCY_FIELD));
		containerOptions.setInvoiceNote((String) row.get(OpxConstants.INVOICE_NOTE_FIELD));
		containerOptions.setOrderProcessingNote((String) row.get(OpxConstants.ORDER_PROCESSING_NOTE_FIELD));
		containerOptions.setPurchaseOrderFormNote((String) row.get(OpxConstants.PURCHASE_ORDER_FORM_NOTE_FIELD));
		containerOptions.setCommunicationModeCode((String) row.get(OpxConstants.COMMUNICATION_MODE_CODE_FIELD));
		containerOptions.setGroupCode((String) row.get(OpxConstants.GROUP_CODE_FIELD));
		containerOptions.setQuoteAccessingSiteNote((String) row.get(OpxConstants.QUOTE_ACCESSING_SITE_NOTE_FIELD));
		containerOptions.setGrossPrice(getDoubleValue((BigDecimal) row.get(OpxConstants.GROSS_PRICE_FIELD)));
		containerOptions.setDiscountTypeInd((Short) row.get(OpxConstants.DISCOUNT_TYPE_ID_FIELD));
		containerOptions.setDiscountAmount(getDoubleValue((BigDecimal) row.get(OpxConstants.DISCOUNT_AMOUNT_FIELD)));
		containerOptions.setDiscountPrice(getDoubleValue((BigDecimal) row.get(OpxConstants.DISCOUNT_PRICE_FIELD)));
		
		return containerOptions;
	}
	
	private Map<String, Object> createContainerInstallmentsParams(OpxContainerInstallments containerInstallments) {
		Map<String, Object> params = createParams();

		params.put(OpxConstants.CONTAINER_ID_PARAM, containerInstallments.getContainerId());
		params.put(OpxConstants.INSTALLMENT_COUNT_PARAM, containerInstallments.getInstallmentCount());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE1_PARAM, containerInstallments.getInstallmentInvoiceDate1());
		params.put(OpxConstants.INSTALLMENT_AMOUNT1_PARAM, containerInstallments.getInstallmentAmount1());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE2_PARAM, containerInstallments.getInstallmentInvoiceDate2());
		params.put(OpxConstants.INSTALLMENT_AMOUNT2_PARAM, containerInstallments.getInstallmentAmount2());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE3_PARAM, containerInstallments.getInstallmentInvoiceDate3());
		params.put(OpxConstants.INSTALLMENT_AMOUNT3_PARAM, containerInstallments.getInstallmentAmount3());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE4_PARAM, containerInstallments.getInstallmentInvoiceDate4());
		params.put(OpxConstants.INSTALLMENT_AMOUNT4_PARAM, containerInstallments.getInstallmentAmount4());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE5_PARAM, containerInstallments.getInstallmentInvoiceDate5());
		params.put(OpxConstants.INSTALLMENT_AMOUNT5_PARAM, containerInstallments.getInstallmentAmount5());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE6_PARAM, containerInstallments.getInstallmentInvoiceDate6());
		params.put(OpxConstants.INSTALLMENT_AMOUNT6_PARAM, containerInstallments.getInstallmentAmount6());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE7_PARAM, containerInstallments.getInstallmentInvoiceDate7());
		params.put(OpxConstants.INSTALLMENT_AMOUNT7_PARAM, containerInstallments.getInstallmentAmount7());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE8_PARAM, containerInstallments.getInstallmentInvoiceDate8());
		params.put(OpxConstants.INSTALLMENT_AMOUNT8_PARAM, containerInstallments.getInstallmentAmount8());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE9_PARAM, containerInstallments.getInstallmentInvoiceDate9());
		params.put(OpxConstants.INSTALLMENT_AMOUNT9_PARAM, containerInstallments.getInstallmentAmount9());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE10_PARAM, containerInstallments.getInstallmentInvoiceDate10());
		params.put(OpxConstants.INSTALLMENT_AMOUNT10_PARAM, containerInstallments.getInstallmentAmount10());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE11_PARAM, containerInstallments.getInstallmentInvoiceDate11());
		params.put(OpxConstants.INSTALLMENT_AMOUNT11_PARAM, containerInstallments.getInstallmentAmount11());
		params.put(OpxConstants.INSTALLMENT_INVOICE_DATE12_PARAM , containerInstallments.getInstallmentInvoiceDate12());
		params.put(OpxConstants.INSTALLMENT_AMOUNT12_PARAM, containerInstallments.getInstallmentAmount12());
		params.put(OpxConstants.SUPPRESS_RESULTS_PARAM, Boolean.FALSE);

		return params;
	}
	
	private OpxContainerInstallments constructContainerInstallmentsFromRow(Map<String, Object> row) {
		OpxContainerInstallments installments = new OpxContainerInstallments();
		
		installments.setContainerId((Integer) row.get(OpxConstants.CONTAINER_ID_FIELD));
		installments.setInstallmentCount((Integer) row.get(OpxConstants.INSTALLMENT_COUNT_FIELD));
		installments.setInstallmentInvoiceDate1((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE1_FIELD));
		installments.setInstallmentAmount1(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT1_FIELD)));
		installments.setInstallmentInvoiceDate2((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE2_FIELD));
		installments.setInstallmentAmount2(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT2_FIELD)));
		installments.setInstallmentInvoiceDate3((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE3_FIELD));
		installments.setInstallmentAmount3(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT3_FIELD)));
		installments.setInstallmentInvoiceDate4((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE4_FIELD));
		installments.setInstallmentAmount4(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT4_FIELD)));
		installments.setInstallmentInvoiceDate5((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE5_FIELD));
		installments.setInstallmentAmount5(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT5_FIELD)));
		installments.setInstallmentInvoiceDate6((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE6_FIELD));
		installments.setInstallmentAmount6(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT6_FIELD)));
		installments.setInstallmentInvoiceDate7((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE7_FIELD));
		installments.setInstallmentAmount7(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT7_FIELD)));
		installments.setInstallmentInvoiceDate8((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE8_FIELD));
		installments.setInstallmentAmount8(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT8_FIELD)));
		installments.setInstallmentInvoiceDate9((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE9_FIELD));
		installments.setInstallmentAmount9(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT9_FIELD)));
		installments.setInstallmentInvoiceDate10((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE10_FIELD));
		installments.setInstallmentAmount10(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT10_FIELD)));
		installments.setInstallmentInvoiceDate11((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE11_FIELD));
		installments.setInstallmentAmount11(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT11_FIELD)));
		installments.setInstallmentInvoiceDate12((Date) row.get(OpxConstants.INSTALLMENT_INVOICE_DATE12_FIELD ));
		installments.setInstallmentAmount12(getDoubleFromBD((BigDecimal) row.get(OpxConstants.INSTALLMENT_AMOUNT12_FIELD)));
		
		return installments;
	}
	
	private Double getDoubleFromBD(BigDecimal val) {
		return val != null ? Double.valueOf(val.doubleValue()) : null;
	}
}
