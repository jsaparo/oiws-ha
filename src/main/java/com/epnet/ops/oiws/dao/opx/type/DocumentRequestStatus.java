package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

public enum DocumentRequestStatus {
	PENDING(1),
	PROCESSING(2),
	COMPLETED(3),
	FAILED(4);

    private static final Map<Integer, DocumentRequestStatus> ENUMS_BY_ID = new HashMap<Integer, DocumentRequestStatus>();
	static {
		ENUMS_BY_ID.put(PENDING.getId(), PENDING);
		ENUMS_BY_ID.put(PROCESSING.getId(), PROCESSING);
		ENUMS_BY_ID.put(COMPLETED.getId(), COMPLETED);
		ENUMS_BY_ID.put(FAILED.getId(), FAILED);
	}
	
    private int id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    DocumentRequestStatus(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static DocumentRequestStatus getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }   
}