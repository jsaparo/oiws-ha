package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class CreateContainerInstallmentsProc extends StoredProcedure {

	public static final String CREATE_CONTAINER_INSTALLMENTS_PROC_NAME = "prx_insert_ContainerInstallment";
	
	@Autowired(required=false)
	public CreateContainerInstallmentsProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, CREATE_CONTAINER_INSTALLMENTS_PROC_NAME);

		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_COUNT_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE1_PARAM , Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT1_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE2_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT2_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE3_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT3_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE4_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT4_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE5_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT5_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE6_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT6_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE7_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT7_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE8_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT8_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE9_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT9_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE10_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT10_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE11_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT11_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_INVOICE_DATE12_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.INSTALLMENT_AMOUNT12_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));

		compile();
	}
}
