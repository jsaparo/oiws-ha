package com.epnet.ops.oiws.dao.opx.dto;

public class OpxContainerItemAccessingSite {
	private Integer containerItemAccessingSiteId;
	private Integer containerId;
	private Integer containerItemId;
	private Integer itemId;
	private String custId;
	private String interfaceIds;
	
	public Integer getContainerItemAccessingSiteId() {
		return containerItemAccessingSiteId;
	}
	public void setContainerItemAccessingSiteId(Integer containerItemAccessingSiteId) {
		this.containerItemAccessingSiteId = containerItemAccessingSiteId;
	}
	public Integer getContainerId() {
		return containerId;
	}
	public void setContainerId(Integer containerId) {
		this.containerId = containerId;
	}
	public Integer getContainerItemId() {
		return containerItemId;
	}
	public void setContainerItemId(Integer containerItemId) {
		this.containerItemId = containerItemId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getInterfaceIds() {
		return interfaceIds;
	}
	public void setInterfaceIds(String interfaceIds) {
		this.interfaceIds = interfaceIds;
	}
}
