package com.epnet.ops.oiws.dao.opx.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Simple object for reading/writing data in the OPX Container table.
 * 
 * @author jsaparo
 *
 */
public class OpxContainer {
	private static final int DEFAULT_ITEM_SIZE = 20;
	private static final int DEFAULT_ASSOCIATIONS_SIZE = 5;
	
	private Integer containerId;
	private String custId;
	private Short containerTypeInd;
	private Short containerStatusInd;
	private Short containerSourceInd;
	private Integer opportunityId;
	private Integer salesRepId;
	private String currencyCode;
	private String languageCode;
	private Integer customerContactId;
	private String purchaseOrderNumber;
	private Integer billingAddressId;
	private Integer billingContactId;	
	private List<OpxContainerItem> containerItems = new ArrayList<OpxContainerItem>(DEFAULT_ITEM_SIZE);
	private List<OpxContainerAssociation> containerAssociations = new ArrayList<OpxContainerAssociation>(DEFAULT_ASSOCIATIONS_SIZE);
	private Date modifiedDate;
	private OpxContainerOptions options;
	private OpxContainerInstallments installments;
	
	public Integer getContainerId() {
		return containerId;
	}
	public void setContainerId(Integer containerId) {
		this.containerId = containerId;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public Short getContainerTypeInd() {
		return containerTypeInd;
	}
	public void setContainerTypeInd(Short containerTypeInd) {
		this.containerTypeInd = containerTypeInd;
	}
	public Short getContainerStatusInd() {
		return containerStatusInd;
	}
	public void setContainerStatusInd(Short containerStatusInd) {
		this.containerStatusInd = containerStatusInd;
	}
	public Short getContainerSourceInd() {
		return containerSourceInd;
	}
	public void setContainerSourceInd(Short containerSourceInd) {
		this.containerSourceInd = containerSourceInd;
	}
	public Integer getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(Integer opportunityId) {
		this.opportunityId = opportunityId;
	}
	public Integer getSalesRepId() {
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public Integer getCustomerContactId() {
		return customerContactId;
	}
	public void setCustomerContactId(Integer customerContactId) {
		this.customerContactId = customerContactId;
	}
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	public List<OpxContainerItem> getContainerItems() {
		return containerItems;
	}
	public void setContainerItems(List<OpxContainerItem> containerItems) {
		this.containerItems = containerItems;
	}
	public Integer getBillingAddressId() {
		return billingAddressId;
	}
	public void setBillingAddressId(Integer billingAddressId) {
		this.billingAddressId = billingAddressId;
	}
	public Integer getBillingContactId() {
		return billingContactId;
	}
	public void setBillingContactId(Integer billingContactId) {
		this.billingContactId = billingContactId;
	}
	public List<OpxContainerAssociation> getContainerAssociations() {
		return containerAssociations;
	}
	public void setContainerAssociations(List<OpxContainerAssociation> containerAssociations) {
		this.containerAssociations = containerAssociations;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public OpxContainerOptions getOptions() {
		return options;
	}
	public void setOptions(OpxContainerOptions options) {
		this.options = options;
	}
	public OpxContainerInstallments getInstallments() {
		return installments;
	}
	public void setInstallments(OpxContainerInstallments installments) {
		this.installments = installments;
	}
}
