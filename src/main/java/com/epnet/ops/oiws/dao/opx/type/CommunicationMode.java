package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

public enum CommunicationMode {
	NONE(1000),
	EMAIL(1001),
	EMAILONCE(1002),
	MAIL(1003),
	FAX(1004),
	EMAILREP(1005);
	
    private static final Map<Integer, CommunicationMode> ENUMS_BY_ID = new HashMap<Integer, CommunicationMode>();
	static {
		ENUMS_BY_ID.put(NONE.getId(), NONE);
		ENUMS_BY_ID.put(EMAIL.getId(), EMAIL);
		ENUMS_BY_ID.put(EMAILONCE.getId(), EMAILONCE);
		ENUMS_BY_ID.put(MAIL.getId(), MAIL);
		ENUMS_BY_ID.put(FAX.getId(), FAX);
		ENUMS_BY_ID.put(EMAILREP.getId(), EMAILREP);
	}
	
    private int id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    CommunicationMode(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static CommunicationMode getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }   
}
