package com.epnet.ops.oiws.dao.opx.dto;

public class OpxDocumentRequest {
	Integer documentRequestId;
	Integer containerId;
	String docRequestTypeCode;
	String baseFileName;
	String languageCode;
	Short statusId;
	String fileLocation;
	
	public Integer getDocumentRequestId() {
		return documentRequestId;
	}
	public void setDocumentRequestId(Integer documentRequestId) {
		this.documentRequestId = documentRequestId;
	}
	public Integer getContainerId() {
		return containerId;
	}
	public void setContainerId(Integer containerId) {
		this.containerId = containerId;
	}
	public String getDocRequestTypeCode() {
		return docRequestTypeCode;
	}
	public void setDocRequestTypeCode(String docRequestTypeCode) {
		this.docRequestTypeCode = docRequestTypeCode;
	}
	public String getBaseFileName() {
		return baseFileName;
	}
	public void setBaseFileName(String baseFileName) {
		this.baseFileName = baseFileName;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public Short getStatusId() {
		return statusId;
	}
	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
}
