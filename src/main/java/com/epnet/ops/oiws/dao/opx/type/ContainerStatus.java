package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author jsaparo
 *
 */
public enum ContainerStatus {
	WIP(1),
	PROCESSED(2),
	SUBMITTED(3),
	APPROVAL(4),
	FAILED(5),
	REJECTED(6);
	
    private static final Map<Integer, ContainerStatus> ENUMS_BY_ID = new HashMap<Integer, ContainerStatus>();
	static {
		ENUMS_BY_ID.put(WIP.getId(), WIP);
		ENUMS_BY_ID.put(SUBMITTED.getId(), SUBMITTED);
		ENUMS_BY_ID.put(PROCESSED.getId(), PROCESSED);
		ENUMS_BY_ID.put(FAILED.getId(), FAILED);
		ENUMS_BY_ID.put(APPROVAL.getId(), APPROVAL);
		ENUMS_BY_ID.put(REJECTED.getId(), REJECTED);
	}
	
    private int id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    ContainerStatus(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static ContainerStatus getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }   
}
