package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class UpdateItemProc extends StoredProcedure {
	public static final String UPDATE_ITEM_PROC_NAME = "prx_update_Item_by_pk";
	
	@Autowired(required=false)
	public UpdateItemProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, UPDATE_ITEM_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.ITEM_ID_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.PRODUCT_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.START_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.END_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.TERM_IN_MONTHS_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.GROSS_PRICE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.PREMIUM_PRICING_DICSOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.PRIMARY_AGENCY_DICSOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.COMPLIMENTARY_PARAM, Types.BIT));
		declareParameter(new SqlParameter(OpxConstants.QUANTITY_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.BUSINESS_MODEL_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.ROYALTY_AMOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.ROYALTY_COMMENT_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.RENEWAL_PARAM, Types.BIT));
		declareParameter(new SqlParameter(OpxConstants.RENEWAL_SUBSCRIPTION_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.RENEWAL_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.CONTRACT_NUMBER_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.PROBABILITY_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.EXPECTED_CLOSE_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.TID_IDENTIFIER_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.LIST_RATE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.MAX_SIMULTANEOUS_USERS_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.PRICED_THROUGH_CONSORTIUM_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.PURCHASE_ORDER_NUMBER_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.INVOICE_START_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.MULTI_YEAR_PAYMENTS_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR1_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR2_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR3_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR4_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR5_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR6_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR7_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR8_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR9_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.AMOUNT_YEAR10_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.UPGRADE_DOWNGRADE_FLAG_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.EHIS_PURCHASE_COUNT_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.EHIS_FREE_COUNT_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.BOOKS_CONSORTIA_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.BOOKS_SHARED_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.PREV_LIST_RATE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.PREV_TERM_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.PREV_ROYALTY_AMOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.PREV_ROYALTY_COMMENT_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.STATUS_TAG_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.ITEM_FTE_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.ITEM_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.CONTROLLING_CUSTOMER_INDICATOR_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_TYPE_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_PRICE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_AMOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.LOST_REASON_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.LOST_NOTES_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));		
		
		compile();
	}
}
