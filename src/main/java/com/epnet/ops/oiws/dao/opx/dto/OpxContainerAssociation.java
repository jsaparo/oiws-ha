package com.epnet.ops.oiws.dao.opx.dto;

public class OpxContainerAssociation {
	private Integer containerAssociationId;
	private Integer parentContainerId;
	private Integer childContainerId;
	private Double discountPrice;
	private Double discountAmount;
	private Short discountTypeInd;
	
	public Integer getContainerAssociationId() {
		return containerAssociationId;
	}
	public void setContainerAssociationId(Integer containerAssociationId) {
		this.containerAssociationId = containerAssociationId;
	}
	public Integer getParentContainerId() {
		return parentContainerId;
	}
	public void setParentContainerId(Integer parentContainerId) {
		this.parentContainerId = parentContainerId;
	}
	public Integer getChildContainerId() {
		return childContainerId;
	}
	public void setChildContainerId(Integer childContainerId) {
		this.childContainerId = childContainerId;
	}
	public Double getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Short getDiscountTypeInd() {
		return discountTypeInd;
	}
	public void setDiscountTypeInd(Short discountTypeInd) {
		this.discountTypeInd = discountTypeInd;
	}
}
