package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

public enum BusinessModel {
	ONEB1U(1),
	ONEB3U(2),
	ONEBUU(3),
	PDL1(4),
	PDL7(5),
	PDL14(6),
	PDL28(7),
	CAM(14),
	SUBUU(21),
	SUBMU(22);

    private static final Map<Integer, BusinessModel> ENUMS_BY_ID = new HashMap<Integer, BusinessModel>();
	static {
		ENUMS_BY_ID.put(ONEB1U.getId(), ONEB1U);
		ENUMS_BY_ID.put(ONEB3U.getId(), ONEB3U);
		ENUMS_BY_ID.put(ONEBUU.getId(), ONEBUU);
		ENUMS_BY_ID.put(PDL1.getId(), PDL1);
		ENUMS_BY_ID.put(PDL7.getId(), PDL7);
		ENUMS_BY_ID.put(PDL14.getId(), PDL14);
		ENUMS_BY_ID.put(PDL28.getId(), PDL28);
		ENUMS_BY_ID.put(CAM.getId(), CAM);
		ENUMS_BY_ID.put(SUBUU.getId(), SUBUU);
		ENUMS_BY_ID.put(SUBMU.getId(), SUBMU);
	}
	
    private int id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    BusinessModel(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static BusinessModel getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }   
}
