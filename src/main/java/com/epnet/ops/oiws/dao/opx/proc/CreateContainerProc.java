package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class CreateContainerProc extends StoredProcedure {

	public static final String CREATE_CONTAINER_PROC_NAME = "prx_insert_Container";
	
	@Autowired(required=false)
	public CreateContainerProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, CREATE_CONTAINER_PROC_NAME);

		declareParameter(new SqlParameter(OpxConstants.CUST_ID_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_TYPE_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_STATUS_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_SOURCE_ID_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.OPPORTUNITY_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.SALES_REP_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.CURRENCY_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.LANGUAGE_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.CUSTOMER_CONTACT_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.PO_NUMBER_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.BILLING_ADDRESS_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.BILLING_CONTACT_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));
		
		compile();
	}
}