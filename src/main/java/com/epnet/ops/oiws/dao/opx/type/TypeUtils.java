package com.epnet.ops.oiws.dao.opx.type;

import org.apache.log4j.Logger;

import com.epnet.ops.oiws.service.impl.OIServiceImpl;

public class TypeUtils {
	private static Logger log = Logger.getLogger(OIServiceImpl.class);
	
	public static BusinessModel getBusinessModelForId(Integer modelId) {
		BusinessModel model = null;
		
	    if (modelId != null) {
	    	try {
	    		model = BusinessModel.getById(modelId);
	    	} catch (Exception ex) {
	    		log.warn("Invalid business model id: " + modelId);
	    	}
	    }
		
	    return model;
	}
	
	public static  Integer getBusinessModelIdFromCode(String code) {
		if (code == null) {
			return null;
		}

		Integer modelId = null;
		
    	try {
	    	modelId = BusinessModel.valueOf(code).getId();
    	} catch (Exception ex) {
    		log.warn("Invalid business model id: " + modelId);
    	}
		
	    return modelId;
	}
	
	public static  DocumentRequestType getDocumentRequestTypeFromCode(String code) {
		if (code == null) {
			return null;
		}

		DocumentRequestType docType = null;
		
    	try {
    		docType = DocumentRequestType.valueOf(code);
    	} catch (Exception ex) {
    		log.warn("Invalid document request type: " + code);
    	}
		
	    return docType;
	}
	
	public static  DocumentRequestStatus getDocumentRequestStatusFromCode(String code) {
		if (code == null) {
			return null;
		}

		DocumentRequestStatus docStatus = null;
		
    	try {
    		docStatus = DocumentRequestStatus.valueOf(code);
    	} catch (Exception ex) {
    		log.warn("Invalid document request type: " + code);
    	}
		
	    return docStatus;
	}
	
	public static ContainerStatus getContainerStatusFromCode(String code) {
		if (code == null) {
			return null;
		}

		ContainerStatus typ = null;
		
    	try {
    		typ = ContainerStatus.valueOf(code);
    	} catch (Exception ex) {
    		log.warn("Invalid document request type: " + code);
    	}
		
	    return typ;
	}
	
	public static ItemState getItemStateFromCode(String code) {
		if (code == null) {
			return null;
		}

		ItemState typ = null;
		
    	try {
    		typ = ItemState.valueOf(code);
    	} catch (Exception ex) {
    		log.warn("Invalid document request type: " + code);
    	}
		
	    return typ;
	}
	
	public static DiscountType getDiscountTypeFromCode(String code) {
		if (code == null) {
			return null;
		}

		DiscountType typ = null;
		
    	try {
    		typ = DiscountType.valueOf(code);
    	} catch (Exception ex) {
    		log.warn("Invalid document request type: " + code);
    	}
		
	    return typ;
	}

}
