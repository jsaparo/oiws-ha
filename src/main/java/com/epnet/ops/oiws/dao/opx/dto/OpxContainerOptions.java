package com.epnet.ops.oiws.dao.opx.dto;

import java.util.Date;

public class OpxContainerOptions {
	private Integer containerId;
	private String orderDescription;
	private String customerNote;
	private String termsAndConditions;
	private Date invoiceDisplayDate;
	private Boolean displaySubscriptionId;
	private Boolean displayPackagedItemPrices;
	private Boolean displayItemInterfaces;
	private Boolean displaySimultaneousUsers;
	private Integer quoteExpireDays;
	private Boolean showMarketingOnQuote;
	private Boolean displayItemDiscount;
	private Boolean displayAccessingSiteCount;
	private Boolean payByCreditCard;
	private Boolean displayPackageNames;
	private Boolean displayTaxId;
	private String taxLanguage;
	private String taxLabel;
	private Double taxPercent;
	private Boolean invoiceAsap;
	private Boolean rushOrder;
	private String quotedCurrency;
	private String invoiceNote;
	private String orderProcessingNote;
	private String purchaseOrderFormNote;
	private String communicationModeCode;
	private String groupCode;
	private String quoteAccessingSiteNote;
	private Double discountPrice;
	private Double discountAmount;
	private Short discountTypeInd;
	private Double grossPrice;
	
	public Integer getContainerId() {
		return containerId;
	}
	public void setContainerId(Integer containerId) {
		this.containerId = containerId;
	}
	public String getOrderDescription() {
		return orderDescription;
	}
	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}
	public String getCustomerNote() {
		return customerNote;
	}
	public void setCustomerNote(String customerNote) {
		this.customerNote = customerNote;
	}
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	public Date getInvoiceDisplayDate() {
		return invoiceDisplayDate;
	}
	public void setInvoiceDisplayDate(Date invoiceDisplayDate) {
		this.invoiceDisplayDate = invoiceDisplayDate;
	}
	public Boolean getDisplaySubscriptionId() {
		return displaySubscriptionId;
	}
	public void setDisplaySubscriptionId(Boolean displaySubscriptionId) {
		this.displaySubscriptionId = displaySubscriptionId;
	}
	public Boolean getDisplayPackagedItemPrices() {
		return displayPackagedItemPrices;
	}
	public void setDisplayPackagedItemPrices(Boolean displayPackagedItemPrices) {
		this.displayPackagedItemPrices = displayPackagedItemPrices;
	}
	public Boolean getDisplayItemInterfaces() {
		return displayItemInterfaces;
	}
	public void setDisplayItemInterfaces(Boolean displayItemInterfaces) {
		this.displayItemInterfaces = displayItemInterfaces;
	}
	public Boolean getDisplaySimultaneousUsers() {
		return displaySimultaneousUsers;
	}
	public void setDisplaySimultaneousUsers(Boolean displaySimultaneousUsers) {
		this.displaySimultaneousUsers = displaySimultaneousUsers;
	}
	public Integer getQuoteExpireDays() {
		return quoteExpireDays;
	}
	public void setQuoteExpireDays(Integer quoteExpireDays) {
		this.quoteExpireDays = quoteExpireDays;
	}
	public Boolean getShowMarketingOnQuote() {
		return showMarketingOnQuote;
	}
	public void setShowMarketingOnQuote(Boolean showMarketingOnQuote) {
		this.showMarketingOnQuote = showMarketingOnQuote;
	}
	public Boolean getDisplayItemDiscount() {
		return displayItemDiscount;
	}
	public void setDisplayItemDiscount(Boolean displayItemDiscount) {
		this.displayItemDiscount = displayItemDiscount;
	}
	public Boolean getDisplayAccessingSiteCount() {
		return displayAccessingSiteCount;
	}
	public void setDisplayAccessingSiteCount(Boolean displayAccessingSiteCount) {
		this.displayAccessingSiteCount = displayAccessingSiteCount;
	}
	public Boolean getPayByCreditCard() {
		return payByCreditCard;
	}
	public void setPayByCreditCard(Boolean payByCreditCard) {
		this.payByCreditCard = payByCreditCard;
	}
	public Boolean getDisplayPackageNames() {
		return displayPackageNames;
	}
	public void setDisplayPackageNames(Boolean displayPackageNames) {
		this.displayPackageNames = displayPackageNames;
	}
	public Boolean getDisplayTaxId() {
		return displayTaxId;
	}
	public void setDisplayTaxId(Boolean displayTaxId) {
		this.displayTaxId = displayTaxId;
	}
	public String getTaxLanguage() {
		return taxLanguage;
	}
	public void setTaxLanguage(String taxLanguage) {
		this.taxLanguage = taxLanguage;
	}
	public String getTaxLabel() {
		return taxLabel;
	}
	public void setTaxLabel(String taxLabel) {
		this.taxLabel = taxLabel;
	}
	public Double getTaxPercent() {
		return taxPercent;
	}
	public void setTaxPercent(Double taxPercent) {
		this.taxPercent = taxPercent;
	}
	public Boolean getInvoiceAsap() {
		return invoiceAsap;
	}
	public void setInvoiceAsap(Boolean invoiceAsap) {
		this.invoiceAsap = invoiceAsap;
	}
	public Boolean getRushOrder() {
		return rushOrder;
	}
	public void setRushOrder(Boolean rushOrder) {
		this.rushOrder = rushOrder;
	}
	public String getQuotedCurrency() {
		return quotedCurrency;
	}
	public void setQuotedCurrency(String quotedCurrency) {
		this.quotedCurrency = quotedCurrency;
	}
	public String getInvoiceNote() {
		return invoiceNote;
	}
	public void setInvoiceNote(String invoiceNote) {
		this.invoiceNote = invoiceNote;
	}
	public String getOrderProcessingNote() {
		return orderProcessingNote;
	}
	public void setOrderProcessingNote(String orderProcessingNote) {
		this.orderProcessingNote = orderProcessingNote;
	}
	public String getPurchaseOrderFormNote() {
		return purchaseOrderFormNote;
	}
	public void setPurchaseOrderFormNote(String purchaseOrderFormNote) {
		this.purchaseOrderFormNote = purchaseOrderFormNote;
	}
	public String getCommunicationModeCode() {
		return communicationModeCode;
	}
	public void setCommunicationModeCode(String communicationModeCode) {
		this.communicationModeCode = communicationModeCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getQuoteAccessingSiteNote() {
		return quoteAccessingSiteNote;
	}
	public void setQuoteAccessingSiteNote(String quoteAccessingSiteNote) {
		this.quoteAccessingSiteNote = quoteAccessingSiteNote;
	}
	public Double getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Short getDiscountTypeInd() {
		return discountTypeInd;
	}
	public void setDiscountTypeInd(Short discountTypeInd) {
		this.discountTypeInd = discountTypeInd;
	}
	public Double getGrossPrice() {
		return grossPrice;
	}
	public void setGrossPrice(Double grossPrice) {
		this.grossPrice = grossPrice;
	}
}
