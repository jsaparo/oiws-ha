package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class GetDocumentRequestProc extends StoredProcedure {

	public static final String GET_DOCUMENT_REQUEST_PROC_NAME = "prx_get_DocumentRequest_by_pk";
	
	@Autowired(required=false)
	public GetDocumentRequestProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, GET_DOCUMENT_REQUEST_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.DOCUMENT_REQUEST_ID_PARAM, Types.INTEGER));
		
		compile();
	}
}
