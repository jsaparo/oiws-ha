package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class UpdateContainerOptionsProc extends StoredProcedure {

	public static final String UPDATE_CONTAINER_OPTIONS_PROC_NAME = "prx_update_ContainerOption_by_pk";
	
	@Autowired(required=false)
	public UpdateContainerOptionsProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, UPDATE_CONTAINER_OPTIONS_PROC_NAME);

		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.ORDER_DESCRIPTION_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.CUSTOMER_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.TERMS_AND_CONDITIONS_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.INVOICE_DISPLAY_DATE_PARAM, Types.DATE));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_SUBSCRIPTION_ID_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_PACKAGED_ITEM_PRICES_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_ITEM_INTERFACES_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_SIMULTANEOUS_USERS_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.QUOTE_EXPIRE_DAYS_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.SHOW_MARKETING_ON_QUOTE_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_ITEM_DISCOUNT_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_ACCESSING_SITE_COUNT_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.PAY_BY_CREDIT_CART_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_PACKAGE_NAMES_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.DISPLAY_TAX_ID_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.TAX_LANGUAGE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.TAX_LABEL_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.TAX_PERCENT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.INVOICE_ASAP_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.RUSH_ORDER_PARAM, Types.BOOLEAN));
		declareParameter(new SqlParameter(OpxConstants.QUOTED_CURRENCY_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.INVOICE_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.ORDER_PROCESSING_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.PURCHASE_ORDER_FORM_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.COMMUNICATION_MODE_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.GROUP_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.QUOTE_ACCESSING_SITE_NOTE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_TYPE_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_PRICE_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.DISCOUNT_AMOUNT_PARAM, Types.DOUBLE));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));

		compile();
	}
}