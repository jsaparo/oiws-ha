package com.epnet.ops.oiws.dao.opx;

/**
 * Checked exception for Opx interaction.
 * 
 * @author jsaparo
 *
 */
public class OpxException extends Exception {

	/** Generated */
	private static final long serialVersionUID = 5177176076775874043L;
	
	public OpxException(String msg) {
		super(msg);
	}

	public OpxException(String msg, Throwable e) {
		super(msg, e);
	}	
}
