package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

public enum ItemState {
	WIP(1),
	PROCESSED(2),
	SUBMITTED(3),
	APPROVAL(4),
	ORDERENTRY(5),
	PENDING(6),
	RECOGNIZED(7),
	FAILED(8),
	REJECTED(9);
	
    private static final Map<Integer, ItemState> ENUMS_BY_ID = new HashMap<Integer, ItemState>();
	static {
		ENUMS_BY_ID.put(WIP.getId(), WIP);
		ENUMS_BY_ID.put(PROCESSED.getId(), PROCESSED);
		ENUMS_BY_ID.put(SUBMITTED.getId(), SUBMITTED);
		ENUMS_BY_ID.put(APPROVAL.getId(), APPROVAL);
		ENUMS_BY_ID.put(ORDERENTRY.getId(), ORDERENTRY);
		ENUMS_BY_ID.put(PENDING.getId(), PENDING);
		ENUMS_BY_ID.put(RECOGNIZED.getId(), RECOGNIZED);
		ENUMS_BY_ID.put(FAILED.getId(), FAILED);
		ENUMS_BY_ID.put(REJECTED.getId(), REJECTED);
	}
	
    private int id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    ItemState(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static ItemState getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }   
}
