package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

public enum DiscountType {
	PERCENT(1),
	FIXED(2);
	
    private static final Map<Integer, DiscountType> ENUMS_BY_ID = new HashMap<Integer, DiscountType>();
	static {
		ENUMS_BY_ID.put(PERCENT.getId(), PERCENT);
		ENUMS_BY_ID.put(FIXED.getId(), FIXED);
	}
	
    private int id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    DiscountType(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static DiscountType getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }   
}

