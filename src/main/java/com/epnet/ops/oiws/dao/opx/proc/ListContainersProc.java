package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class ListContainersProc extends StoredProcedure {
	public static final String LIST_CONTAINERS_PROC_NAME = "prx_get_Container_by_Customer_Employee_Type";
	
	@Autowired(required=false)
	public ListContainersProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, LIST_CONTAINERS_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.CUST_ID_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.SALES_REP_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_TYPE_ID_PARAM, Types.INTEGER));
		
		compile();
	}
}
