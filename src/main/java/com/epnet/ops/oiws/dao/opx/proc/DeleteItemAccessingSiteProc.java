package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class DeleteItemAccessingSiteProc extends StoredProcedure {
	
	public static final String CREATE_ITEM_ACCESSING_SITE_PROC_NAME = "prx_insert_ContainerItemAccessingSite";

	@Autowired(required=false)
	public DeleteItemAccessingSiteProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, CREATE_ITEM_ACCESSING_SITE_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.ITEM_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.CUST_ID_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.INTERFACE_IDENTIFIERS_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));
		
		compile();
	}	
}