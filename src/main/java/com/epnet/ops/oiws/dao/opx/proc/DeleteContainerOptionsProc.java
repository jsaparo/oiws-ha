package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class DeleteContainerOptionsProc extends StoredProcedure {

	public static final String DELETE_CONTAINER_OPTIONS_PROC_NAME = "prx_delete_ContainerOption_by_pk";
	
	@Autowired(required=false)
	public DeleteContainerOptionsProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, DELETE_CONTAINER_OPTIONS_PROC_NAME);

		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));

		compile();
	}
}