package com.epnet.ops.oiws.dao.opx;

import java.util.List;
import java.util.Map;

import com.epnet.ops.oiws.dao.opx.dto.OpxContainerInstallments;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerItemAccessingSite;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerAssociation;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerOptions;
import com.epnet.ops.oiws.dao.opx.dto.OpxDocumentRequest;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainer;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerItem;
import com.epnet.ops.oiws.dao.opx.dto.OpxItem;
import com.epnet.ops.oiws.dao.opx.type.ContainerStatus;
import com.epnet.ops.oiws.dao.opx.type.ContainerType;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestStatus;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestType;
import com.epnet.ops.oiws.dao.opx.type.ItemState;

/**
 * Defines public methods for interacting directly with the database.
 * 
 * @author jsaparo
 *
 */
public interface IOpxDataService {
	
	/**
	 * Create a new container object in Opx
	 *  
	 * @param container The container info to save
	 * @return An identifier for newly created container record
	 * @throws OpxException
	 */
	int createContainer(OpxContainer container) throws OpxException;
	
	/**
	 * Update an existing container record in Opx.
	 * 
	 * @param container The container info to update
	 * @throws OpxException
	 */
	void updateContainer(OpxContainer container) throws OpxException;

	/**
	 * Create a new container item
	 * 
	 * @param containerItem
	 * @return The id of the newly created container item
	 * @throws OpxException
	 */
	int createContainerItem(OpxContainerItem containerItem) throws OpxException;

	/**
	 * Create a new, fee-standing item.
	 * 
	 * @param item The item to create
	 * @return The id of the newly created item
	 * @throws OpxException
	 */
	int createItem(OpxItem item) throws OpxException;

	/**
	 * Update a container item.
	 * 
	 * @param containerItem the item to update
	 * @throws OpxException
	 */
	void updateContainerItem(OpxContainerItem containerItem) throws OpxException;
	
	/**
	 * Update an existing, free-standing item.
	 * 
	 * @param item
	 * @throws OpxException
	 */
	void updateItem(OpxItem item) throws OpxException;
	
	/**
	 * Fetch a container record from Ops.
	 * 
	 * @param containerId The id of the container to fetch.
	 * @param headerOnly Indicates whether to skip retrieval of container items
	 * @return A container object, or null of no such container exists
	 * @throws OpxException
	 */
	OpxContainer getContainer(int containerId, boolean headerOnly) throws OpxException;

	/**
	 * Delete a container from the database
	 * 
	 * @param containerId
	 */
	void deleteContainer(int containerId) throws OpxException;
	
	/**
	 * Fetch containers for a specified customer, container typ, and sales rep.
	 * 
	 * @param customerId The customer id to search for
	 * @param containerType The container type to search for
	 * @param salesRepId The sales rep to search for
	 * @return A collection of matching containers, or null if none match
	 * @throws OpxException
	 */
	List<OpxContainer> listContainers(String customerId, ContainerType containerType, Integer salesRepId) throws OpxException;	
	
	/**
	 * Fetch all items linked to a container.
	 * 
	 * @param containerId The id of the container to fetch items for
	 * @return A list of container item links, or empty list if no items exist for the container
	 * @throws OpxException
	 */
	List<OpxContainerItem> getContainerItems(int containerId) throws OpxException;	
	
	/**
	 * Retrieve a single container/item link record.
	 * 
	 * @param containerItemId The id of the container item link within the container to retrieve
	 * @return A container item link, or null if not found
	 * @throws OpxException
	 */
	OpxContainerItem getContainerItem(int containerItemId) throws OpxException;
	
	/**
	 * Retrieve a single item record, detached from any container.
	 * 
	 * @param itemId The id of the item to retrieve
	 * @return An item, or null if not found
	 * @throws OpxException
	 */
	OpxItem getItem(int itemId) throws OpxException;
	
	/**
	 * Retrieve a container's item links mapped by item id.
	 * 
	 * @param containerId
	 * @return A map of container item links
	 * @throws OpxException
	 */
	Map<Integer, OpxContainerItem> getContainerItemMap(int containerId) throws OpxException;
	
	/**
	 * Remove an item from a container
	 * 
	 * @param containerId Id of container to remove item from
	 * @param itemId Id of item to remove from container
	 */
	void removeItemFromContainer(int containerId, int itemId) throws OpxException;
	
	/**
	 * Set the status of a specified container.
	 * 
	 * @param containerId The id of the container to set status for
	 * @param status The status value
	 */
	void updateContainerStatus(int containerId, ContainerStatus status) throws OpxException;
	
	/**
	 * Set the state of a specified item.
	 * 
	 * @param itemId The id of the item to set state for
	 * @param state The state value
	 */
	void setContainerState(int orderId, ContainerStatus orderStatus, ItemState itemState) throws OpxException;
	
	/**
	 * Set the state of a specified item.
	 * 
	 * @param itemId The id of the item to set state for
	 * @param state The state value
	 */
	void setItemState(int itemId, ItemState state) throws OpxException;
	
	/**
	 * Gets the first container with a specified status and type.
	 * 
	 * @param status The status to search for
	 * @param type The container type to search for
	 * @return A matching container object, or null if none found
	 */
	OpxContainer getFirstContainersByStatusAndType(ContainerStatus status, ContainerType type) throws OpxException;
	
	/**
	 * Create a new document request record.
	 * 
	 * @param containerId The id of the container to which the request applies.
	 * @param docType The type of document being requested 
	 * @param baseFileName The base file name of the document
	 * @param languageCode A language code to be fed to the document generation process, or null for ENG.
	 * @return The id of the newly created document request record
	 */
	Integer createDocumentGenerationRequest(
		int containerId, DocumentRequestType docType, String baseFileName, String languageCode) throws OpxException;
	
	/**
	 * Gets the id of the next unprocessed document request.
	 * 
	 * @return A document request identifier, or null if not found
	 */
	Integer getNextUnprocessedDocumentRequestId() throws OpxException;
	
	/**
	 * Set the statis and final file location for a specified document request.
	 * 
	 * @param docRequestId The id of the document generation request
	 * @param status The status of the request
	 * @param fileLocation The full path/url to the generated document
	 */
	void setDocumentGenerationRequestStatus(
		int docRequestId, DocumentRequestStatus status, String fileLocation) throws OpxException;
	
	/**
	 * Fetch document request generation detail for a specified documeng generation request id.
	 * 
	 * @param docRequestId The id of the request to fetch
	 * @return A request detail object, or null if not found
	 */
	OpxDocumentRequest getDocumentGenerationRequest(int docRequestId) throws OpxException;
	
	/**
	 * Associate one container with another as a child.
	 * 
	 * @param parentContainerId The id of the existing parent container id
	 * @param childContainerId The id of the existing child conatiner id
	 * @return The id of the newly created association record
	 */
	Integer createContainerAssociation(int parentContainerId, int childContainerId) throws OpxException;

	/**
	 * Delete a container association by association id.
	 * 
	 * @param containerAssociationId The id of the association to delete
	 */
	void deleteContainerAssociation(int containerAssociationId) throws OpxException;
	
	/**
	 * Get container association detail.
	 * @param containerAssociationId The id of the container association to get details for
	 * @return A container association object, or null if not found
	 */
	OpxContainerAssociation getContainerAssociation(int containerAssociationId) throws OpxException;
	
	/**
	 * Get a list of container associations for a given container.
	 * @param containerId The id of the container to get associations for
	 * @return A list of associations, or null/empty list if not found
	 */
	List<OpxContainerAssociation> getContainerAssociations(int containerId) throws OpxException;
	
	/**
	 * Associate accessing site data with a container item.
	 * 
	 * @param opxAccessingSite Object representing the association to create
	 * @throws OpxException
	 */
	Integer createContainerItemAccessingSite(OpxContainerItemAccessingSite opxAccessingSite) throws OpxException;
	
	/**
	 * Remove accessing site information from a container item.
	 * 
	 * @param containerItemAccessingSiteId The id of the accessing site data to remove.
	 * @throws OpxException
	 */
	void deleteContainerItemAccessingSite(int containerItemAccessingSiteId) throws OpxException;
	
	/**
	 * Update the interface identifiers for a given item accessing site.
	 * 
	 * @param containerItemAccessingSiteId The id of the accessing site data to update.
	 * @param interfaceIds A comma-delimited list of interface identifiers
	 * @throws OpxException
	 */
	void updateContainerItemAccessingSite(int containerItemAccessingSiteId, String interfaceIds) throws OpxException;
	
	/**
	 * Fetch a list of item accessing site object for a given container id/item id combination
	 * 
	 * @param containerId The id of the container
	 * @param itemId The if of the item in the container
	 * @return A list of item accessing site object, or null/empty list if not available
	 * 
	 * @throws OpxException
	 */
	List<OpxContainerItemAccessingSite> getContainerItemAccessingSites(int containerId, int itemId) throws OpxException;

	/**
	 * Create a container options record
	 * 
	 * @param opxContainerOptions The options record to create
	 * @return The id of the container associated with the newly created options
	 * @throws OpxException
	 */
	Integer createContainerOptions(OpxContainerOptions opxContainerOptions) throws OpxException;
	
	/**
	 * Fetch the options record for a container.
	 * 
	 * @param containerId The id of the container to fetch the options for.
	 * @return An options object, or null if not found
	 * @throws OpxException
	 */
	OpxContainerOptions getContainerOptions(int containerId) throws OpxException;
	
	/**
	 * Delete a container options record.
	 * @param containerId The id of the container to remove the options record for 
	 * @throws OpxException
	 */
	void deleteContainerOptions(int containerId) throws OpxException;
	
	/**
	 * Update an existing container options record.
	 * 
	 * @param opxContainerOptions The container options data to save
	 * @throws OpxException
	 */
	void updateContainerOptions(OpxContainerOptions opxContainerOptions) throws OpxException;
	
	/**
	 * Create a container installments record
	 * 
	 * @param opxContainerInstallments The installments record to create
	 * @return The id of the container associated with the newly created installments
	 * @throws OpxException
	 */
	Integer createContainerInstallments(OpxContainerInstallments opxContainerInstallments) throws OpxException;
	
	/**
	 * Fetch the installments record for a container.
	 * 
	 * @param containerId The id of the container to fetch the installments for.
	 * @return An installments object, or null if not found
	 * @throws OpxException
	 */
	OpxContainerInstallments getContainerInstallments(int containerId) throws OpxException;
	
	/**
	 * Delete a container installments record.
	 * @param containerId The id of the container to remove the installments record for 
	 * @throws OpxException
	 */
	void deleteContainerInstallments(int containerId) throws OpxException;
	
	/**
	 * Update an existing container installments record.
	 * 
	 * @param opxContainerInstallments The container installments data to save
	 * @throws OpxException
	 */
	void updateContainerInstallments(OpxContainerInstallments opxContainerInstallments) throws OpxException;

}
