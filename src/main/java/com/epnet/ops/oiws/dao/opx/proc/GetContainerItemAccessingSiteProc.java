package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class GetContainerItemAccessingSiteProc extends StoredProcedure {

	public static final String GET_CONTAINER_ITEM_ACCESSING_SITE_PROC_NAME = "prx_get_ContainerItemAccessingSite_by_pk";
	
	@Autowired(required=false)
	public GetContainerItemAccessingSiteProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, GET_CONTAINER_ITEM_ACCESSING_SITE_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ITEM_ACCESSING_SITE_ID_PARAM, Types.INTEGER));
		
		compile();
	}
}

