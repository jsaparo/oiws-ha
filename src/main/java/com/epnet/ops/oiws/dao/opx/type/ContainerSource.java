package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author jsaparo
 *
 */
public enum ContainerSource {
	WSR(1),
	ECM(2);

    private static final Map<Integer, ContainerSource> ENUMS_BY_ID = new HashMap<Integer, ContainerSource>();
	static {
		ENUMS_BY_ID.put(WSR.getId(), WSR);
		ENUMS_BY_ID.put(ECM.getId(), ECM);
	}
	
    private int id;   

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    ContainerSource(int id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public int getId() {
        return id;
    }
    
    public static ContainerSource getById(int id) {
    	return ENUMS_BY_ID.get(id);
    }
}
