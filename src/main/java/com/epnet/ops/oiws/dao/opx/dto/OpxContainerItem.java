package com.epnet.ops.oiws.dao.opx.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Simple object for reading/writing data in the OPX ContainerItem table.
 * 
 * @author jsaparo
 *
 */public class OpxContainerItem {
	private Integer containerItemId;
	private Integer containerId;
	private Integer itemId;
	private OpxContainer container;
	private OpxItem item;
	private Integer sequence;
	List<OpxContainerItemAccessingSite> accessingSites = new ArrayList<OpxContainerItemAccessingSite>();
	private Double discountPrice;
	private Double discountAmount;
	private Short discountTypeInd;
	private Double grossPrice;
	private Date startDate;
	private Date endDate;
	private String groupCode;
	private String trialInterfaces;
	private Boolean active;
	private Integer termInMonths;
	
	public Integer getContainerItemId() {
		return containerItemId;
	}
	
	public void setContainerItemId(Integer containerItemId) {
		this.containerItemId = containerItemId;
	}
	
	public Integer getContainerId() {
		return containerId;
	}

	public void setContainerId(Integer containerId) {
		this.containerId = containerId;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public OpxContainer getContainer() {
		return container;
	}
	
	public void setContainer(OpxContainer container) {
		this.container = container;
	}
	
	public OpxItem getItem() {
		return item;
	}
	
	public void setItem(OpxItem item) {
		this.item = item;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public List<OpxContainerItemAccessingSite> getAccessingSites() {
		return accessingSites;
	}

	public void setAccessingSites(List<OpxContainerItemAccessingSite> accessingSites) {
		this.accessingSites = accessingSites;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Short getDiscountTypeInd() {
		return discountTypeInd;
	}

	public void setDiscountTypeInd(Short discountTypeInd) {
		this.discountTypeInd = discountTypeInd;
	}

	public Double getGrossPrice() {
		return grossPrice;
	}

	public void setGrossPrice(Double grossPrice) {
		this.grossPrice = grossPrice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getTrialInterfaces() {
		return trialInterfaces;
	}

	public void setTrialInterfaces(String trialInterfaces) {
		this.trialInterfaces = trialInterfaces;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(Integer termInMonths) {
		this.termInMonths = termInMonths;
	}
}
