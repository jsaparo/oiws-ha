package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class CreateDocumentRequestProc extends StoredProcedure {

	public static final String CREATE_DOC_REQUEST_PROC_NAME = "prx_insert_DocumentRequest";
	
	@Autowired(required=false)
	public CreateDocumentRequestProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, CREATE_DOC_REQUEST_PROC_NAME);

		declareParameter(new SqlParameter(OpxConstants.CONTAINER_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.DOCUMENT_REQUEST_TYPE_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.BASE_FILENAME_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.LANGUAGE_CODE_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));
		
		compile();
	}

}
