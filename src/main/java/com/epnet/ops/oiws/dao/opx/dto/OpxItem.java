package com.epnet.ops.oiws.dao.opx.dto;

import java.util.Date;

/**
 * Simple pojo for reading/writing data in the OPX Item table.
 * @author jsaparo
 *
 */
public class OpxItem {
	private Integer itemId;
	private Integer productId;
	private Date startDate;
	private Date endDate;
	private Integer termInMonths;
	private Double grossPrice;
	private Double discountPrice;
	private Double discountAmount;
	private Short discountTypeInd;	
	private Double premiumPricingDiscount;
	private Double primaryAgencyDiscount;
	private Boolean complimentary;
	private Integer quantity;
	private Integer businessModelId;
	private Double royaltyAmount;
	private String royaltyComment;
	private Boolean renewal;
	private String renewalNote;
	private String contractNumber;
	private Short itemStateInd;
	private Double probability;
	private Integer renewalSubscriptionId;
	private Date modifiedDate;
	private Date ExpectedCloseDate;
	private Integer TidIdentifier;
	private Double ListRate;
	private Integer MaxSimultaneousUsers;
	private String PurchaseOrderNumber;
	private Date InvoiceStartDate;
	private Integer MultiYearPayments;
	private Double AmountYear1;
	private Double AmountYear2;
	private Double AmountYear3;
	private Double AmountYear4;
	private Double AmountYear5;
	private Double AmountYear6;
	private Double AmountYear7;
	private Double AmountYear8;
	private Double AmountYear9;
	private Double AmountYear10;
	private String UpgradeDowngradeFlag;
	private Integer EhisPurchaseCount;
	private Integer EhisFreeCount;
	private Boolean BooksConsortia;
	private Boolean BooksShared;
	private Double PrevListRate;
	private Integer PrevTerm;
	private Double PrevRoyaltyAmount;
	private String PrevRoyaltyComment;
	private String StatusTag;
	private Integer ItemFTE;
	private String ItemNote;
	private String ControllingCustomerIndicator;
	private String groupCode;
	private String trialInterfaces;
	private Boolean active;
	private String lostReason;
	private String lostNotes;
	private Integer pricedThroughConsortiumId;

	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getTermInMonths() {
		return termInMonths;
	}
	public void setTermInMonths(Integer termInMonths) {
		this.termInMonths = termInMonths;
	}
	public Double getGrossPrice() {
		return grossPrice;
	}
	public void setGrossPrice(Double grossPrice) {
		this.grossPrice = grossPrice;
	}
	public Double getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Short getDiscountTypeInd() {
		return discountTypeInd;
	}
	public void setDiscountTypeInd(Short discountTypeInd) {
		this.discountTypeInd = discountTypeInd;
	}
	public Double getPremiumPricingDiscount() {
		return premiumPricingDiscount;
	}
	public void setPremiumPricingDiscount(Double premiumPricingDiscount) {
		this.premiumPricingDiscount = premiumPricingDiscount;
	}
	public Double getPrimaryAgencyDiscount() {
		return primaryAgencyDiscount;
	}
	public void setPrimaryAgencyDiscount(Double primaryAgencyDiscount) {
		this.primaryAgencyDiscount = primaryAgencyDiscount;
	}
	public Boolean getComplimentary() {
		return complimentary;
	}
	public void setComplimentary(Boolean complimentary) {
		this.complimentary = complimentary;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getBusinessModelId() {
		return businessModelId;
	}
	public void setBusinessModelId(Integer businessModelId) {
		this.businessModelId = businessModelId;
	}
	public Double getRoyaltyAmount() {
		return royaltyAmount;
	}
	public void setRoyaltyAmount(Double royaltyAmount) {
		this.royaltyAmount = royaltyAmount;
	}
	public String getRoyaltyComment() {
		return royaltyComment;
	}
	public void setRoyaltyComment(String royaltyComment) {
		this.royaltyComment = royaltyComment;
	}
	public Boolean getRenewal() {
		return renewal;
	}
	public void setRenewal(Boolean renewal) {
		this.renewal = renewal;
	}
	public String getRenewalNote() {
		return renewalNote;
	}
	public void setRenewalNote(String renewalNote) {
		this.renewalNote = renewalNote;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public Short getItemStateInd() {
		return itemStateInd;
	}
	public void setItemStateInd(Short itemStateInd) {
		this.itemStateInd = itemStateInd;
	}
	public Double getProbability() {
		return probability;
	}
	public void setProbability(Double probability) {
		this.probability = probability;
	}
	public Integer getRenewalSubscriptionId() {
		return renewalSubscriptionId;
	}
	public void setRenewalSubscriptionId(Integer renewalSubscriptionId) {
		this.renewalSubscriptionId = renewalSubscriptionId;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Date getExpectedCloseDate() {
		return ExpectedCloseDate;
	}
	public void setExpectedCloseDate(Date expectedCloseDate) {
		ExpectedCloseDate = expectedCloseDate;
	}
	public Integer getTidIdentifier() {
		return TidIdentifier;
	}
	public void setTidIdentifier(Integer tidIdentifier) {
		TidIdentifier = tidIdentifier;
	}
	public Double getListRate() {
		return ListRate;
	}
	public void setListRate(Double listRate) {
		ListRate = listRate;
	}
	public Integer getMaxSimultaneousUsers() {
		return MaxSimultaneousUsers;
	}
	public void setMaxSimultaneousUsers(Integer maxSimultaneousUsers) {
		MaxSimultaneousUsers = maxSimultaneousUsers;
	}
	public String getPurchaseOrderNumber() {
		return PurchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		PurchaseOrderNumber = purchaseOrderNumber;
	}
	public Date getInvoiceStartDate() {
		return InvoiceStartDate;
	}
	public void setInvoiceStartDate(Date invoiceStartDate) {
		InvoiceStartDate = invoiceStartDate;
	}
	public Integer getMultiYearPayments() {
		return MultiYearPayments;
	}
	public void setMultiYearPayments(Integer multiYearPayments) {
		MultiYearPayments = multiYearPayments;
	}
	public Double getAmountYear1() {
		return AmountYear1;
	}
	public void setAmountYear1(Double amountYear1) {
		AmountYear1 = amountYear1;
	}
	public Double getAmountYear2() {
		return AmountYear2;
	}
	public void setAmountYear2(Double amountYear2) {
		AmountYear2 = amountYear2;
	}
	public Double getAmountYear3() {
		return AmountYear3;
	}
	public void setAmountYear3(Double amountYear3) {
		AmountYear3 = amountYear3;
	}
	public Double getAmountYear4() {
		return AmountYear4;
	}
	public void setAmountYear4(Double amountYear4) {
		AmountYear4 = amountYear4;
	}
	public Double getAmountYear5() {
		return AmountYear5;
	}
	public void setAmountYear5(Double amountYear5) {
		AmountYear5 = amountYear5;
	}
	public Double getAmountYear6() {
		return AmountYear6;
	}
	public void setAmountYear6(Double amountYear6) {
		AmountYear6 = amountYear6;
	}
	public Double getAmountYear7() {
		return AmountYear7;
	}
	public void setAmountYear7(Double amountYear7) {
		AmountYear7 = amountYear7;
	}
	public Double getAmountYear8() {
		return AmountYear8;
	}
	public void setAmountYear8(Double amountYear8) {
		AmountYear8 = amountYear8;
	}
	public Double getAmountYear9() {
		return AmountYear9;
	}
	public void setAmountYear9(Double amountYear9) {
		AmountYear9 = amountYear9;
	}
	public Double getAmountYear10() {
		return AmountYear10;
	}
	public void setAmountYear10(Double amountYear10) {
		AmountYear10 = amountYear10;
	}
	public String getUpgradeDowngradeFlag() {
		return UpgradeDowngradeFlag;
	}
	public void setUpgradeDowngradeFlag(String upgradeDowngradeFlag) {
		UpgradeDowngradeFlag = upgradeDowngradeFlag;
	}
	public Integer getEhisPurchaseCount() {
		return EhisPurchaseCount;
	}
	public void setEhisPurchaseCount(Integer ehisPurchaseCount) {
		EhisPurchaseCount = ehisPurchaseCount;
	}
	public Integer getEhisFreeCount() {
		return EhisFreeCount;
	}
	public void setEhisFreeCount(Integer ehisFreeCount) {
		EhisFreeCount = ehisFreeCount;
	}
	public Boolean getBooksConsortia() {
		return BooksConsortia;
	}
	public void setBooksConsortia(Boolean booksConsortia) {
		BooksConsortia = booksConsortia;
	}
	public Boolean getBooksShared() {
		return BooksShared;
	}
	public void setBooksShared(Boolean booksShared) {
		BooksShared = booksShared;
	}
	public Double getPrevListRate() {
		return PrevListRate;
	}
	public void setPrevListRate(Double prevListRate) {
		PrevListRate = prevListRate;
	}
	public Integer getPrevTerm() {
		return PrevTerm;
	}
	public void setPrevTerm(Integer prevTerm) {
		PrevTerm = prevTerm;
	}
	public Double getPrevRoyaltyAmount() {
		return PrevRoyaltyAmount;
	}
	public void setPrevRoyaltyAmount(Double prevRoyaltyAmount) {
		PrevRoyaltyAmount = prevRoyaltyAmount;
	}
	public String getPrevRoyaltyComment() {
		return PrevRoyaltyComment;
	}
	public void setPrevRoyaltyComment(String prevRoyaltyComment) {
		PrevRoyaltyComment = prevRoyaltyComment;
	}
	public String getStatusTag() {
		return StatusTag;
	}
	public void setStatusTag(String statusTag) {
		StatusTag = statusTag;
	}
	public Integer getItemFTE() {
		return ItemFTE;
	}
	public void setItemFTE(Integer itemFTE) {
		ItemFTE = itemFTE;
	}
	public String getItemNote() {
		return ItemNote;
	}
	public void setItemNote(String itemNote) {
		ItemNote = itemNote;
	}
	public String getControllingCustomerIndicator() {
		return ControllingCustomerIndicator;
	}
	public void setControllingCustomerIndicator(String controllingCustomerIndicator) {
		ControllingCustomerIndicator = controllingCustomerIndicator;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getTrialInterfaces() {
		return trialInterfaces;
	}
	public void setTrialInterfaces(String trialInterfaces) {
		this.trialInterfaces = trialInterfaces;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getLostReason() {
		return lostReason;
	}
	public void setLostReason(String lostReason) {
		this.lostReason = lostReason;
	}
	public String getLostNotes() {
		return lostNotes;
	}
	public void setLostNotes(String lostNotes) {
		this.lostNotes = lostNotes;
	}
	public Integer getPricedThroughConsortiumId() {
		return pricedThroughConsortiumId;
	}
	public void setPricedThroughConsortiumId(Integer pricedThroughConsortiumId) {
		this.pricedThroughConsortiumId = pricedThroughConsortiumId;
	}
}
