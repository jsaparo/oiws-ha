package com.epnet.ops.oiws.dao.opx.proc;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import com.epnet.ops.oiws.dao.opx.OpxConstants;

@Component
public class UpdateDocumentRequestStatusProc extends StoredProcedure {
	public static final String UPDATE_DOC_REQUEST_STATUS_PROC_NAME = "prx_update_DocumentRequest_status_by_pk";
	
	@Autowired(required=false)
	public UpdateDocumentRequestStatusProc(@Qualifier("opxTemplate") JdbcTemplate opxTemplate) {
		super(opxTemplate, UPDATE_DOC_REQUEST_STATUS_PROC_NAME);
		
		declareParameter(new SqlParameter(OpxConstants.DOCUMENT_REQUEST_ID_PARAM, Types.INTEGER));
		declareParameter(new SqlParameter(OpxConstants.DOCUMENT_REQUEST_STATUS_ID_PARAM, Types.TINYINT));
		declareParameter(new SqlParameter(OpxConstants.FILE_LOCATION_PARAM, Types.VARCHAR));
		declareParameter(new SqlParameter(OpxConstants.SUPPRESS_RESULTS_PARAM, Types.BIT));

		compile();
	}
}