package com.epnet.ops.oiws.dao.opx.type;

import java.util.HashMap;
import java.util.Map;

public enum DocumentRequestType {
	WNSR_POF("POF"),
	WNSR_QUOTE("QUOTE");

    private static final Map<String, DocumentRequestType> ENUMS_BY_ID = new HashMap<String, DocumentRequestType>();
	static {
		ENUMS_BY_ID.put(WNSR_POF.getId(), WNSR_POF);
		ENUMS_BY_ID.put(WNSR_QUOTE.getId(), WNSR_QUOTE);
	}
	
    private String id;

    /**
     * Constructor.
     * 
     * @param id Assigned universal ID
      */
    DocumentRequestType(String id) {
        this.id = id;
    }

    /**
     * Get assigned universal ID.
     * 
     * @return A number
     */
    public String getId() {
        return id;
    }
    
    public static DocumentRequestType getById(String id) {
    	return ENUMS_BY_ID.get(id);
    }
}
