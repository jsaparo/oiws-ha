package com.epnet.ops.oiws.dao.opx.dto;

import java.util.Date;

public class OpxContainerInstallments {
	private Integer containerId;
	private Integer installmentCount;
	private Date installmentInvoiceDate1;
	private Double installmentAmount1;
	private Date installmentInvoiceDate2;
	private Double installmentAmount2;
	private Date installmentInvoiceDate3;
	private Double installmentAmount3;
	private Date installmentInvoiceDate4;
	private Double installmentAmount4;
	private Date installmentInvoiceDate5;
	private Double installmentAmount5;
	private Date installmentInvoiceDate6;
	private Double installmentAmount6;
	private Date installmentInvoiceDate7;
	private Double installmentAmount7;
	private Date installmentInvoiceDate8;
	private Double installmentAmount8;
	private Date installmentInvoiceDate9;
	private Double installmentAmount9;
	private Date installmentInvoiceDate10;
	private Double installmentAmount10;
	private Date installmentInvoiceDate11;
	private Double installmentAmount11;
	private Date installmentInvoiceDate12;
	private Double installmentAmount12;
	public Integer getContainerId() {
		return containerId;
	}
	public void setContainerId(Integer containerId) {
		this.containerId = containerId;
	}
	public Integer getInstallmentCount() {
		return installmentCount;
	}
	public void setInstallmentCount(Integer installmentCount) {
		this.installmentCount = installmentCount;
	}
	public Date getInstallmentInvoiceDate1() {
		return installmentInvoiceDate1;
	}
	public void setInstallmentInvoiceDate1(Date installmentInvoiceDate1) {
		this.installmentInvoiceDate1 = installmentInvoiceDate1;
	}
	public Double getInstallmentAmount1() {
		return installmentAmount1;
	}
	public void setInstallmentAmount1(Double installmentAmount1) {
		this.installmentAmount1 = installmentAmount1;
	}
	public Date getInstallmentInvoiceDate2() {
		return installmentInvoiceDate2;
	}
	public void setInstallmentInvoiceDate2(Date installmentInvoiceDate2) {
		this.installmentInvoiceDate2 = installmentInvoiceDate2;
	}
	public Double getInstallmentAmount2() {
		return installmentAmount2;
	}
	public void setInstallmentAmount2(Double installmentAmount2) {
		this.installmentAmount2 = installmentAmount2;
	}
	public Date getInstallmentInvoiceDate3() {
		return installmentInvoiceDate3;
	}
	public void setInstallmentInvoiceDate3(Date installmentInvoiceDate3) {
		this.installmentInvoiceDate3 = installmentInvoiceDate3;
	}
	public Double getInstallmentAmount3() {
		return installmentAmount3;
	}
	public void setInstallmentAmount3(Double installmentAmount3) {
		this.installmentAmount3 = installmentAmount3;
	}
	public Date getInstallmentInvoiceDate4() {
		return installmentInvoiceDate4;
	}
	public void setInstallmentInvoiceDate4(Date installmentInvoiceDate4) {
		this.installmentInvoiceDate4 = installmentInvoiceDate4;
	}
	public Double getInstallmentAmount4() {
		return installmentAmount4;
	}
	public void setInstallmentAmount4(Double installmentAmount4) {
		this.installmentAmount4 = installmentAmount4;
	}
	public Date getInstallmentInvoiceDate5() {
		return installmentInvoiceDate5;
	}
	public void setInstallmentInvoiceDate5(Date installmentInvoiceDate5) {
		this.installmentInvoiceDate5 = installmentInvoiceDate5;
	}
	public Double getInstallmentAmount5() {
		return installmentAmount5;
	}
	public void setInstallmentAmount5(Double installmentAmount5) {
		this.installmentAmount5 = installmentAmount5;
	}
	public Date getInstallmentInvoiceDate6() {
		return installmentInvoiceDate6;
	}
	public void setInstallmentInvoiceDate6(Date installmentInvoiceDate6) {
		this.installmentInvoiceDate6 = installmentInvoiceDate6;
	}
	public Double getInstallmentAmount6() {
		return installmentAmount6;
	}
	public void setInstallmentAmount6(Double installmentAmount6) {
		this.installmentAmount6 = installmentAmount6;
	}
	public Date getInstallmentInvoiceDate7() {
		return installmentInvoiceDate7;
	}
	public void setInstallmentInvoiceDate7(Date installmentInvoiceDate7) {
		this.installmentInvoiceDate7 = installmentInvoiceDate7;
	}
	public Double getInstallmentAmount7() {
		return installmentAmount7;
	}
	public void setInstallmentAmount7(Double installmentAmount7) {
		this.installmentAmount7 = installmentAmount7;
	}
	public Date getInstallmentInvoiceDate8() {
		return installmentInvoiceDate8;
	}
	public void setInstallmentInvoiceDate8(Date installmentInvoiceDate8) {
		this.installmentInvoiceDate8 = installmentInvoiceDate8;
	}
	public Double getInstallmentAmount8() {
		return installmentAmount8;
	}
	public void setInstallmentAmount8(Double installmentAmount8) {
		this.installmentAmount8 = installmentAmount8;
	}
	public Date getInstallmentInvoiceDate9() {
		return installmentInvoiceDate9;
	}
	public void setInstallmentInvoiceDate9(Date installmentInvoiceDate9) {
		this.installmentInvoiceDate9 = installmentInvoiceDate9;
	}
	public Double getInstallmentAmount9() {
		return installmentAmount9;
	}
	public void setInstallmentAmount9(Double installmentAmount9) {
		this.installmentAmount9 = installmentAmount9;
	}
	public Date getInstallmentInvoiceDate10() {
		return installmentInvoiceDate10;
	}
	public void setInstallmentInvoiceDate10(Date installmentInvoiceDate10) {
		this.installmentInvoiceDate10 = installmentInvoiceDate10;
	}
	public Double getInstallmentAmount10() {
		return installmentAmount10;
	}
	public void setInstallmentAmount10(Double installmentAmount10) {
		this.installmentAmount10 = installmentAmount10;
	}
	public Date getInstallmentInvoiceDate11() {
		return installmentInvoiceDate11;
	}
	public void setInstallmentInvoiceDate11(Date installmentInvoiceDate11) {
		this.installmentInvoiceDate11 = installmentInvoiceDate11;
	}
	public Double getInstallmentAmount11() {
		return installmentAmount11;
	}
	public void setInstallmentAmount11(Double installmentAmount11) {
		this.installmentAmount11 = installmentAmount11;
	}
	public Date getInstallmentInvoiceDate12() {
		return installmentInvoiceDate12;
	}
	public void setInstallmentInvoiceDate12(Date installmentInvoiceDate12) {
		this.installmentInvoiceDate12 = installmentInvoiceDate12;
	}
	public Double getInstallmentAmount12() {
		return installmentAmount12;
	}
	public void setInstallmentAmount12(Double installmentAmount12) {
		this.installmentAmount12 = installmentAmount12;
	}
}
