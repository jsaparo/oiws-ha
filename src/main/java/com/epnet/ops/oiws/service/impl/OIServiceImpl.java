package com.epnet.ops.oiws.service.impl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.epnet.ops.oiws.AppConstants;
import com.epnet.ops.oiws.beans.AccessingSite;
import com.epnet.ops.oiws.beans.BaseOrder;
import com.epnet.ops.oiws.beans.BaseOrderItem;
import com.epnet.ops.oiws.beans.BasicStatus;
import com.epnet.ops.oiws.beans.CreateOrderPackageRequest;
import com.epnet.ops.oiws.beans.GenerateOrderDocumentRequest;
import com.epnet.ops.oiws.beans.GetNextOrderDocumentToGenerateResponse;
import com.epnet.ops.oiws.beans.GetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.ObjectFactory;
import com.epnet.ops.oiws.beans.OrderInstallments;
import com.epnet.ops.oiws.beans.OrderOptions;
import com.epnet.ops.oiws.beans.Package;
import com.epnet.ops.oiws.beans.RemoveOrderPackageRequest;
import com.epnet.ops.oiws.beans.SaveItemResponse;
import com.epnet.ops.oiws.beans.SetItemStateRequest;
import com.epnet.ops.oiws.beans.SetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.SetOrderStateRequest;
import com.epnet.ops.oiws.beans.SetOrderStatusRequest;
import com.epnet.ops.oiws.dao.opx.IOpxDataService;
import com.epnet.ops.oiws.dao.opx.OpxException;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainer;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerAssociation;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerInstallments;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerItem;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerItemAccessingSite;
import com.epnet.ops.oiws.dao.opx.dto.OpxContainerOptions;
import com.epnet.ops.oiws.dao.opx.dto.OpxDocumentRequest;
import com.epnet.ops.oiws.dao.opx.dto.OpxItem;
import com.epnet.ops.oiws.dao.opx.type.BusinessModel;
import com.epnet.ops.oiws.dao.opx.type.DiscountType;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestStatus;
import com.epnet.ops.oiws.dao.opx.type.DocumentRequestType;
import com.epnet.ops.oiws.dao.opx.type.ItemState;
import com.epnet.ops.oiws.dao.opx.type.ContainerSource;
import com.epnet.ops.oiws.dao.opx.type.ContainerStatus;
import com.epnet.ops.oiws.dao.opx.type.ContainerType;
import com.epnet.ops.oiws.dao.opx.type.TypeUtils;
import com.epnet.ops.oiws.service.IOIService;
import com.epnet.ops.oiws.service.OIServiceException;
import com.epnet.ops.oiws.util.CurrencyUtil;
import com.epnet.ops.oiws.util.DateUtil;

/**
 * Main service implementation.  This class provides data persistence methods, 
 * interpreting between service level structures and OPX db structures.
 * 
 * @author jsaparo
 *
 */
@Transactional
@Component
public class OIServiceImpl implements IOIService {

	private Logger log = Logger.getLogger(OIServiceImpl.class);
	
	private static final String DEFAULT_LANGUAGE_CODE = "ENG";
	private static final String DEFAULT_CURRENCY_CODE = "USD";
	private static final int DEFAULT_QUANTITY = 1;
	
	ObjectFactory factory = new ObjectFactory();
	
	@Resource
	IOpxDataService opxDataService;
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#saveOrder(com.epnet.ops.oiws.beans.BaseOrder)
	 */
	@Override
	public int saveOrder(BaseOrder order) {
		log.info("saveOrder called for id: " + order.getOrderIdentifier());

		Integer orderId = order.getOrderIdentifier();
		
		// Initialize container with values that can be set for both creates and updates
		OpxContainer container = constructCommonContainerFromOrder(order);
		
		try {			
			
			// Save the main order record. If an id is specified, we are updating an existing container
			if (orderId != null) {
				BaseOrder existingOrder = getOrder(orderId);
				if (existingOrder == null) {
					throw new OIServiceException("Specified order does not exist for given id: " + orderId);
				}

				// Retain existing values that can can't be updated
				container.setCustId(existingOrder.getCustomerIdentifier());
				container.setOpportunityId(existingOrder.getOpportunityIdentifier());

				// Update db record
				opxDataService.updateContainer(container);
				
			} else { // Otherwise creating a new container
				
				// Set values that can be specified for creation
				container.setCustId(order.getCustomerIdentifier());
				container.setContainerTypeInd(getContainerTypeIdForCode(order.getCategoryCode()));
				container.setContainerStatusInd((short) ContainerStatus.WIP.getId());
				container.setContainerSourceInd(checkOrderSource(order.getSourceCode()));
				container.setOpportunityId(order.getOpportunityIdentifier());
				
				// Insert db record
				orderId = opxDataService.createContainer(container);
				order.setOrderIdentifier(orderId);
			}
			
			// Save order items - at this point we have an order id
			for (BaseOrderItem orderItem : order.getOrderItem()) {
				orderItem.setOrderIdentifier(orderId); // Be sure all get associated with same order
				saveOrderItem(orderItem);
			}
			
			// Save packages
			for (Package pkg : order.getPackage()) {
				saveOrder(pkg.getOrder());
			}		
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error saving order with identifier " + order.getOrderIdentifier() + ". Message is: " + ex.getMessage(), ex);
		}
		
		return orderId;
	}

	@Override
	public SaveItemResponse saveOrderItem(BaseOrderItem orderItem) {
		OpxContainerItem containerItem = constructContainerItemFromOrderItem(orderItem);
		
		// Determine if we're dealing with a new item
		Integer itemId = containerItem.getItemId();
		boolean isNewItem = (itemId == null);
		
		// Determine if we're dealing with a new container item
		Integer containerItemId = containerItem.getContainerItemId();
		boolean isNewContainerItem = (containerItemId == null);

		// Determine if we have a container
		boolean haveContainer = containerItem.getContainerId() != null;
		
		// If a new item, check that a container identifier has been specified. New items must be in a container.
		if (isNewItem && !haveContainer) {
			throw new OIServiceException(
				"An new order item may not be saved without specifying both a container and and item.");
		}

		if (!isNewItem && isNewContainerItem && haveContainer) { 
			// Assume we're creating a new association between an existing item and an existing container
			try {
				containerItemId = opxDataService.createContainerItem(containerItem);
			} catch (OpxException e) {
				throw new OIServiceException(
					"Error creating container item for container id: " + containerItem.getContainerId() + 
					" and item id: " + containerItem.getItemId());
			}
		} else if (!isNewItem && !isNewContainerItem) { 
			// Assume we're updating an existing item within an existing container
			try {
				OpxContainerItem existingContainerItem = opxDataService.getContainerItem(containerItemId);
				
				if (existingContainerItem == null) {
					throw new OIServiceException("Unable to update non-existent order item with id: " + containerItemId);
				}
				
				overlayExistingContainerItemWithOrderItem(existingContainerItem, orderItem);
				
				opxDataService.updateContainerItem(existingContainerItem);
				
			} catch (OpxException ex) {
				throw new OIServiceException(
					"Error saving order item with identifier " + containerItemId + " --> " + ex.getMessage(), ex);
			}
		} else if (!isNewItem && !haveContainer) { 
			// Assume we're updating an existing item, independent of any container
			try {
				OpxItem existingItem = opxDataService.getItem(itemId);
				
				if (existingItem == null) {
					throw new OIServiceException("Unable to update non-existent item with id: " + itemId);
				}
				
				overlayExistingItemWithOrderItem(existingItem, orderItem);
				
				opxDataService.updateItem(existingItem);
			} catch (OpxException ex) {
				throw new OIServiceException(
					"Error saving item with identifier " + itemId + " --> " + ex.getMessage(), ex);
			}
		}
		else if (isNewItem && isNewContainerItem && haveContainer) {
			// Assume we're creating a new item, and associating it with an existing container
			try {
				containerItemId = opxDataService.createContainerItem(containerItem);
			} catch (OpxException ex) {
				throw new OIServiceException(
					"Error creating order item for container:  " + containerItem.getContainerId() + " --> " + ex.getMessage(), ex);
			}
		} else {
			throw new OIServiceException(
				"Unknown saveItem condition: newItem: " + isNewItem + 
				", isNewContainer: " + isNewContainerItem + ", haveContainer: " + haveContainer);
		}
		
		// If we're dealing with a container item, save it's accessing site info
		if (containerItem.getContainerItemId() != null) {
			try {
				
				// First remove all existing accessing sitesj records - its' easier to batch remove/insert than upsert
				List<OpxContainerItemAccessingSite> accessingSites = 
					opxDataService.getContainerItemAccessingSites(containerItem.getContainerItemId(), containerItem.getItemId());
				
				if (!CollectionUtils.isEmpty(accessingSites)) {
					for (OpxContainerItemAccessingSite cias : accessingSites) {
						opxDataService.deleteContainerItemAccessingSite(cias.getContainerItemAccessingSiteId());
					}
				}

				// Insert new item accessing site rows
				for (AccessingSite as : orderItem.getAccessingSite()) {
					OpxContainerItemAccessingSite opxAs = createOpxAccessingSiteFromOrderAccessingSite(as);
					
					opxAs.setContainerId(containerItem.getContainerId());
					opxAs.setContainerId(containerItem.getContainerItemId());
					opxAs.setItemId(containerItem.getItemId());

					opxDataService.createContainerItemAccessingSite(opxAs);
				}
				
			} catch (OpxException ex) {
				throw new OIServiceException(
					"Error updating accessing site info for order item: " + containerItem.getContainerItemId());
			}
		}
		
		SaveItemResponse resp = factory.createSaveItemResponse();
		resp.setItemIdentifier(containerItem.getItemId());
		resp.setOrderItemIdentifier(containerItem.getContainerItemId());
		resp.setOrderIdentifier(containerItem.getContainerId());
		
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#getOrder(java.lang.String)
	 */
	@Override
	public BaseOrder getOrder(Integer orderId) {
		log.info("getOrder called for id: " + orderId);

		BaseOrder order = factory.createBaseOrder();
		
		try {
			OpxContainer container = opxDataService.getContainer(orderId, false);

			order = constructOrderFromContainer(container);
			
			// Populate order items from container
			if (!CollectionUtils.isEmpty(container.getContainerItems())) {
				for (OpxContainerItem containerItem : container.getContainerItems()) {
					order.getOrderItem().add(constructOrderItemFromContainerItem(containerItem));
				}
			}
			
			// Populate packages associated with container
			if (!CollectionUtils.isEmpty(container.getContainerAssociations())) {
				for (OpxContainerAssociation association : container.getContainerAssociations()) {
					order.getPackage().add(constructPackageFromContainerAssociation(association));
				}
			}
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error getting order with identifier " + orderId + ". Message is: " + ex.getMessage(), ex);
		}
		
		return order;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#getItem(java.lang.Integer)
	 */
	@Override
	public BaseOrderItem getOrderItem(Integer orderItemId, Integer itemId) {
		log.info("getOrderItem called for orderItem: " + orderItemId + " and item: " + itemId);
		
		if (orderItemId == null && itemId == null) {
			throw new OIServiceException("Must provide either order item id, or item id");
		}
		
		BaseOrderItem orderItem = null;
			
		try {
			if (orderItemId != null) {
				OpxContainerItem containerItem = opxDataService.getContainerItem(orderItemId);
				if (containerItem == null) {
					throw new OIServiceException(
						"Could not retrieve order item with identifier: " + orderItemId);
				}
				
				orderItem = constructOrderItemFromContainerItem(containerItem);
			} else {
				OpxItem item = opxDataService.getItem(itemId);
				
				if (item == null) {
					throw new OIServiceException(
						"Could not retrieve item with identifier: " + itemId);
				}
				
				orderItem = constructOrderItemFromItem(item);
			}
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error getting order item with identifier " + itemId + ". Message is: " + ex.getMessage(), ex);
		}
		
		return orderItem;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#listOrders(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	public List<BaseOrder> listOrders(String custId, String categoryCode, Integer salesRepId) {
		
		if (categoryCode == null && salesRepId == null) {
			throw new OIServiceException("Either category or sales rep must be specified for getting a list of orders");
		}
		
		List<BaseOrder> orders = null;
		
		try {
			// Fetch containers
			List<OpxContainer> containers = 
				opxDataService.listContainers(custId, getContainerTypeForCode(categoryCode), salesRepId);

			// Convert containers to orders
			if (!CollectionUtils.isEmpty(containers)) {
				orders = new ArrayList<BaseOrder>(containers.size());
				for (OpxContainer container : containers) {
					BaseOrder order = constructOrderFromContainer(container);
					
					// Attach order options if they exist
					OpxContainerOptions containerOptions = opxDataService.getContainerOptions(order.getOrderIdentifier());
					if (containerOptions != null) {
						order.setOptions(constructOrderOptionsFromContainerOptions(containerOptions));
					}
					
					orders.add(order);
				}
			}
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error retrieving orders for customer: " + custId + " -> " + ex.getMessage(), ex);
		}
		
		return orders;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#removeOrderItem(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public BasicStatus removeOrderItem(Integer orderId, Integer itemId) {
		log.info("removeOrderItem called for order: " + orderId + " and item: " + itemId);
		
		try {
			opxDataService.removeItemFromContainer(orderId, itemId);
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error removing item: " + itemId + 
				" from order: " + orderId + " -> " + ex.getMessage(), ex);
		}
		
		return factory.createBasicStatus();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#submitOrder(java.lang.String)
	 */
	@Override
	public void submitOrder(int orderId) {
		log.info("submitOrder called for id: " + orderId);
		
		try {
			// Fetch existing container status
			OpxContainer container = opxDataService.getContainer(orderId, true);
			ContainerStatus orderStatus = ContainerStatus.getById(container.getContainerStatusInd());
			
			// Can only set to submitted if status is WIP
			if (orderStatus != ContainerStatus.WIP) {
				throw new OIServiceException("Order " + orderId + " with status: " + orderStatus + " may not be submitted.");
			}

			// Set status
			opxDataService.setContainerState(orderId, ContainerStatus.SUBMITTED, ItemState.SUBMITTED);
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error setting status to submitted for order " + orderId + " -> " + ex.getMessage(), ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#setOrderStatus(com.epnet.ops.oiws.beans.SetOrderStatusRequest)
	 */
	@Override
	public BasicStatus setOrderStatus(SetOrderStatusRequest request) {
		log.info("removeOrderItem called for order: " + request.getOrderIdentifier() + 
			" and status: " + request.getStatusCode());
		
		try {
			opxDataService.updateContainerStatus(request.getOrderIdentifier(), ContainerStatus.valueOf(request.getStatusCode()));
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error setting order status for order: " + request.getOrderIdentifier() + 
				" and status " + request.getStatusCode() + " -> " + ex.getMessage(), ex);
		}
		
		BasicStatus status = factory.createBasicStatus();
		status.setStatusCode(AppConstants.STATUS_OK);
		
		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#createOrderDocument(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public BasicStatus generateOrderDocument(GenerateOrderDocumentRequest request) {
		log.info("createOrderDocument called for : " + request);
		
		try {
			DocumentRequestType docRequestType = TypeUtils.getDocumentRequestTypeFromCode(request.getDocTypeCode());
			
			if (docRequestType == null) {
				throw new OIServiceException("Invalid document request type: " + request.getDocTypeCode());
			}
			
			if (request.getLanguageCode() == null) {
				request.setLanguageCode(DEFAULT_LANGUAGE_CODE);
			}
			
			opxDataService.createDocumentGenerationRequest(
				request.getOrderIdentifier(), docRequestType, request.getBaseFileName(), request.getLanguageCode());
			
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error creating document generation request for order: " + request.getOrderIdentifier() + 
				" and type " + request.getDocTypeCode() + " -> " + ex.getMessage(), ex);
		}
		
		BasicStatus status = factory.createBasicStatus();
		status.setStatusCode(AppConstants.STATUS_OK);
		
		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#getNextOrderDocumentToCreate(java.lang.String)
	 */
	@Override
	public synchronized GetNextOrderDocumentToGenerateResponse getNextOrderDocumentToGenerate() {
		
		GetNextOrderDocumentToGenerateResponse response = factory.createGetNextOrderDocumentToGenerateResponse();
		
		Integer docRequestId = null;

		try {
			docRequestId = opxDataService.getNextUnprocessedDocumentRequestId();
			
			if (docRequestId != null) {
				response.setRequestId(docRequestId);
				
				OpxDocumentRequest docRequest = opxDataService.getDocumentGenerationRequest(docRequestId);
				if (docRequest == null) {
					throw new OIServiceException("Unable to retrieve document request for id: " + docRequestId);
				}
				
				DocumentRequestType docType = DocumentRequestType.getById(docRequest.getDocRequestTypeCode());
				
				if (docType == null) {
					throw new OIServiceException(
						"Invalid doc type for request id: " + docRequestId + " -> " + docRequest.getDocumentRequestId());
				}
				response.setDocTypeCode(docType.name());
				response.setBaseFileName(docRequest.getBaseFileName());
				response.setLanguageCode(docRequest.getLanguageCode());
				response.setOrderIdentifier(docRequest.getContainerId());
			}
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error fetching next document generation request -> " + ex.getMessage(), ex);
		}
		
		return response;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#setOrderDocumentStatus(com.epnet.ops.oiws.beans.SetOrderDocumentStatusRequest)
	 */
	@Override
	public BasicStatus setOrderDocumentStatus(SetOrderDocumentStatusRequest request) {
		log.info("setOrderDocumentStatus called for : " + request);

		DocumentRequestStatus docRequestStatus = TypeUtils.getDocumentRequestStatusFromCode(request.getStatusCode());
		
		if (docRequestStatus == null) {
			throw new OIServiceException("Invalid document status: " + request.getStatusCode());
		}
		
		try {
			opxDataService.setDocumentGenerationRequestStatus(request.getRequestId(), docRequestStatus, request.getFileLocation());
		} catch (OpxException ex) {
			throw new OIServiceException(
				"Error setting document generation status for request: " + request.getRequestId() + 
				" and status " + request.getStatusCode() + " -> " + ex.getMessage(), ex);
		}
		
		BasicStatus status = factory.createBasicStatus();
		status.setStatusCode(AppConstants.STATUS_OK);
		
		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#getOrderDocumentStatus(com.epnet.ops.oiws.beans.GetOrderDocumentStatusRequest)
	 */
	@Override
	public BasicStatus getOrderDocumentStatus(GetOrderDocumentStatusRequest request) {
		log.info("getOrderDocumentStatus called for : " + request);

		BasicStatus status = factory.createBasicStatus();
		status.setStatusCode(AppConstants.STATUS_OK);
		
		return status;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#getNextSubmittedOrderToProcess()
	 */
	@Override
	public synchronized Integer getNextSubmittedOrderToProcess() {

		OpxContainer container = null;
		
		try {
			container = opxDataService.getFirstContainersByStatusAndType(ContainerStatus.SUBMITTED, ContainerType.ORDER);

			if (container != null) {
				// Set container and items to a processed state
				opxDataService.setContainerState(
					container.getContainerId(), ContainerStatus.PROCESSED, ItemState.PROCESSED);
			}
		} catch (OpxException ex) {
			throw new OIServiceException("Error setting order status to PROCESSED.", ex);
		}
		
		return container != null ? container.getContainerId() : null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#deleteOrder(java.lang.Integer)
	 */
	@Override
	public BasicStatus deleteOrder(Integer orderId) {
		try {
			opxDataService.deleteContainer(orderId);
		} catch (OpxException ex) {
			throw new OIServiceException("Error deleting order with id: " + orderId + " - > " + ex.getMessage(), ex);
		}
		
		return new BasicStatus();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#createOrderPackage(com.epnet.ops.oiws.beans.CreateOrderPackageRequest)
	 */
	@Override
	public Integer createOrderPackage(CreateOrderPackageRequest request) {
		
		Integer packageId = null;
		
		try {
			packageId = opxDataService.createContainerAssociation(request.getOrderIdentifier(), request.getPackageOrderIdentifier());
		} catch (OpxException ex) {
			throw new OIServiceException("Error associating order with id: " + request.getOrderIdentifier() + 
				" with package id: " +  request.getOrderIdentifier() +  " - > " + ex.getMessage(), ex);
		}
		
		return packageId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#removeOrderPackage(com.epnet.ops.oiws.beans.RemoveOrderPackageRequest)
	 */
	@Override
	public BasicStatus removeOrderPackage(RemoveOrderPackageRequest request) {
		try {
			opxDataService.deleteContainerAssociation(request.getPackageIdentifier());
		} catch (OpxException ex) {
			throw new OIServiceException("Error order package with id: " + request.getPackageIdentifier() + " - > " + ex.getMessage(), ex);
		}
		
		return new BasicStatus();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#setOrderState(com.epnet.ops.oiws.beans.SetOrderStateRequest)
	 */
	@Override
	public BasicStatus setOrderState(SetOrderStateRequest request) {
		try {
			ContainerStatus orderStatus = TypeUtils.getContainerStatusFromCode(request.getOrderStateCode());
			
			if (orderStatus == null) {
				throw new OIServiceException("Invalid order state specified: " + request.getOrderStateCode());
			}
			
			ItemState itemState = request.getItemStateCode() != null ? TypeUtils.getItemStateFromCode(request.getItemStateCode()) : null;
			
			opxDataService.setContainerState(request.getOrderIdentifier(), orderStatus, itemState);
		} catch (OpxException ex) {
			throw new OIServiceException("Error setting order state for order id: " + 
				request.getOrderIdentifier() + " - > " + ex.getMessage(), ex);
		}
		
		return new BasicStatus();
	}

	/*
	 * (non-Javadoc)
	 * @see com.epnet.ops.oiws.service.IOIService#setItemState(com.epnet.ops.oiws.beans.SetItemStateRequest)
	 */
	@Override
	public BasicStatus setItemState(SetItemStateRequest request) {
		try {
			ItemState itemState = request.getItemStateCode() != null ? TypeUtils.getItemStateFromCode(request.getItemStateCode()) : null;

			if (itemState == null) {
				throw new OIServiceException("Invalid item state specified: " + request.getItemStateCode());
			}
			opxDataService.setItemState(request.getItemIdentifier(), itemState);
		} catch (OpxException ex) {
			throw new OIServiceException("Error setting item state for item id: " + 
				request.getItemIdentifier() + " - > " + ex.getMessage(), ex);
		}
		
		return new BasicStatus();
	}

	/**
	 * Fetch the enumeration id of the given container type code.
	 * 
	 * @param code The code to look up
	 * @return An identifier, or null if not found
	 */
	private Short getContainerTypeIdForCode(String code) {
		ContainerType containerType = getContainerTypeForCode(code);
		
		if (containerType == null) {
			return null;
		}
		
		return (short) containerType.getId();
	}

	/**
	 * Fetch the enumeration id of the given container type code.
	 * 
	 * @param code The code to look up
	 * @return An identifier, or null if not found
	 */
	private ContainerType getContainerTypeForCode(String code) {
		if (StringUtils.isEmpty(code)) {
			return null;
		}
		
		try {
			return ContainerType.valueOf(code);
		} catch (IllegalArgumentException e) {
			throw new OIServiceException("Invalid container type specified: " + code);
		}
	}
	
	/**
	 * Fetch the enumeration id of the given order source code.
	 * 
	 * @param code The code to look up
	 * @return An identifier, or null if not found
	 */
	private Short checkOrderSource(String code) {
		if (StringUtils.isEmpty(code)) {
			return null;
		}
		
		try {
			return (short) ContainerSource.valueOf(code).getId();
		} catch (IllegalArgumentException e) {
			throw new OIServiceException("Invalid order source specified: " + code);
		}
	}

	/**
	 * Initializes a new order object from a given container object.
	 * 
	 * @param opxContainer The container to create the order from.
	 * 
	 * @return A populated order object
	 */
	private BaseOrder constructOrderFromContainer(OpxContainer opxContainer) {
		BaseOrder order = factory.createBaseOrder();
		order.setOrderIdentifier(opxContainer.getContainerId());
		order.setCategoryCode(ContainerType.getById(opxContainer.getContainerTypeInd()).toString());
		order.setCustomerIdentifier(opxContainer.getCustId());
		order.setLanguageCode(opxContainer.getLanguageCode());
		order.setCurrencyCode(opxContainer.getCurrencyCode());
		order.setSalesRepIdentifier(opxContainer.getSalesRepId());
		order.setOpportunityIdentifier(opxContainer.getOpportunityId());
		order.setCustomerContactIdentifier(opxContainer.getCustomerContactId());
		order.setPurchaseOrderNumber(opxContainer.getPurchaseOrderNumber());
		order.setSourceCode(ContainerSource.getById(opxContainer.getContainerSourceInd()).toString());
		order.setStateCode(ContainerStatus.getById(opxContainer.getContainerStatusInd()).toString());
		order.setBillingContactIdentifier(opxContainer.getBillingContactId());
		order.setBillingAddressIdentifier(opxContainer.getBillingAddressId());
	    order.setModifiedDate(DateUtil.createXMLGregorianCalendar(opxContainer.getModifiedDate()));

		// Derive additional display-only currency values
		if (!StringUtils.isEmpty(order.getCurrencyCode())) {
			Currency cur = Currency.getInstance(order.getCurrencyCode());
			order.setCurrencyName(cur.getDisplayName());
			String symbol = CurrencyUtil.getSymbolForCode(order.getCurrencyCode());
			order.setCurrencySymbol(symbol != null ? symbol : "");
		}

		
		// Populate options if present
		if (opxContainer.getOptions() != null) {
			order.setOptions(constructOrderOptionsFromContainerOptions(opxContainer.getOptions()));
		}
		
		// Populate installments if present
		if (opxContainer.getInstallments() != null) {
			order.setInstallments(constructOrderInstallmentsFromContainerInstallments(opxContainer.getInstallments()));
		}

		return order;
	}

	/**
	 * Initializes a new order item from a given container item.
	 * 
	 * @param order The container item to create the order item from.
	 * 
	 * @return A populated order item object
	 */
	private BaseOrderItem constructOrderItemFromContainerItem(OpxContainerItem containerItem) {
		BaseOrderItem orderItem = constructOrderItemFromItem(containerItem.getItem());
		
		orderItem.setOrderIdentifier(containerItem.getContainerId());
		orderItem.setOrderItemIdentifier(containerItem.getContainerItemId());
	    orderItem.setSequence(containerItem.getSequence());

	    // Override item discount type if specified
	    if (containerItem.getDiscountTypeInd() != null) {
	    	DiscountType discountType = DiscountType.getById(containerItem.getDiscountTypeInd());
	    	if (discountType != null) {
	    		orderItem.setDiscountType(discountType.name());
	    	}
	    }
	    
	    // Override item discount amount if specified
	    if (containerItem.getDiscountAmount() != null) {
	    	orderItem.setDiscountAmount(containerItem.getDiscountAmount());
	    }
	    
	    // Override item discount price if specified
	    if (containerItem.getDiscountPrice() != null) {
	    	orderItem.setDiscountPrice(containerItem.getDiscountPrice());
	    }
	    
	    if (containerItem.getGrossPrice() != null) {
	    	orderItem.setGrossPrice(containerItem.getGrossPrice());
	    }
	    
	    if (containerItem.getStartDate() != null) {
	    	orderItem.setStartDate(DateUtil.createXMLGregorianCalendar(containerItem.getStartDate()));
	    }
	    
	    if (containerItem.getEndDate() != null) {
	    	orderItem.setEndDate(DateUtil.createXMLGregorianCalendar(containerItem.getEndDate()));
	    }
	    
	    if (containerItem.getGroupCode() != null) {
	    	orderItem.setGroupCode(containerItem.getGroupCode());
	    }
	    
	    if (containerItem.getTrialInterfaces() != null) {
	    	orderItem.setTrialInterfaces(containerItem.getTrialInterfaces());
	    }
	    
	    if (containerItem.getActive() != null) {
	    	orderItem.setActive(containerItem.getActive());
	    }
	    
	    if (containerItem.getTermInMonths() != null) {
	    	orderItem.setTermInMonths(containerItem.getTermInMonths());
	    }

		if (!CollectionUtils.isEmpty(containerItem.getAccessingSites())) {
			for (OpxContainerItemAccessingSite opxAs : containerItem.getAccessingSites()) {
				orderItem.getAccessingSite().add(createAccessingSiteFromOpxAccessingSite(opxAs));
			}
		}
		
	    return orderItem;
	}
	
	/**
	 * Initializes a new order item from a given container item.
	 * 
	 * @param opxItem The item to create the order item from.
	 * 
	 * @return A populated order item object
	 */
	private BaseOrderItem constructOrderItemFromItem(OpxItem opxItem) {
		BaseOrderItem orderItem = factory.createBaseOrderItem();
		
		orderItem.setItemIdentifier(opxItem.getItemId());		
	    orderItem.setProductIdentifier(opxItem.getProductId());
		orderItem.setStartDate(DateUtil.createXMLGregorianCalendar(opxItem.getStartDate()));
	    orderItem.setEndDate(DateUtil.createXMLGregorianCalendar(opxItem.getEndDate()));
	    orderItem.setTermInMonths(opxItem.getTermInMonths());
	    orderItem.setGrossPrice(opxItem.getGrossPrice());
	    if (opxItem.getDiscountTypeInd() != null) {
	    	DiscountType discountType = DiscountType.getById(opxItem.getDiscountTypeInd());
	    	if (discountType != null) {
	    		orderItem.setDiscountType(discountType.name());
	    	}
	    }
	    orderItem.setDiscountPrice(opxItem.getDiscountPrice());
	    orderItem.setDiscountAmount(opxItem.getDiscountAmount());
	    orderItem.setPremiumPricingDiscount(opxItem.getPremiumPricingDiscount());
	    orderItem.setPrimaryAgencyDiscount(opxItem.getPrimaryAgencyDiscount());
	    orderItem.setComplimentary(opxItem.getComplimentary());
	    orderItem.setQuantity(opxItem.getQuantity());
	    if (opxItem.getBusinessModelId() != null) {
    		BusinessModel model = TypeUtils.getBusinessModelForId(opxItem.getBusinessModelId());
    		if (model != null) {
    			orderItem.setBusinessModel(model.name());
    		}
	    }
	    orderItem.setRoyaltyAmount(opxItem.getRoyaltyAmount());
	    orderItem.setRoyaltyComment(opxItem.getRoyaltyComment());
	    orderItem.setRenewal(opxItem.getRenewal());
	    orderItem.setRenewalSubscriptionIdentifier(opxItem.getRenewalSubscriptionId());
	    orderItem.setRenewalNote(opxItem.getRenewalNote());
	    orderItem.setContractNumber(opxItem.getContractNumber());
	    orderItem.setStateCode(ItemState.getById(opxItem.getItemStateInd()).toString());
	    orderItem.setProbability(opxItem.getProbability());
	    orderItem.setModifiedDate(DateUtil.createXMLGregorianCalendar(opxItem.getModifiedDate()));
	    orderItem.setExpectedCloseDate(DateUtil.createXMLGregorianCalendar(opxItem.getExpectedCloseDate()));
	    orderItem.setPurchaseOrderNumber(opxItem.getPurchaseOrderNumber());
	    orderItem.setTidIdentifier(opxItem.getTidIdentifier());
	    orderItem.setListRate(opxItem.getListRate());
	    orderItem.setMaxSimultaneousUsers(opxItem.getMaxSimultaneousUsers());
	    orderItem.setPricedThroughConsortiumIdentifier(opxItem.getPricedThroughConsortiumId());
		orderItem.setInvoiceStartDate(DateUtil.createXMLGregorianCalendar(opxItem.getInvoiceStartDate()));
		orderItem.setMultiYearPayments(opxItem.getMultiYearPayments());
		orderItem.setAmountYear1(opxItem.getAmountYear1());
		orderItem.setAmountYear2(opxItem.getAmountYear2());
		orderItem.setAmountYear3(opxItem.getAmountYear3());
		orderItem.setAmountYear4(opxItem.getAmountYear4());
		orderItem.setAmountYear5(opxItem.getAmountYear5());
		orderItem.setAmountYear6(opxItem.getAmountYear6());
		orderItem.setAmountYear7(opxItem.getAmountYear7());
		orderItem.setAmountYear8(opxItem.getAmountYear8());
		orderItem.setAmountYear9(opxItem.getAmountYear9());
		orderItem.setAmountYear10(opxItem.getAmountYear10());
		orderItem.setUpgradeDowngradeFlag(opxItem.getUpgradeDowngradeFlag());
		orderItem.setEhisPurchaseCount(opxItem.getEhisPurchaseCount());
		orderItem.setEhisFreeCount(opxItem.getEhisFreeCount());
		orderItem.setBooksConsortia(opxItem.getBooksConsortia());
		orderItem.setBooksShared(opxItem.getBooksShared());
		orderItem.setPrevListRate(opxItem.getPrevListRate());
		orderItem.setPrevTerm(opxItem.getPrevTerm());
		orderItem.setPrevRoyaltyAmount(opxItem.getPrevRoyaltyAmount());
		orderItem.setPrevRoyaltyComment(opxItem.getPrevRoyaltyComment());
		orderItem.setStatusTag(opxItem.getStatusTag());
		orderItem.setItemFte(opxItem.getItemFTE());
		orderItem.setItemNote(opxItem.getItemNote());
		orderItem.setGroupCode(opxItem.getGroupCode());
		orderItem.setTrialInterfaces(opxItem.getTrialInterfaces());
		orderItem.setActive(opxItem.getActive());
		orderItem.setLostReason(opxItem.getLostReason());
		orderItem.setLostNotes(opxItem.getLostNotes());
		orderItem.setPricedThroughConsortiumIdentifier(opxItem.getPricedThroughConsortiumId());
		
	    return orderItem;
	}
	
	/**
	 * Create a container object from a given order object, initializing it
	 * with values common to both creating and updating a container.
	 * 
	 * @param order The order object to create the container from.
	 * 
	 * @return A populated Opx container object
	 */
	private OpxContainer constructCommonContainerFromOrder(BaseOrder order) {
		OpxContainer container = new OpxContainer();
		container.setContainerId(order.getOrderIdentifier());
		container.setSalesRepId(order.getSalesRepIdentifier());
		container.setCurrencyCode(order.getCurrencyCode() != null ? order.getCurrencyCode() : DEFAULT_CURRENCY_CODE);
		container.setLanguageCode(order.getLanguageCode() != null ? order.getLanguageCode() : DEFAULT_LANGUAGE_CODE);
		container.setCustomerContactId(order.getCustomerContactIdentifier());
		container.setPurchaseOrderNumber(order.getPurchaseOrderNumber());
		container.setBillingContactId(order.getBillingContactIdentifier());
		container.setBillingAddressId(order.getBillingAddressIdentifier());
		
		// Populate options
		if (order.getOptions() != null) {
			container.setOptions(
				constructContainerOptionsFromOrderOptions(order.getOrderIdentifier(), order.getOptions()));
		}
		
		// Populate installments
		if (order.getInstallments() != null) {
			container.setInstallments(
				constructContainerInstallmentsFromOrderInstallments(order.getOrderIdentifier(), order.getInstallments()));
		}

		return container;
	}

	/**
	 * Initializes a new opx container item from a given order item and order.
	 * Perform certain validations and use default value in some cases.
	 * 
	 * @param orderItem The item to create the container item from.
	 * 
	 * @return A populated Opx item object
	 */
	private OpxContainerItem constructContainerItemFromOrderItem(BaseOrderItem orderItem) {
		OpxContainerItem containerItem = new OpxContainerItem();

		// Load container item link info
		containerItem.setContainerId(orderItem.getOrderIdentifier());
		containerItem.setContainerItemId(orderItem.getOrderItemIdentifier());
		containerItem.setItemId(orderItem.getItemIdentifier());
		
		OpxItem opxItem = new OpxItem();
		containerItem.setItem(opxItem);
		containerItem.setSequence(orderItem.getSequence());

		// Check product id
		if (orderItem.getProductIdentifier() <= 0) {
			throw new OIServiceException("Invalid product identifier specified: " + orderItem.getProductIdentifier());
		}
		
		opxItem.setProductId(orderItem.getProductIdentifier());
		
		// Set initial state of item to WIP
		opxItem.setItemStateInd((short) ItemState.WIP.getId());

		// Check start date
		if (orderItem.getStartDate() == null) {
			throw new OIServiceException("Start date must be specified for item with product id: " + opxItem.getProductId());
		}

		opxItem.setStartDate(DateUtil.createDate(orderItem.getStartDate()));

		// Check/default term in months
		opxItem.setTermInMonths(orderItem.getTermInMonths());
		
		// Set end date if specified
		if (orderItem.getEndDate() != null) {
			opxItem.setEndDate(DateUtil.createDate(orderItem.getEndDate()));
		} else if (opxItem.getTermInMonths() != null) {
			// Default to current date plus term in months minus one day (exclusive) if end date is null
			// and term in months is specified
			Calendar cal = Calendar.getInstance();
			cal.setTime(opxItem.getStartDate());
			cal.add(Calendar.MONTH, opxItem.getTermInMonths());
			cal.add(Calendar.DAY_OF_MONTH, -1);
			opxItem.setEndDate(cal.getTime());
		}
		
		// Check that end date is after begin date
		if (opxItem.getEndDate() != null & opxItem.getStartDate() != null && !opxItem.getEndDate().after(opxItem.getStartDate())) {
			throw new OIServiceException("End date must be after start date for item with product id: " + opxItem.getProductId());			
		}
		
		// Check gross price. Use discount price if not specified.
		Double grossPrice = orderItem.getGrossPrice() != null ? orderItem.getGrossPrice() : orderItem.getDiscountPrice();
		opxItem.setGrossPrice(grossPrice);
		
		// Check discount price. Use gross price if not specified.
		Double discountPrice = orderItem.getDiscountPrice() != null ? orderItem.getDiscountPrice() : orderItem.getGrossPrice();
		opxItem.setDiscountPrice(discountPrice);
		
		// Check/default quantity
		opxItem.setQuantity(orderItem.getQuantity() != null ? orderItem.getQuantity() : DEFAULT_QUANTITY);
		
		// Check/default business model
		if (!StringUtils.isEmpty(orderItem.getBusinessModel())) {
			try {
				opxItem.setBusinessModelId(BusinessModel.valueOf(orderItem.getBusinessModel()).getId());
			} catch (Exception ex) {
				throw new OIServiceException("Invalid business model '" + orderItem.getBusinessModel() + 
					"' specified for item with product id: " + opxItem.getProductId());							
			}
		}
		
		// Other values can be null
		opxItem.setPremiumPricingDiscount(orderItem.getPremiumPricingDiscount());
		opxItem.setPrimaryAgencyDiscount(orderItem.getPrimaryAgencyDiscount());
		opxItem.setComplimentary(orderItem.isComplimentary());
		opxItem.setRenewal(orderItem.isRenewal());
		opxItem.setRenewalSubscriptionId(orderItem.getRenewalSubscriptionIdentifier());
		opxItem.setRoyaltyAmount(orderItem.getRoyaltyAmount());
		opxItem.setRoyaltyComment(orderItem.getRoyaltyComment());
		opxItem.setRenewalNote(orderItem.getRenewalNote());
		opxItem.setContractNumber(orderItem.getContractNumber());
		opxItem.setProbability(orderItem.getProbability());
		opxItem.setExpectedCloseDate(DateUtil.createDate(orderItem.getExpectedCloseDate()));
		opxItem.setPurchaseOrderNumber(orderItem.getPurchaseOrderNumber());
	    opxItem.setTidIdentifier(orderItem.getTidIdentifier());
	    opxItem.setListRate(orderItem.getListRate());
	    opxItem.setMaxSimultaneousUsers(orderItem.getMaxSimultaneousUsers());
		opxItem.setInvoiceStartDate(DateUtil.createDate(orderItem.getInvoiceStartDate()));
		opxItem.setMultiYearPayments(orderItem.getMultiYearPayments());
		opxItem.setAmountYear1(orderItem.getAmountYear1());
		opxItem.setAmountYear2(orderItem.getAmountYear2());
		opxItem.setAmountYear3(orderItem.getAmountYear3());
		opxItem.setAmountYear4(orderItem.getAmountYear4());
		opxItem.setAmountYear5(orderItem.getAmountYear5());
		opxItem.setAmountYear6(orderItem.getAmountYear6());
		opxItem.setAmountYear7(orderItem.getAmountYear7());
		opxItem.setAmountYear8(orderItem.getAmountYear8());
		opxItem.setAmountYear9(orderItem.getAmountYear9());
		opxItem.setAmountYear10(orderItem.getAmountYear10());
		opxItem.setUpgradeDowngradeFlag(orderItem.getUpgradeDowngradeFlag());
		opxItem.setEhisPurchaseCount(orderItem.getEhisPurchaseCount());
		opxItem.setEhisFreeCount(orderItem.getEhisFreeCount());
		opxItem.setBooksConsortia(orderItem.isBooksConsortia());
		opxItem.setBooksShared(orderItem.isBooksShared());
		opxItem.setPrevListRate(orderItem.getPrevListRate());
		opxItem.setPrevTerm(orderItem.getPrevTerm());
		opxItem.setPrevRoyaltyAmount(orderItem.getPrevRoyaltyAmount());
		opxItem.setPrevRoyaltyComment(orderItem.getPrevRoyaltyComment());
		opxItem.setStatusTag(orderItem.getStatusTag());
		opxItem.setItemFTE(orderItem.getItemFte());
		opxItem.setItemNote(orderItem.getItemNote());
		opxItem.setGroupCode(orderItem.getGroupCode());
		opxItem.setTrialInterfaces(orderItem.getTrialInterfaces());
		opxItem.setActive(orderItem.isActive());
		opxItem.setLostReason(orderItem.getLostReason());
		opxItem.setLostNotes(orderItem.getLostNotes());
		opxItem.setPricedThroughConsortiumId(orderItem.getPricedThroughConsortiumIdentifier());		

		return containerItem;
	}
	
	/**
	 * Overlay updatable fields from a given order item on top of an existing container item.
	 * Not all fields are able to be updated.
	 * 
	 * @param opxItem
	 * @param orderItem
	 */
	private void overlayExistingContainerItemWithOrderItem(OpxContainerItem containerItem, BaseOrderItem orderItem) {
		containerItem.setSequence(orderItem.getSequence());
		overlayExistingItemWithOrderItem(containerItem.getItem(), orderItem);
	}
	
	/**
	 * Overlay updatable fields from a given order item on top of an existing container item.
	 * Not all fields are able to be updated.
	 * 
	 * @param opxItem
	 * @param orderItem
	 */
	private void overlayExistingItemWithOrderItem(OpxItem opxItem, BaseOrderItem orderItem) {
		opxItem.setStartDate(DateUtil.createDate(orderItem.getStartDate()));
		opxItem.setEndDate(DateUtil.createDate(orderItem.getEndDate()));
		opxItem.setTermInMonths(orderItem.getTermInMonths());
		opxItem.setGrossPrice(orderItem.getGrossPrice());
		opxItem.setDiscountPrice(orderItem.getDiscountPrice());
		opxItem.setComplimentary(orderItem.isComplimentary());
		opxItem.setQuantity(orderItem.getQuantity());
		opxItem.setBusinessModelId(TypeUtils.getBusinessModelIdFromCode(orderItem.getBusinessModel()));
		opxItem.setRoyaltyAmount(orderItem.getRoyaltyAmount());
		opxItem.setRoyaltyComment(orderItem.getRoyaltyComment());
		opxItem.setRenewal(orderItem.isRenewal());
		opxItem.setRenewalSubscriptionId(orderItem.getRenewalSubscriptionIdentifier());
		opxItem.setRenewalNote(orderItem.getRenewalNote());
		opxItem.setContractNumber(orderItem.getContractNumber());
		opxItem.setItemStateInd((short) ItemState.valueOf(orderItem.getStateCode()).getId());
		opxItem.setProbability(orderItem.getProbability());
		opxItem.setExpectedCloseDate(DateUtil.createDate(orderItem.getExpectedCloseDate()));
		opxItem.setPurchaseOrderNumber(orderItem.getPurchaseOrderNumber());
	    opxItem.setTidIdentifier(orderItem.getTidIdentifier());
	    opxItem.setListRate(orderItem.getListRate());
	    opxItem.setMaxSimultaneousUsers(orderItem.getMaxSimultaneousUsers());
		opxItem.setInvoiceStartDate(DateUtil.createDate(orderItem.getInvoiceStartDate()));
		opxItem.setMultiYearPayments(orderItem.getMultiYearPayments());
		opxItem.setAmountYear1(orderItem.getAmountYear1());
		opxItem.setAmountYear2(orderItem.getAmountYear2());
		opxItem.setAmountYear3(orderItem.getAmountYear3());
		opxItem.setAmountYear4(orderItem.getAmountYear4());
		opxItem.setAmountYear5(orderItem.getAmountYear5());
		opxItem.setAmountYear6(orderItem.getAmountYear6());
		opxItem.setAmountYear7(orderItem.getAmountYear7());
		opxItem.setAmountYear8(orderItem.getAmountYear8());
		opxItem.setAmountYear9(orderItem.getAmountYear9());
		opxItem.setAmountYear10(orderItem.getAmountYear10());
		opxItem.setUpgradeDowngradeFlag(orderItem.getUpgradeDowngradeFlag());
		opxItem.setEhisPurchaseCount(orderItem.getEhisPurchaseCount());
		opxItem.setEhisFreeCount(orderItem.getEhisFreeCount());
		opxItem.setBooksConsortia(orderItem.isBooksConsortia());
		opxItem.setBooksShared(orderItem.isBooksShared());
		opxItem.setPrevListRate(orderItem.getPrevListRate());
		opxItem.setPrevTerm(orderItem.getPrevTerm());
		opxItem.setPrevRoyaltyAmount(orderItem.getPrevRoyaltyAmount());
		opxItem.setPrevRoyaltyComment(orderItem.getPrevRoyaltyComment());
		opxItem.setStatusTag(orderItem.getStatusTag());
		opxItem.setItemFTE(orderItem.getItemFte());
		opxItem.setItemNote(orderItem.getItemNote());
		opxItem.setGroupCode(orderItem.getGroupCode());
		opxItem.setTrialInterfaces(orderItem.getTrialInterfaces());
		opxItem.setActive(orderItem.isActive());
		opxItem.setLostReason(orderItem.getLostReason());
		opxItem.setLostNotes(orderItem.getLostNotes());
		opxItem.setPricedThroughConsortiumId(orderItem.getPricedThroughConsortiumIdentifier());		
	}
	
	/**
	 * Create a package object from an Opx container association.
	 * @param association The association to create the package from
	 * @return A package object
	 */
	private Package constructPackageFromContainerAssociation(OpxContainerAssociation association) {
		Package pkg = factory.createPackage();

		pkg.setPackageIdentifier(association.getContainerAssociationId());
		pkg.setOrder(getOrder(association.getChildContainerId()));
		
		return pkg;
	}
	
	/**
	 * Create a container accessing site from an order accessing site
	 * 
	 * @param as The accessing site to create from
	 * @return
	 */
	private OpxContainerItemAccessingSite createOpxAccessingSiteFromOrderAccessingSite(AccessingSite as) {
		OpxContainerItemAccessingSite opxAs = new OpxContainerItemAccessingSite();
		
		opxAs.setCustId(as.getCustomerIdentifier());
		if (!CollectionUtils.isEmpty(as.getInterfaceIdentifier())) {
			opxAs.setInterfaceIds(StringUtils.join(as.getInterfaceIdentifier(), ","));
		}

		return opxAs;
	}

	private AccessingSite createAccessingSiteFromOpxAccessingSite(OpxContainerItemAccessingSite opxAs) {
		AccessingSite as = factory.createAccessingSite();
		
		as.setCustomerIdentifier(opxAs.getCustId());
		
		if (!StringUtils.isEmpty(opxAs.getInterfaceIds())) {
			String[] ids = StringUtils.split(opxAs.getInterfaceIds(), ",");
			for (String id : ids) {
				as.getInterfaceIdentifier().add(id);
			}
		}

		return as;
	}
	
	private OpxContainerOptions constructContainerOptionsFromOrderOptions(Integer orderId, OrderOptions orderOptions) {
		OpxContainerOptions containerOptions = new OpxContainerOptions();

		containerOptions.setContainerId(orderId);
		containerOptions.setOrderDescription(orderOptions.getOrderDescription());
		containerOptions.setCustomerNote(orderOptions.getCustomerNote());
		containerOptions.setTermsAndConditions(orderOptions.getTermsAndConditions());
		containerOptions.setInvoiceDisplayDate(DateUtil.createDate(orderOptions.getInvoiceDisplayDate()));
		containerOptions.setDisplaySubscriptionId(orderOptions.isDisplaySubscriptionId());
		containerOptions.setDisplayPackagedItemPrices(orderOptions.isDisplayPackagedItemPrices());
		containerOptions.setDisplayItemInterfaces(orderOptions.isDisplayItemInterfaces());
		containerOptions.setDisplaySimultaneousUsers(orderOptions.isDisplaySimultaneousUsers());
		containerOptions.setQuoteExpireDays(orderOptions.getQuoteExpireDays());
		containerOptions.setShowMarketingOnQuote(orderOptions.isShowMarketingOnQuote());
		containerOptions.setDisplayItemDiscount(orderOptions.isDisplayItemDiscount());
		containerOptions.setDisplayAccessingSiteCount(orderOptions.isDisplayAccessingSiteCount());
		containerOptions.setPayByCreditCard(orderOptions.isPayByCreditCard());
		containerOptions.setDisplayPackageNames(orderOptions.isDisplayPackageNames());
		containerOptions.setDisplayTaxId(orderOptions.isDisplayTaxId());
		containerOptions.setTaxLanguage(orderOptions.getTaxLanguage());
		containerOptions.setTaxLabel(orderOptions.getTaxLabel());
		containerOptions.setTaxPercent(orderOptions.getTaxPercent());
		containerOptions.setInvoiceAsap(orderOptions.isInvoiceAsap());
		containerOptions.setRushOrder(orderOptions.isRushOrder());
		containerOptions.setQuotedCurrency(orderOptions.getQuotedCurrency());
		containerOptions.setInvoiceNote(orderOptions.getInvoiceNote());
		containerOptions.setOrderProcessingNote(orderOptions.getOrderProcessingNote());
		containerOptions.setPurchaseOrderFormNote(orderOptions.getPurchaseOrderFormNote());
		containerOptions.setCommunicationModeCode(orderOptions.getCommunicationModeCode());
		containerOptions.setGroupCode(orderOptions.getGroupCode());
		containerOptions.setQuoteAccessingSiteNote(orderOptions.getQuoteAccessingSiteNote());
		containerOptions.setGrossPrice(orderOptions.getGrossPrice());
		if (orderOptions.getDiscountType() != null) {
			DiscountType discountType = TypeUtils.getDiscountTypeFromCode(orderOptions.getDiscountType());
			if (discountType != null) {
				containerOptions.setDiscountTypeInd((short) discountType.getId());
			}
		}
		containerOptions.setDiscountAmount(orderOptions.getDiscountAmount());
		containerOptions.setDiscountPrice(orderOptions.getDiscountPrice());
		
		return containerOptions;
	}
	
	private OrderOptions constructOrderOptionsFromContainerOptions(OpxContainerOptions containerOptions) {
		OrderOptions orderOptions = factory.createOrderOptions();

		orderOptions.setOrderDescription(containerOptions.getOrderDescription());
		orderOptions.setCustomerNote(containerOptions.getCustomerNote());
		orderOptions.setTermsAndConditions(containerOptions.getTermsAndConditions());
		orderOptions.setInvoiceDisplayDate(DateUtil.createXMLGregorianCalendar(containerOptions.getInvoiceDisplayDate()));
		orderOptions.setDisplaySubscriptionId(containerOptions.getDisplaySubscriptionId());
		orderOptions.setDisplayPackagedItemPrices(containerOptions.getDisplayPackagedItemPrices());
		orderOptions.setDisplayItemInterfaces(containerOptions.getDisplayItemInterfaces());
		orderOptions.setDisplaySimultaneousUsers(containerOptions.getDisplaySimultaneousUsers());
		orderOptions.setQuoteExpireDays(containerOptions.getQuoteExpireDays());
		orderOptions.setShowMarketingOnQuote(containerOptions.getShowMarketingOnQuote());
		orderOptions.setDisplayItemDiscount(containerOptions.getDisplayItemDiscount());
		orderOptions.setDisplayAccessingSiteCount(containerOptions.getDisplayAccessingSiteCount());
		orderOptions.setPayByCreditCard(containerOptions.getPayByCreditCard());
		orderOptions.setDisplayPackageNames(containerOptions.getDisplayPackageNames());
		orderOptions.setDisplayTaxId(containerOptions.getDisplayTaxId());
		orderOptions.setTaxLanguage(containerOptions.getTaxLanguage());
		orderOptions.setTaxLabel(containerOptions.getTaxLabel());
		orderOptions.setTaxPercent(containerOptions.getTaxPercent());
		orderOptions.setInvoiceAsap(containerOptions.getInvoiceAsap());
		orderOptions.setRushOrder(containerOptions.getRushOrder());
		orderOptions.setQuotedCurrency(containerOptions.getQuotedCurrency());
		orderOptions.setInvoiceNote(containerOptions.getInvoiceNote());
		orderOptions.setOrderProcessingNote(containerOptions.getOrderProcessingNote());
		orderOptions.setPurchaseOrderFormNote(containerOptions.getPurchaseOrderFormNote());
		orderOptions.setCommunicationModeCode(containerOptions.getCommunicationModeCode());
		orderOptions.setGroupCode(containerOptions.getGroupCode());
		orderOptions.setQuoteAccessingSiteNote(containerOptions.getQuoteAccessingSiteNote());
		orderOptions.setGrossPrice(containerOptions.getGrossPrice());
	    if (containerOptions.getDiscountTypeInd() != null) {
	    	DiscountType discountType = DiscountType.getById(containerOptions.getDiscountTypeInd());
	    	if (discountType != null) {
	    		orderOptions.setDiscountType(discountType.name());
	    	}
	    }
	    orderOptions.setDiscountPrice(containerOptions.getDiscountPrice());
	    orderOptions.setDiscountAmount(containerOptions.getDiscountAmount());
	    
		
		return orderOptions;
	}
	
	private OpxContainerInstallments constructContainerInstallmentsFromOrderInstallments(Integer orderId, OrderInstallments orderInstallments) {
		OpxContainerInstallments containerInstallments = new OpxContainerInstallments();

		containerInstallments.setInstallmentCount(orderInstallments.getInstallmentCount());
		containerInstallments.setInstallmentInvoiceDate1(DateUtil.createDate(orderInstallments.getInvoiceDate1()));
		containerInstallments.setInstallmentAmount1(orderInstallments.getAmount1());
		containerInstallments.setInstallmentInvoiceDate2(DateUtil.createDate(orderInstallments.getInvoiceDate2()));
		containerInstallments.setInstallmentAmount2(orderInstallments.getAmount2());
		containerInstallments.setInstallmentInvoiceDate3(DateUtil.createDate(orderInstallments.getInvoiceDate3()));
		containerInstallments.setInstallmentAmount3(orderInstallments.getAmount3());
		containerInstallments.setInstallmentInvoiceDate4(DateUtil.createDate(orderInstallments.getInvoiceDate4()));
		containerInstallments.setInstallmentAmount4(orderInstallments.getAmount4());
		containerInstallments.setInstallmentInvoiceDate5(DateUtil.createDate(orderInstallments.getInvoiceDate5()));
		containerInstallments.setInstallmentAmount5(orderInstallments.getAmount5());
		containerInstallments.setInstallmentInvoiceDate6(DateUtil.createDate(orderInstallments.getInvoiceDate6()));
		containerInstallments.setInstallmentAmount6(orderInstallments.getAmount6());
		containerInstallments.setInstallmentInvoiceDate7(DateUtil.createDate(orderInstallments.getInvoiceDate7()));
		containerInstallments.setInstallmentAmount7(orderInstallments.getAmount7());
		containerInstallments.setInstallmentInvoiceDate8(DateUtil.createDate(orderInstallments.getInvoiceDate8()));
		containerInstallments.setInstallmentAmount8(orderInstallments.getAmount8());
		containerInstallments.setInstallmentInvoiceDate9(DateUtil.createDate(orderInstallments.getInvoiceDate9()));
		containerInstallments.setInstallmentAmount9(orderInstallments.getAmount9());
		containerInstallments.setInstallmentInvoiceDate10(DateUtil.createDate(orderInstallments.getInvoiceDate10()));
		containerInstallments.setInstallmentAmount10(orderInstallments.getAmount10());
		containerInstallments.setInstallmentInvoiceDate11(DateUtil.createDate(orderInstallments.getInvoiceDate11()));
		containerInstallments.setInstallmentAmount11(orderInstallments.getAmount11());
		containerInstallments.setInstallmentInvoiceDate12(DateUtil.createDate(orderInstallments.getInvoiceDate12()));
		containerInstallments.setInstallmentAmount12(orderInstallments.getAmount12());
		
		return containerInstallments;
	}
	
	private OrderInstallments constructOrderInstallmentsFromContainerInstallments(OpxContainerInstallments containerInstallments) {
		OrderInstallments orderInstallments = factory.createOrderInstallments();

		orderInstallments.setInstallmentCount(containerInstallments.getInstallmentCount());
		orderInstallments.setInvoiceDate1(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate1()));
		orderInstallments.setAmount1(containerInstallments.getInstallmentAmount1());
		orderInstallments.setInvoiceDate2(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate2()));
		orderInstallments.setAmount2(containerInstallments.getInstallmentAmount2());
		orderInstallments.setInvoiceDate3(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate3()));
		orderInstallments.setAmount3(containerInstallments.getInstallmentAmount3());
		orderInstallments.setInvoiceDate4(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate4()));
		orderInstallments.setAmount4(containerInstallments.getInstallmentAmount4());
		orderInstallments.setInvoiceDate5(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate5()));
		orderInstallments.setAmount5(containerInstallments.getInstallmentAmount5());
		orderInstallments.setInvoiceDate6(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate6()));
		orderInstallments.setAmount6(containerInstallments.getInstallmentAmount6());
		orderInstallments.setInvoiceDate7(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate7()));
		orderInstallments.setAmount7(containerInstallments.getInstallmentAmount7());
		orderInstallments.setInvoiceDate8(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate8()));
		orderInstallments.setAmount8(containerInstallments.getInstallmentAmount8());
		orderInstallments.setInvoiceDate9(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate9()));
		orderInstallments.setAmount9(containerInstallments.getInstallmentAmount9());
		orderInstallments.setInvoiceDate10(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate10()));
		orderInstallments.setAmount10(containerInstallments.getInstallmentAmount10());
		orderInstallments.setInvoiceDate11(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate11()));
		orderInstallments.setAmount11(containerInstallments.getInstallmentAmount11());
		orderInstallments.setInvoiceDate12(DateUtil.createXMLGregorianCalendar(containerInstallments.getInstallmentInvoiceDate12()));
		orderInstallments.setAmount12(containerInstallments.getInstallmentAmount12());
		
		return orderInstallments;
	}
}