package com.epnet.ops.oiws.service;

import java.util.List;

import com.epnet.ops.oiws.beans.BaseOrder;
import com.epnet.ops.oiws.beans.BaseOrderItem;
import com.epnet.ops.oiws.beans.BasicStatus;
import com.epnet.ops.oiws.beans.CreateOrderPackageRequest;
import com.epnet.ops.oiws.beans.GenerateOrderDocumentRequest;
import com.epnet.ops.oiws.beans.GetNextOrderDocumentToGenerateResponse;
import com.epnet.ops.oiws.beans.GetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.RemoveOrderPackageRequest;
import com.epnet.ops.oiws.beans.SaveItemResponse;
import com.epnet.ops.oiws.beans.SetItemStateRequest;
import com.epnet.ops.oiws.beans.SetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.SetOrderStateRequest;
import com.epnet.ops.oiws.beans.SetOrderStatusRequest;

/**
 * Defines public methods invoked directly by the web service end point methods.
 * 
 * @author jsaparo
 */
public interface IOIService {

	/**
	 * Persist an order structure to the database. 
	 * 
	 * @param order The order to save to the database
	 * @return The identifier of the newly created order
	 */
	int saveOrder(BaseOrder order);
	
	/**
	 * Persist an order structure to the database. 
	 * 
	 * @param item The order item to save to the database
	 * @return The identifier of the newly created item
	 */
	SaveItemResponse saveOrderItem(BaseOrderItem item);
	
	/**
	 * Retrieve an order structure from the database OPs. 
	 * 
	 * @param orderId The unique identifier of the order to retrieve
	 * @return An order structure, or null if not found
	 */
	BaseOrder getOrder(Integer orderId);

	/**
	 * Delete an order from the database OPs. 
	 * 
	 * @param orderId The unique identifier of the order to delete
	 * @return An status structure
	 */
	BasicStatus deleteOrder(Integer orderId);

	/**
	 * Retrieve a collection of order headers for the given criteria.
	 *  
	 * @param custId The customer id to search for
	 * @param categoryCode The category code or 'container type' to search for
	 * @param salesRepId A sales resp to search for
	 * @return A list of matching order header objects, or null if none found
	 */
	List<BaseOrder> listOrders(String custId, String categoryCode, Integer salesRepId);
	
	/**
	 * Retrieve an order item structure from the database OPs. 
	 * 
	 * @param orderItemId The unique identifier of the order item to retrieve
	 * @param itemId The unique identifier of the item to retrieve
	 * @return An order item structure, or null if not found
	 */
	BaseOrderItem getOrderItem(Integer orderItemId, Integer itemId);
	
	/**
	 * Remove an order item from an order. 
	 * 
	 * @param orderId The unique identifier of the order to remove the item from
	 * @param itemId The unique identifier of the item to remove from the order
	 */
	BasicStatus removeOrderItem(Integer orderId, Integer itemId);
	
	/**
	 * Submit a saved order as a purchased order. 
	 * 
	 * @param orderId The unique identifier of the order to submit
	 */
	void submitOrder(int orderId);
	
	/**
	 * Set the status for an order.
	 * 
	 * @param Request details
	 * @return A status indicating the result of the call
	 */
	BasicStatus setOrderStatus(SetOrderStatusRequest request);

	/**
	 * Create a request to generate an order document.
	 * 
	 * @param request Request details
	 * @return A status indicating the result of the call
	 */
	BasicStatus generateOrderDocument(GenerateOrderDocumentRequest request);
	
	/**
	 * Get the id of the next order to generate a document of a particular type for.
	 * 
	 * @return An document generation request response, or null if none exists
	 */
	GetNextOrderDocumentToGenerateResponse getNextOrderDocumentToGenerate();
	
	/**
	 * Set the status for an order document creation request.
	 * 
	 * @param Request details
	 * @return A status indicating the result of the call
	 */
	BasicStatus setOrderDocumentStatus(SetOrderDocumentStatusRequest request);
	
	/**
	 * Get the status for an order document creation request.
	 * 
	 * @param request Request details
	 * @return A status indicating the result of the call
	 */
	BasicStatus getOrderDocumentStatus(GetOrderDocumentStatusRequest request);
	
	/**
	 * Get the id of the next order to submit for processing.
	 * 
	 * @return An order identifier
	 */
	Integer getNextSubmittedOrderToProcess();
	
	/**
	 * Associate an order as a package within another order.
	 * 
	 * @param request Request details
	 * @return The unique id of the newly created order-to-package association
	 */
	Integer createOrderPackage(CreateOrderPackageRequest request);
	
	/**
	 * Dissociate a package from an order.
	 * 
	 * @param request Request details
	 */
	BasicStatus removeOrderPackage(RemoveOrderPackageRequest request);
	
	/**
	 * Set the state of an order, and optionally its items as well.
	 * @param request The request details
	 * @return A status object
	 */
	BasicStatus setOrderState(SetOrderStateRequest request);
	
	/**
	 * Set the state of an individual item.
	 * @param request The request details
	 * @return A status object
	 */
	BasicStatus setItemState(SetItemStateRequest request);
	
}
