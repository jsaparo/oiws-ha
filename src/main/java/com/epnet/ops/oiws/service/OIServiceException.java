package com.epnet.ops.oiws.service;

import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;
import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;

/**
 * General service exception.  If this is thrown while processing
 * a request, the caller will receive a SOAP fault with the specified
 * message string and optional exception trace.
 * 
 * @author jsaparo
 *
 */
@SoapFault(faultCode = FaultCode.SERVER)
public class OIServiceException extends RuntimeException {
	/** Generated */
	private static final long serialVersionUID = -4186410903723957243L;

	public OIServiceException(String msg) {
		super(msg);
	}

	public OIServiceException(String msg, Throwable e) {
		super(msg, e);
	}
}
