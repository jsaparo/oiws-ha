package com.epnet.ops.oiws.endpoint;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.epnet.bse.security.ws.AuthManager;
import com.epnet.ops.oiws.beans.AuthorizeRequest;
import com.epnet.ops.oiws.beans.AuthorizeResponse;
import com.epnet.ops.oiws.beans.BaseOrder;
import com.epnet.ops.oiws.beans.BaseOrderItem;
import com.epnet.ops.oiws.beans.BasicStatus;
import com.epnet.ops.oiws.beans.CreateOrderPackageRequest;
import com.epnet.ops.oiws.beans.CreateOrderPackageResponse;
import com.epnet.ops.oiws.beans.DeleteOrderRequest;
import com.epnet.ops.oiws.beans.DeleteOrderResponse;
import com.epnet.ops.oiws.beans.GenerateOrderDocumentRequest;
import com.epnet.ops.oiws.beans.GenerateOrderDocumentResponse;
import com.epnet.ops.oiws.beans.GetItemRequest;
import com.epnet.ops.oiws.beans.GetItemResponse;
import com.epnet.ops.oiws.beans.GetNextOrderDocumentToGenerateRequest;
import com.epnet.ops.oiws.beans.GetNextOrderDocumentToGenerateResponse;
import com.epnet.ops.oiws.beans.GetNextSubmittedOrderToProcessRequest;
import com.epnet.ops.oiws.beans.GetNextSubmittedOrderToProcessResponse;
import com.epnet.ops.oiws.beans.GetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.GetOrderDocumentStatusResponse;
import com.epnet.ops.oiws.beans.GetOrderRequest;
import com.epnet.ops.oiws.beans.GetOrderResponse;
import com.epnet.ops.oiws.beans.ListOrdersRequest;
import com.epnet.ops.oiws.beans.ListOrdersResponse;
import com.epnet.ops.oiws.beans.LogoutRequest;
import com.epnet.ops.oiws.beans.LogoutResponse;
import com.epnet.ops.oiws.beans.ObjectFactory;
import com.epnet.ops.oiws.beans.RemoveItemRequest;
import com.epnet.ops.oiws.beans.RemoveItemResponse;
import com.epnet.ops.oiws.beans.RemoveOrderPackageRequest;
import com.epnet.ops.oiws.beans.RemoveOrderPackageResponse;
import com.epnet.ops.oiws.beans.SaveItemRequest;
import com.epnet.ops.oiws.beans.SaveItemResponse;
import com.epnet.ops.oiws.beans.SaveOrderRequest;
import com.epnet.ops.oiws.beans.SaveOrderResponse;
import com.epnet.ops.oiws.beans.SetItemStateRequest;
import com.epnet.ops.oiws.beans.SetItemStateResponse;
import com.epnet.ops.oiws.beans.SetOrderDocumentStatusRequest;
import com.epnet.ops.oiws.beans.SetOrderDocumentStatusResponse;
import com.epnet.ops.oiws.beans.SetOrderStateRequest;
import com.epnet.ops.oiws.beans.SetOrderStateResponse;
import com.epnet.ops.oiws.beans.SetOrderStatusRequest;
import com.epnet.ops.oiws.beans.SetOrderStatusResponse;
import com.epnet.ops.oiws.beans.SubmitOrderRequest;
import com.epnet.ops.oiws.beans.SubmitOrderResponse;
import com.epnet.ops.oiws.service.IOIService;

/**
 * Web service entry points. Performs light parameter conversion, then
 * delegates work to service calls.
 * 
 * @author jsaparo
 *
 */
@Endpoint
public class OIServiceEndpoint {

	private Logger logger = Logger.getLogger(OIServiceEndpoint.class);

	private static final String NAMESPACE_URI = "http://www.ebscohost.com/esd/service/OIWS";

	@Resource
	IOIService oiService;
	
	@Resource
	AuthManager authManager;
	
	ObjectFactory wsFactory = new ObjectFactory();
	
	/**
	 * Persist information for an order in the database.
	 * 
	 * @param SaveOrderRequest
	 * @return SaveOrderResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveOrderRequest")
	@ResponsePayload
	public SaveOrderResponse saveOrder(@RequestPayload SaveOrderRequest request) {
		logger.info("saveOrder called");

		// Create the order
		int orderId = oiService.saveOrder(request.getOrder());
		
		// Build response
		SaveOrderResponse response = wsFactory.createSaveOrderResponse();
		response.setOrderIdentifier(orderId);
		
		return response;
	}
	
	/**
	 * Persist information for an item in the database.
	 * 
	 * @param SaveItemRequest
	 * @return SaveItemResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveItemRequest")
	@ResponsePayload
	public SaveItemResponse saveItem(@RequestPayload SaveItemRequest request) {
		logger.info("saveOrder called");

		return oiService.saveOrderItem(request.getItem());
	}
	
	/**
	 * Get information for an order from the database.
	 * 
	 * @param GetOrderRequest
	 * @return GetOrderResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getOrderRequest")
	@ResponsePayload
	public GetOrderResponse getOrder(@RequestPayload GetOrderRequest request) {
		logger.info("getOrder called");

		// Create the order
		BaseOrder order = oiService.getOrder(request.getOrderIdentifier());
		
		// Build response
		GetOrderResponse response = wsFactory.createGetOrderResponse();
		response.setOrder(order);
		
		return response;
	}
	
	/**
	 * Delete an order from the database.
	 * 
	 * @param DeleteOrderRequest
	 * @return DeleteOrderResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteOrderRequest")
	@ResponsePayload
	public DeleteOrderResponse deleteOrder(@RequestPayload DeleteOrderRequest request) {
		logger.info("deleteOrder called");

		// Build response
		DeleteOrderResponse response = wsFactory.createDeleteOrderResponse();
		response.setStatus(oiService.deleteOrder(request.getOrderIdentifier()));
		
		return response;
	}
	
	/**
	 * Get information for an order item from the database.
	 * 
	 * @param GetItemRequest
	 * @return GetItemResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getItemRequest")
	@ResponsePayload
	public GetItemResponse getItem(@RequestPayload GetItemRequest request) {
		logger.info("getItem called");

		// Create the order
		BaseOrderItem item = oiService.getOrderItem(request.getOrderItemIdentifier(), request.getItemIdentifier());
		
		// Build response
		GetItemResponse response = wsFactory.createGetItemResponse();
		response.setOrderItem(item);
		
		return response;
	}
	
	/**
	 * Remove an item from an order in the database.
	 * 
	 * @param RemoveItemRequest
	 * @return RemoveItemResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "removeItemRequest")
	@ResponsePayload
	public RemoveItemResponse removeItem(@RequestPayload RemoveItemRequest request) {
		logger.info("removeItem called");
	
		RemoveItemResponse response = wsFactory.createRemoveItemResponse();
		response.setStatus(oiService.removeOrderItem(request.getOrderIdentifier(), request.getItemIdentifier()));
		
		return response;
	}
	
	/**
	 * Get a list of order header objects from the database.
	 * 
	 * @param ListOrdersRequest
	 * @return ListOrdersResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "listOrdersRequest")
	@ResponsePayload
	public ListOrdersResponse listOrders(@RequestPayload ListOrdersRequest request) {
		logger.info("listOrders called");

		// Create the order
		List<BaseOrder> orders = oiService.listOrders(
			request.getCustomerIdentifier(), request.getCategoryCode(), request.getSalesRepIdentifier());
		
		// Build response
		ListOrdersResponse response = wsFactory.createListOrdersResponse();
		if (!CollectionUtils.isEmpty(orders)) {
			for (BaseOrder order : orders) {
				response.getOrder().add(order);
			}
		}

		return response;
	}
	
	/**
	 * Submit an order for processing.
	 * 
	 * @param SubmitOrderRequest
	 * @return SubmitOrderResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "submitOrderRequest")
	@ResponsePayload
	public SubmitOrderResponse submitOrder(@RequestPayload SubmitOrderRequest request) {
		logger.info("submitOrder called");

		// Create the order
		oiService.submitOrder(request.getOrderIdentifier());
		
		// Build response
		SubmitOrderResponse response = wsFactory.createSubmitOrderResponse();
		
		return response;
	}

	/**
	 * Set the status of a specified order.
	 * 
	 * @param SetOrderStatusRequest
	 * @return SetOrderStatusResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "setOrderStatusRequest")
	@ResponsePayload
	public SetOrderStatusResponse setOrderStatus(@RequestPayload SetOrderStatusRequest request) {
		logger.info("setOrdeStatus called for order: " + request.getOrderIdentifier() + " and stats: " + request.getStatusCode());

		// Create the order
		BasicStatus status = oiService.setOrderStatus(request);
		
		// Create response
		SetOrderStatusResponse response = wsFactory.createSetOrderStatusResponse();
		response.setStatus(status);
		
		return response;
	}

	/**
	 * Set the state of a specified order, and optionally its items.
	 * 
	 * @param SetOrderStateRequest
	 * @return SetOrderStateResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "setOrderStateRequest")
	@ResponsePayload
	public SetOrderStateResponse setOrderState(@RequestPayload SetOrderStateRequest request) {
		logger.info("setOrdeState called for order: " + request.getOrderIdentifier() + 
			" and order state: " + request.getOrderStateCode() + " and item state: " + request.getItemStateCode());

		// Create the order
		BasicStatus status = oiService.setOrderState(request);
		
		// Create response
		SetOrderStateResponse response = wsFactory.createSetOrderStateResponse();
		response.setStatus(status);
		
		return response;
	}

	/**
	 * Set the state of a specified order item.
	 * 
	 * @param SetItemStateRequest
	 * @return SetItemStateResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "setItemStateRequest")
	@ResponsePayload
	public SetItemStateResponse setItemState(@RequestPayload SetItemStateRequest request) {
		logger.info("setItemState called for item: " + request.getItemIdentifier() + 
			" and state: " + request.getItemStateCode());

		// Create the order
		BasicStatus status = oiService.setItemState(request);
		
		// Create response
		SetItemStateResponse response = wsFactory.createSetItemStateResponse();
		response.setStatus(status);
		
		return response;
	}
	
	/**
	 * Get information for the next submitted order to process.
	 * 
	 * @param GetNextSubmittedOrderToProcessRequest
	 * @return GetNextSubmittedOrderToProcessResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getNextSubmittedOrderToProcessRequest")
	@ResponsePayload
	public GetNextSubmittedOrderToProcessResponse getNextSubmittedOrderToProcess(@RequestPayload GetNextSubmittedOrderToProcessRequest request) {
		logger.info("getNextSubmittedOrderToProcess called");

		// Create the order
		Integer orderIdentifier = oiService.getNextSubmittedOrderToProcess();
		
		// Create response
		GetNextSubmittedOrderToProcessResponse response = wsFactory.createGetNextSubmittedOrderToProcessResponse();
		response.setOrderIdentifier(orderIdentifier);
		
		return response;
	}
	
	/**
	 * Create an order document.
	 * 
	 * @param GenerateOrderDocumentRequest
	 * @return GenerateOrderDocumentResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "generateOrderDocumentRequest")
	@ResponsePayload
	public GenerateOrderDocumentResponse createOrderDocument(@RequestPayload GenerateOrderDocumentRequest request) {
		logger.info("createOrderDocument called");

		// Create the order
		BasicStatus status = oiService.generateOrderDocument(request);
		
		// Create response
		GenerateOrderDocumentResponse response = wsFactory.createGenerateOrderDocumentResponse();
		response.setStatus(status);
		
		return response;
	}

	/**
	 * Get information for the next request to geneate an order document of a specified type.
	 * 
	 * @param GetNextOrderDocumentToGenerateRequest
	 * @return GetNextOrderDocumentToGenerateResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getNextOrderDocumentToGenerateRequest")
	@ResponsePayload
	public GetNextOrderDocumentToGenerateResponse getNextOrderDocumentToCreate(@RequestPayload GetNextOrderDocumentToGenerateRequest request) {
		logger.info("getNextOrderDocumentToCreate called");

		return oiService.getNextOrderDocumentToGenerate();
	}
	
	/**
	 * Set the status of a document generation request.
	 * 
	 * @param SetOrderDocumentStatusRequest
	 * @return SetOrderDocumentStatusResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "setOrderDocumentStatusRequest")
	@ResponsePayload
	public SetOrderDocumentStatusResponse setOrderDocumentStatus(@RequestPayload SetOrderDocumentStatusRequest request) {
		logger.info("setOrderDocumentStatus called");

		// Create the order
		BasicStatus status = oiService.setOrderDocumentStatus(request);
		
		// Create response
		SetOrderDocumentStatusResponse response = wsFactory.createSetOrderDocumentStatusResponse();
		response.setStatus(status);
		
		return response;
	}

	/**
	 * Get the status of an order document.
	 * 
	 * @param GetOrderDocumentStatusRequest
	 * @return GetOrderDocumentStatusResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getOrderDocumentStatusRequest")
	@ResponsePayload
	public GetOrderDocumentStatusResponse getOrderDocumentStatus(@RequestPayload GetOrderDocumentStatusRequest request) {
		logger.info("getOrderDocumentStatus called");

		// Create the order
		BasicStatus status = oiService.getOrderDocumentStatus(request);
		
		// Create response
		GetOrderDocumentStatusResponse response = wsFactory.createGetOrderDocumentStatusResponse();
		response.setStatus(status);
		
		return response;
	}

	/**
	 * Associate an order as a package with a containing order.
	 * 
	 * @param CreateOrderPackageRequest
	 * @return CreateOrderPackageResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "createOrderPackageRequest")
	@ResponsePayload
	public CreateOrderPackageResponse createOrderPackage(@RequestPayload CreateOrderPackageRequest request) {
		logger.info("createOrderPackage called");

		// Create the order
		int packageId = oiService.createOrderPackage(request);
		
		// Build response
		CreateOrderPackageResponse response = wsFactory.createCreateOrderPackageResponse();
		response.setPackageIdentifier(packageId);
		
		return response;
	}

	/**
	 * Dissociate an order as a package from a containing order.
	 * 
	 * @param RemoveOrderPackageRequest
	 * @return RemoveOrderPackageResponse
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "removeOrderPackageRequest")
	@ResponsePayload
	public RemoveOrderPackageResponse removeOrderPackage(@RequestPayload RemoveOrderPackageRequest request) {
		logger.info("removeOrderPackage called");

		// Create the order
		BasicStatus status = oiService.removeOrderPackage(request);
		
		// Build response
		RemoveOrderPackageResponse response = wsFactory.createRemoveOrderPackageResponse();
		response.setStatus(status);

		return response;
	}

	/**
	 * This method will authenticate the specified user.
	 * @param req
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart="AuthorizeRequest")
	@ResponsePayload
	public AuthorizeResponse handleAuthorize(@RequestPayload AuthorizeRequest req)
	{
		AuthorizeResponse response = new AuthorizeResponse();
		if (req.getUsername() != null && req.getPassword() != null) {
			response.setAuthorizeResult(String.valueOf(
				authManager.authenticate(req.getUsername(), req.getPassword())));
		}
		else {
			response.setAuthorizeResult(String.valueOf(authManager.authorize()));
		}
		return response;
	}
	
	/**
	 * This method will sign out the current User.
	 * @param req
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart="LogoutRequest")
	@ResponsePayload
	public LogoutResponse handleLogout(@RequestPayload LogoutRequest req)
	{
		LogoutResponse response = new LogoutResponse();
		authManager.logout();

		return response;
	}
	
}
