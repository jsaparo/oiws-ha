//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.13 at 05:28:42 PM EDT 
//


package com.epnet.ops.oiws.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				A structure representing a set options that
 * 				typically affect the presentation of
 * 				order data on various documents
 * 				such as quotes and purchase order forms.
 * 			
 * 
 * <p>Java class for OrderOptions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderOptions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="termsAndConditions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceDisplayDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="displaySubscriptionId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="displayPackagedItemPrices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="displayItemInterfaces" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="displaySimultaneousUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="quoteExpireDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="showMarketingOnQuote" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="displayItemDiscount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="payByCreditCard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="displayAccessingSiteCount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="displayPackageNames" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="taxLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="displayTaxId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="taxLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxPercent" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="invoiceAsap" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="rushOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="quotedCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderProcessingNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="purchaseOrderFormNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="communicationModeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="groupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accessingSiteNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="grossPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="discountPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="discountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discountAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="quoteAccessingSiteNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderOptions", propOrder = {
    "orderDescription",
    "customerNote",
    "termsAndConditions",
    "invoiceDisplayDate",
    "displaySubscriptionId",
    "displayPackagedItemPrices",
    "displayItemInterfaces",
    "displaySimultaneousUsers",
    "quoteExpireDays",
    "showMarketingOnQuote",
    "displayItemDiscount",
    "payByCreditCard",
    "displayAccessingSiteCount",
    "displayPackageNames",
    "taxLanguage",
    "displayTaxId",
    "taxLabel",
    "taxPercent",
    "invoiceAsap",
    "rushOrder",
    "quotedCurrency",
    "invoiceNote",
    "orderProcessingNote",
    "purchaseOrderFormNote",
    "communicationModeCode",
    "groupCode",
    "accessingSiteNote",
    "grossPrice",
    "discountPrice",
    "discountType",
    "discountAmount",
    "quoteAccessingSiteNote"
})
public class OrderOptions {

    protected String orderDescription;
    protected String customerNote;
    protected String termsAndConditions;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar invoiceDisplayDate;
    protected Boolean displaySubscriptionId;
    protected Boolean displayPackagedItemPrices;
    protected Boolean displayItemInterfaces;
    protected Boolean displaySimultaneousUsers;
    protected Integer quoteExpireDays;
    protected Boolean showMarketingOnQuote;
    protected Boolean displayItemDiscount;
    protected Boolean payByCreditCard;
    protected Boolean displayAccessingSiteCount;
    protected Boolean displayPackageNames;
    protected String taxLanguage;
    protected Boolean displayTaxId;
    protected String taxLabel;
    protected Double taxPercent;
    protected Boolean invoiceAsap;
    protected Boolean rushOrder;
    protected String quotedCurrency;
    protected String invoiceNote;
    protected String orderProcessingNote;
    protected String purchaseOrderFormNote;
    protected String communicationModeCode;
    protected String groupCode;
    protected String accessingSiteNote;
    protected Double grossPrice;
    protected Double discountPrice;
    protected String discountType;
    protected Double discountAmount;
    protected String quoteAccessingSiteNote;

    /**
     * Gets the value of the orderDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderDescription() {
        return orderDescription;
    }

    /**
     * Sets the value of the orderDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderDescription(String value) {
        this.orderDescription = value;
    }

    /**
     * Gets the value of the customerNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNote() {
        return customerNote;
    }

    /**
     * Sets the value of the customerNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNote(String value) {
        this.customerNote = value;
    }

    /**
     * Gets the value of the termsAndConditions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    /**
     * Sets the value of the termsAndConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermsAndConditions(String value) {
        this.termsAndConditions = value;
    }

    /**
     * Gets the value of the invoiceDisplayDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceDisplayDate() {
        return invoiceDisplayDate;
    }

    /**
     * Sets the value of the invoiceDisplayDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceDisplayDate(XMLGregorianCalendar value) {
        this.invoiceDisplayDate = value;
    }

    /**
     * Gets the value of the displaySubscriptionId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplaySubscriptionId() {
        return displaySubscriptionId;
    }

    /**
     * Sets the value of the displaySubscriptionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplaySubscriptionId(Boolean value) {
        this.displaySubscriptionId = value;
    }

    /**
     * Gets the value of the displayPackagedItemPrices property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayPackagedItemPrices() {
        return displayPackagedItemPrices;
    }

    /**
     * Sets the value of the displayPackagedItemPrices property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayPackagedItemPrices(Boolean value) {
        this.displayPackagedItemPrices = value;
    }

    /**
     * Gets the value of the displayItemInterfaces property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayItemInterfaces() {
        return displayItemInterfaces;
    }

    /**
     * Sets the value of the displayItemInterfaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayItemInterfaces(Boolean value) {
        this.displayItemInterfaces = value;
    }

    /**
     * Gets the value of the displaySimultaneousUsers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplaySimultaneousUsers() {
        return displaySimultaneousUsers;
    }

    /**
     * Sets the value of the displaySimultaneousUsers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplaySimultaneousUsers(Boolean value) {
        this.displaySimultaneousUsers = value;
    }

    /**
     * Gets the value of the quoteExpireDays property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteExpireDays() {
        return quoteExpireDays;
    }

    /**
     * Sets the value of the quoteExpireDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteExpireDays(Integer value) {
        this.quoteExpireDays = value;
    }

    /**
     * Gets the value of the showMarketingOnQuote property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowMarketingOnQuote() {
        return showMarketingOnQuote;
    }

    /**
     * Sets the value of the showMarketingOnQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowMarketingOnQuote(Boolean value) {
        this.showMarketingOnQuote = value;
    }

    /**
     * Gets the value of the displayItemDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayItemDiscount() {
        return displayItemDiscount;
    }

    /**
     * Sets the value of the displayItemDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayItemDiscount(Boolean value) {
        this.displayItemDiscount = value;
    }

    /**
     * Gets the value of the payByCreditCard property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPayByCreditCard() {
        return payByCreditCard;
    }

    /**
     * Sets the value of the payByCreditCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPayByCreditCard(Boolean value) {
        this.payByCreditCard = value;
    }

    /**
     * Gets the value of the displayAccessingSiteCount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayAccessingSiteCount() {
        return displayAccessingSiteCount;
    }

    /**
     * Sets the value of the displayAccessingSiteCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayAccessingSiteCount(Boolean value) {
        this.displayAccessingSiteCount = value;
    }

    /**
     * Gets the value of the displayPackageNames property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayPackageNames() {
        return displayPackageNames;
    }

    /**
     * Sets the value of the displayPackageNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayPackageNames(Boolean value) {
        this.displayPackageNames = value;
    }

    /**
     * Gets the value of the taxLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxLanguage() {
        return taxLanguage;
    }

    /**
     * Sets the value of the taxLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxLanguage(String value) {
        this.taxLanguage = value;
    }

    /**
     * Gets the value of the displayTaxId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayTaxId() {
        return displayTaxId;
    }

    /**
     * Sets the value of the displayTaxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayTaxId(Boolean value) {
        this.displayTaxId = value;
    }

    /**
     * Gets the value of the taxLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxLabel() {
        return taxLabel;
    }

    /**
     * Sets the value of the taxLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxLabel(String value) {
        this.taxLabel = value;
    }

    /**
     * Gets the value of the taxPercent property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxPercent() {
        return taxPercent;
    }

    /**
     * Sets the value of the taxPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxPercent(Double value) {
        this.taxPercent = value;
    }

    /**
     * Gets the value of the invoiceAsap property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInvoiceAsap() {
        return invoiceAsap;
    }

    /**
     * Sets the value of the invoiceAsap property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInvoiceAsap(Boolean value) {
        this.invoiceAsap = value;
    }

    /**
     * Gets the value of the rushOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRushOrder() {
        return rushOrder;
    }

    /**
     * Sets the value of the rushOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRushOrder(Boolean value) {
        this.rushOrder = value;
    }

    /**
     * Gets the value of the quotedCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuotedCurrency() {
        return quotedCurrency;
    }

    /**
     * Sets the value of the quotedCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuotedCurrency(String value) {
        this.quotedCurrency = value;
    }

    /**
     * Gets the value of the invoiceNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNote() {
        return invoiceNote;
    }

    /**
     * Sets the value of the invoiceNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNote(String value) {
        this.invoiceNote = value;
    }

    /**
     * Gets the value of the orderProcessingNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderProcessingNote() {
        return orderProcessingNote;
    }

    /**
     * Sets the value of the orderProcessingNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderProcessingNote(String value) {
        this.orderProcessingNote = value;
    }

    /**
     * Gets the value of the purchaseOrderFormNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderFormNote() {
        return purchaseOrderFormNote;
    }

    /**
     * Sets the value of the purchaseOrderFormNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderFormNote(String value) {
        this.purchaseOrderFormNote = value;
    }

    /**
     * Gets the value of the communicationModeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommunicationModeCode() {
        return communicationModeCode;
    }

    /**
     * Sets the value of the communicationModeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommunicationModeCode(String value) {
        this.communicationModeCode = value;
    }

    /**
     * Gets the value of the groupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * Sets the value of the groupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupCode(String value) {
        this.groupCode = value;
    }

    /**
     * Gets the value of the accessingSiteNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessingSiteNote() {
        return accessingSiteNote;
    }

    /**
     * Sets the value of the accessingSiteNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessingSiteNote(String value) {
        this.accessingSiteNote = value;
    }

    /**
     * Gets the value of the grossPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getGrossPrice() {
        return grossPrice;
    }

    /**
     * Sets the value of the grossPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setGrossPrice(Double value) {
        this.grossPrice = value;
    }

    /**
     * Gets the value of the discountPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDiscountPrice() {
        return discountPrice;
    }

    /**
     * Sets the value of the discountPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDiscountPrice(Double value) {
        this.discountPrice = value;
    }

    /**
     * Gets the value of the discountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountType() {
        return discountType;
    }

    /**
     * Sets the value of the discountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountType(String value) {
        this.discountType = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDiscountAmount(Double value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the quoteAccessingSiteNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteAccessingSiteNote() {
        return quoteAccessingSiteNote;
    }

    /**
     * Sets the value of the quoteAccessingSiteNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteAccessingSiteNote(String value) {
        this.quoteAccessingSiteNote = value;
    }

}
