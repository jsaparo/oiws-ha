//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.13 at 05:28:42 PM EDT 
//


package com.epnet.ops.oiws.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getNextOrderDocumentToCreateFault" type="{http://www.ebscohost.com/esd/service/OIWS}OIWSFaultType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNextOrderDocumentToCreateFault"
})
@XmlRootElement(name = "getNextOrderDocumentToGenerateFault")
public class GetNextOrderDocumentToGenerateFault {

    @XmlElement(required = true)
    protected OIWSFaultType getNextOrderDocumentToCreateFault;

    /**
     * Gets the value of the getNextOrderDocumentToCreateFault property.
     * 
     * @return
     *     possible object is
     *     {@link OIWSFaultType }
     *     
     */
    public OIWSFaultType getGetNextOrderDocumentToCreateFault() {
        return getNextOrderDocumentToCreateFault;
    }

    /**
     * Sets the value of the getNextOrderDocumentToCreateFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link OIWSFaultType }
     *     
     */
    public void setGetNextOrderDocumentToCreateFault(OIWSFaultType value) {
        this.getNextOrderDocumentToCreateFault = value;
    }

}
